﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using App.DataAccess.Mappings;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using System.Linq;
using App.DataAccess.Core;

namespace App.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            ConfigureEntities(builder);
        }

        private void ConfigureEntities(ModelBuilder builder)
        {
            var types = typeof(ApplicationDbContext).GetTypeInfo().Assembly.GetTypes();
            var maps = (from t in types
                        where typeof(IEntityConfiguration).IsAssignableFrom(t)
                              && !t.GetTypeInfo().IsAbstract
                              && !t.GetTypeInfo().IsInterface
                        select (IEntityConfiguration)Activator.CreateInstance(t)).ToArray();

            foreach (var map in maps)
            {
                map.Configure(builder);
            }
        }
    }

    public class ApplicationDbContextFactory : IDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext Create(DbContextFactoryOptions options)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(options.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{options.EnvironmentName}.json", optional: true);
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var configuration = config.Build();

            builder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            return new ApplicationDbContext(builder.Options);
        }
    }
}
