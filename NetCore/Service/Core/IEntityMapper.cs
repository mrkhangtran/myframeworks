﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Entity.Core;

namespace App.Service.Core
{
    /// <summary>
    /// Maps between entity and DTO
    /// </summary>
    /// <typeparam name="TDto"></typeparam>
    /// <typeparam name="TEntity"></typeparam>
    public interface IEntityMapper<TDto, TEntity> where TDto : BaseDto where TEntity : BaseEntity
    {
        /// <summary>
        /// Maps DTO to entity.
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="entity"></param>
        /// <param name="username"></param>
        /// <param name="entityState"></param>
        /// <returns></returns>
        TEntity MapToEntity(TDto dto, TEntity entity, string username, EntityState entityState);

        /// <summary>
        /// Maps entity to DTO
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TDto MapFromEntity(TEntity entity);

        /// <summary>
        /// Maps entities to DTOs
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        IEnumerable<TDto> MapFromEntity(IEnumerable<TEntity> entities);
    }
}
