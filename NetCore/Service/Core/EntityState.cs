﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Service.Core
{
    public enum EntityState
    {
        Created,
        Modified,
        None
    }
}
