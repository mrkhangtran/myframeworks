﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;

namespace App.Service.Core
{
    public interface ICreateMapping
    {
        /// <summary>
        /// Defines the mapping and adds it into the mapping profile
        /// </summary>
        /// <param name="profile"></param>
        void CreateMapping(Profile profile);
    }
}
