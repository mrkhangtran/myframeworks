﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Service.Core.Exceptions
{
    public class InvalidSortParameterException : Exception
    {
        public string Parameter { get; }

        public InvalidSortParameterException(string parameter)
            : base("Invalid sort parameter")
        {
            Parameter = parameter;
        }
    }
}
