﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Service.Core
{
    public class ListResultDto<TDto> where TDto : BaseDto
    {
        public IEnumerable<TDto> Items { get; set; }
        public int TotalCount { get; set; }
        public int PageCount { get; set; }
    }
}
