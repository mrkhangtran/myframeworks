﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Entity.Core;
using AutoMapper;

namespace App.Service.Core
{
    abstract class EntityMapper<TDto, TEntity> : IEntityMapper<TDto, TEntity>, ICreateMapping
        where TDto : BaseDto
        where TEntity : BaseEntity, new()
    {
        private readonly IMapper _mapper;

        protected EntityMapper() {}

        protected EntityMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public virtual TEntity MapToEntity(TDto dto, TEntity entity, string username, EntityState entityState)
        {
            var result = entity == null ? _mapper.Map<TEntity>(dto) : _mapper.Map(dto, entity);

            if (entityState == EntityState.Created)
            {
                result.CreatedBy = username;
                result.CreatedDate = DateTime.UtcNow;
            }
            else if (entityState == EntityState.Modified)
            {
                result.ModifiedBy = username;
                result.ModifiedDate = DateTime.UtcNow;
            }

            return result;
        }

        public virtual TDto MapFromEntity(TEntity entity)
        {
            return _mapper.Map<TDto>(entity);
        }

        public virtual IEnumerable<TDto> MapFromEntity(IEnumerable<TEntity> entities)
        {
            return _mapper.Map<IEnumerable<TDto>>(entities);
        }

        public abstract void CreateMapping(Profile profile);
    }
}
