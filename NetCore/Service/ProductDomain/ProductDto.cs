﻿using System.Collections.Generic;
using App.Service.Core;

namespace App.Service.ProductDomain
{
    public class ProductDto : BaseDto
    {
        public string Name { get; set; }

        public ICollection<ProductPhotoDto> Photos { get; set; }
    }
}
