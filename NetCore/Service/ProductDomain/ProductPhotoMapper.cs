﻿using App.Entity.Core;
using App.Entity.ProductDomain;
using App.Service.Core;
using AutoMapper;

namespace App.Service.ProductDomain
{
    class ProductPhotoMapper : EntityMapper<ProductPhotoDto, ProductPhoto>
    {
        public ProductPhotoMapper() {}

        public ProductPhotoMapper(IMapper mapper) : base(mapper)
        {
        }

        public override void CreateMapping(Profile profile)
        {
            profile.CreateMap<ProductPhoto, ProductPhotoDto>().IncludeBase<BaseEntity, BaseDto>();
            profile.CreateMap<ProductPhotoDto, ProductPhoto>().IncludeBase<BaseDto, BaseEntity>();
        }
    }
}
