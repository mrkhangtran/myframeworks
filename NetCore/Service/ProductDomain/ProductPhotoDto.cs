﻿using App.Service.Core;

namespace App.Service.ProductDomain
{
    public class ProductPhotoDto : BaseDto
    {
        public string Url { get; set; }
    }
}
