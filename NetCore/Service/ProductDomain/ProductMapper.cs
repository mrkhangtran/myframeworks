﻿using System;
using System.Linq;
using App.Entity.Core;
using App.Entity.ProductDomain;
using App.Service.Core;
using AutoMapper;

namespace App.Service.ProductDomain
{
    class ProductMapper : EntityMapper<ProductDto, Product>
    {
        private readonly IEntityMapper<ProductPhotoDto, ProductPhoto> _photoMapper;

        public ProductMapper() {}

        public ProductMapper(IMapper mapper, IEntityMapper<ProductPhotoDto, ProductPhoto> photoMapper) : base(mapper)
        {
            _photoMapper = photoMapper;
        }

        public override void CreateMapping(Profile profile)
        {
            profile.CreateMap<Product, ProductDto>().IncludeBase<BaseEntity, BaseDto>();

            profile.CreateMap<ProductDto, Product>().IncludeBase<BaseDto, BaseEntity>()
                .ForMember(x => x.Photos, c => c.Ignore()); // handle child collection manually
        }

        public override Product MapToEntity(ProductDto dto, Product entity, string username, EntityState entityState)
        {
            entity = base.MapToEntity(dto, entity, username, entityState);

            if (dto.Id > 0)
            {
                // remove deleted photos
                entity.Photos.Where(oldPhoto => dto.Photos.All(newPhoto => newPhoto.Id != oldPhoto.Id))
                    .ToList().ForEach(oldPhoto => entity.Photos.Remove(oldPhoto));
            }

            // add new photos
            dto.Photos.Where(p => p.Id == 0).ToList().ForEach(p =>
            {
                var photo = _photoMapper.MapToEntity(p, null, username, EntityState.Created);
                entity.Photos.Add(photo);
            });

            return entity;
        }
    }
}
