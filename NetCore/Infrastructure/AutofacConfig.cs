﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.DataAccess;
using App.Repository.Core;
using App.Service.ProductDomain;
using Autofac;

namespace App.Infrastructure
{
    public class AutofacConfig
    {
        /// <summary>
        /// Registers dependencies
        /// </summary>
        /// <param name="builder"></param>
        public static void Register(ContainerBuilder builder)
        {
            // Unit of Work
            //builder.RegisterType<ApplicationDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            // Services
            // A trick to load dependency assembly prior to do the registration
            // Just need to register for 1 class to let the assembly load, once it is loaded, 
            // it can then be used to scan for other classes and registered dynamically
            builder.RegisterType<ProductService>().As<IProductService>();
            RegisterDependenciesByConvention(builder, "App.Service");
        }

        /// <summary>
        /// Registers dynamically all implementations with their interface
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="assemblyName"></param>
        private static void RegisterDependenciesByConvention(ContainerBuilder builder, string assemblyName)
        {
            var neededAssemblies = AppDomain.CurrentDomain.GetAssemblies(assemblyName);
            builder.RegisterAssemblyTypes(neededAssemblies)
                .AsImplementedInterfaces()
                .PreserveExistingDefaults();
        }
    }
}
