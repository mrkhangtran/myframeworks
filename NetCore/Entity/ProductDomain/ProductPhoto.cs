﻿using App.Entity.Core;

namespace App.Entity.ProductDomain
{
    public class ProductPhoto : BaseEntity
    {
        public string Url { get; set; }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}
