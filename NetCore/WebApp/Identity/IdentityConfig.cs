﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace WebApp.Identity
{
    public class IdentityConfig
    {
        public static void Register(IServiceCollection services, IConfigurationRoot configuration)
        {
            RegisterAspNetIdentity(services, configuration);
            RegisterIdentityServer(services);
        }

        private static void RegisterAspNetIdentity(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddDbContext<ApplicationIdentityDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationIdentityDbContext>()
                .AddDefaultTokenProviders();
        }

        private static void RegisterIdentityServer(IServiceCollection services)
        {
            var clients = new List<Client>
            {
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",
                    AllowedGrantTypes = GrantTypes.Hybrid,

                    RequireConsent = false,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    RedirectUris = {"http://localhost:2237/signin-oidc"},
                    PostLogoutRedirectUris = {"http://localhost:2237/signout-callback-oidc"},

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "myapi"
                    },
                    AllowOfflineAccess = true
                }
            };

            var apiResources = new List<ApiResource>
            {
                new ApiResource("myapi", "My API")
            };

            var identityResouces = new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };

            services.AddIdentityServer()
                .AddTemporarySigningCredential()
                .AddInMemoryIdentityResources(identityResouces)
                .AddInMemoryApiResources(apiResources)
                .AddInMemoryClients(clients)
                .AddAspNetIdentity<ApplicationUser>();
        }
    }
}
