﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Service.ProductDomain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductService _productService;

        public HomeController(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<IActionResult> Index()
        {
            //var fake = new ProductDto
            //{
            //    Name = "fff",
            //    CreatedBy = "admin",
            //    CreatedDate = DateTime.UtcNow,
            //    Photos = new List<ProductPhotoDto>
            //    {
            //        new ProductPhotoDto {Id = 0, Url = "newnew_3"}
            //    }
            //};

            //await _productService.InsertOrUpdateAsync(fake, "admin");

            //var test = await _productService.ListAsync("a");

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        //[Authorize(Roles = "Admin")]
        public IActionResult Secure()
        {
            ViewData["Message"] = "Secure page.";

            return View();
        }
    }
}
