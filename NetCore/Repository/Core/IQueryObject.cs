﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using App.Entity;
using App.Entity.Core;

namespace App.Repository.Core
{
    public interface IQueryObject<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// Returns the query expression
        /// </summary>
        /// <returns></returns>
        Expression<Func<TEntity, bool>> Query();

        Expression<Func<TEntity, bool>> And(Expression<Func<TEntity, bool>> query);

        Expression<Func<TEntity, bool>> Or(Expression<Func<TEntity, bool>> query);

        Expression<Func<TEntity, bool>> And(IQueryObject<TEntity> queryObject);

        Expression<Func<TEntity, bool>> Or(IQueryObject<TEntity> queryObject);
    }
}
