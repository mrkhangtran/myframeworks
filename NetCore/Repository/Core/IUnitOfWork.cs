﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Entity;
using App.Entity.Core;

namespace App.Repository.Core
{
    public interface IUnitOfWork
    {
        ///// <summary>
        ///// Executes SQL query against database
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="sqlQuery">Query</param>
        ///// <param name="parameters">Parameters</param>
        ///// <returns></returns>
        //IEnumerable<T> ExecuteSqlQuery<T>(string sqlQuery, params object[] parameters);

        ///// <summary>
        ///// Executes SQL command against database
        ///// </summary>
        ///// <param name="sqlCommand">Command</param>
        ///// <param name="parameters">Parameters</param>
        ///// <returns></returns>
        //void ExecuteSqlCommand(string sqlCommand, params object[] parameters);

        /// <summary>
        /// Gets database server time
        /// </summary>
        /// <returns></returns>
        //DateTime GetDatabaseServerTime();

        /// <summary>
        /// Saves changes to database
        /// </summary>
        /// <param name="strategy">Concurrency resolution strategy</param>
        /// <returns></returns>
        void SaveChanges(ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None);

        /// <summary>
        /// Saves changes asynchronously
        /// </summary>
        /// <param name="strategy">Concurrency resolution strategy</param>
        /// <returns></returns>
        Task SaveChangesAsync(ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None);

        /// <summary>
        /// Saves changes asynchronously
        /// </summary>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <param name="strategy">Concurrency resolution strategy</param>
        /// <returns></returns>
        Task SaveChangesAsync(CancellationToken cancellationToken, ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None);

        /// <summary>
        /// Forces reloading data from database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">Item to reload</param>
        /// <returns></returns>
        T Reload<T>(T item) where T : BaseEntity;

        /// <summary>
        /// Begins a transaction
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// Commits a transaction
        /// </summary>
        /// <returns></returns>
        bool CommitTransaction();

        /// <summary>
        /// Rollbacks a transaction
        /// </summary>
        void RollbackTransaction();

        /// <summary>
        /// Gets a repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IRepository<T> Repository<T>() where T : BaseEntity;
    }
}
