﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Seedwork;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infrastructure.Repositories
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : AggregateRoot
    {
        protected readonly ApplicationDbContext DbContext;

        public IUnitOfWork UnitOfWork => DbContext;

        protected RepositoryBase(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        protected virtual Task LoadChildrenAsync(T entity)
        {
            return Task.CompletedTask;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            var entity = await DbContext.Set<T>().FindAsync(id);
            if (entity != null)
            {
                await LoadChildrenAsync(entity);
            }

            return entity;
        }

        public async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> filter)
        {
            var entities = await DbContext.Set<T>().Where(filter).ToListAsync();
            return entities;
        }

        public Task InsertAsync(T entity)
        {
            return DbContext.Set<T>().AddAsync(entity);
        }

        public Task InsertRangeAsync(IEnumerable<T> entities)
        {
            return DbContext.Set<T>().AddRangeAsync(entities);
        }

        public void Update(T entity)
        {
            DbContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            DbContext.Set<T>().Remove(entity);
        }
    }
}
