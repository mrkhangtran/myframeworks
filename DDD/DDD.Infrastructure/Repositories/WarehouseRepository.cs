﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Aggregates.WarehouseAggregate;

namespace DDD.Infrastructure.Repositories
{
    public class WarehouseRepository : RepositoryBase<Warehouse>, IWarehouseRepository
    {
        public WarehouseRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
