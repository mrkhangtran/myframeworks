﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Aggregates.ProductAggregate;
using DDD.Domain.Seedwork;

namespace DDD.Infrastructure.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        protected override async Task LoadChildrenAsync(Product entity)
        {
            //await DbContext.Entry(entity).Collection(i => i.Photos).LoadAsync();
            await DbContext.Entry(entity).Reference(i => i.Detail).LoadAsync();
            await DbContext.Entry(entity).Reference(i => i.Status).LoadAsync();
        }
    }
}
