﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Exceptions;

namespace DDD.Infrastructure.Idempotency
{
    public class RequestManager : IRequestManager
    {
        private readonly ApplicationDbContext _context;

        public RequestManager(ApplicationDbContext context)
        {
            _context = context;
        }


        public async Task<bool> ExistsAsync(Guid id)
        {
            var request = await _context.FindAsync<ClientRequest>(id);

            return request != null;
        }

        public async Task CreateRequestForCommandAsync<T>(Guid id)
        {
            var exists = await ExistsAsync(id);

            var request = exists
                ? throw new DomainException($"Request with {id} already exists")
                : new ClientRequest
                {
                    Id = id,
                    Name = typeof(T).Name,
                    Time = DateTime.UtcNow
                };

            _context.Add(request);

            await _context.SaveChangesAsync();
        }
    }
}
