﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Domain.Aggregates.ProductAggregate;
using DDD.Domain.Aggregates.WarehouseAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDD.Infrastructure.EntityConfigurations
{
    class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            // table
            builder.ToTable("Products");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("ProductsSequenceHiLo");

            // value object
            builder.OwnsOne(x => x.Detail);

            // collection
            builder.Metadata.FindNavigation(nameof(Product.Photos)).SetPropertyAccessMode(PropertyAccessMode.Field);

            // FK
            builder.HasOne<Warehouse>()
                .WithMany()
                .IsRequired(false)
                .HasForeignKey("WarehouseId");

            builder.HasOne(x => x.Status)
                .WithMany()
                .HasForeignKey("StatusId");

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.CreateBy).IsRequired();
            builder.Property(x => x.UpdatedBy).IsRequired();
            builder.Property<int>("StatusId").IsRequired();
        }
    }
}
