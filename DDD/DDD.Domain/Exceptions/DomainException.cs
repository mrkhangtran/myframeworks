﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Domain.Exceptions
{
    public class DomainException : Exception
    {
        public bool IsDbConcurrencyUpdate { get; set; }

        public DomainException()
        { }

        public DomainException(string message)
            : base(message)
        { }

        public DomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
