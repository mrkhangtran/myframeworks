﻿using System;
using System.Collections.Generic;
using DDD.Common.Helpers;
using MediatR;

namespace DDD.Domain.Seedwork
{
    public abstract class Entity
    {
        public int Id { get; protected set; }

        public DateTime CreatedDateUtc { get; protected set; }

        public DateTime UpdatedDateUtc { get; protected set; }

        public string CreateBy { get; protected set; }

        public string UpdatedBy { get; protected set; }

        public byte[] RowVersion { get; protected set; }

        private readonly List<INotification> _domainEvents = new List<INotification>();
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents;

        protected Entity(string createdBy)
        {
            CreateBy = createdBy;
            UpdatedBy = createdBy;
            CreatedDateUtc = DateTime.UtcNow;
            UpdatedDateUtc = DateTime.UtcNow;
        }

        protected void Audit(string updatedBy)
        {
            UpdatedBy = updatedBy;
            UpdatedDateUtc = DateTime.UtcNow;
        }

        public void AddDomainEvent(INotification @event)
        {
            _domainEvents.Add(@event);
        }

        public void RemoveDomainEvent(INotification @event)
        {
            _domainEvents.Remove(@event);
        }

        public void ClearDomainEvents()
        {
            _domainEvents.Clear();
        }

        public bool IsTransient()
        {
            return Id == 0;
        }

        public void SetRowVersion(string rowVersion)
        {
            RowVersion = ByteArrayConverter.FromString(rowVersion);
        }
    }
}

