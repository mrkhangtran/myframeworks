﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DDD.Domain.Seedwork
{
    public interface IUnitOfWork : IDisposable
    {
        Task SaveChangesAsync(ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None, CancellationToken cancellationToken = default(CancellationToken));

        void BeginTransaction();

        void CommitTransaction();

        void RollbackTransaction();
    }
}
