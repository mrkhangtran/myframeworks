﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace DDD.Domain.Events
{
    public class WarehouseNameChangedDomainEvent : INotification
    {
        public string WarehouseName { get; }

        public int WarehouseId { get; }

        public WarehouseNameChangedDomainEvent(string warehouseName, int warehouseId)
        {
            WarehouseName = warehouseName;
            WarehouseId = warehouseId;
        }
    }
}
