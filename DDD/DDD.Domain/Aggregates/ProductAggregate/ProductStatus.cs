﻿using DDD.Domain.Seedwork;

namespace DDD.Domain.Aggregates.ProductAggregate
{
    public class ProductStatus : Enumeration
    {
        public static ProductStatus ForSale = new ProductStatus(1, "For Sale");
        public static ProductStatus Suspended = new ProductStatus(2, "Suspended");

        public ProductStatus(int id, string name) : base(id, name)
        {
            
        }
    }
}
