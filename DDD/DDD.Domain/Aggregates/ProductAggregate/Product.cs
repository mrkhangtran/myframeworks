﻿using System;
using System.Collections.Generic;
using DDD.Domain.Exceptions;
using DDD.Domain.Seedwork;

namespace DDD.Domain.Aggregates.ProductAggregate
{
    public class Product : AggregateRoot
    {
        public string Name { get; private set; }

        public int? WarehouseId { get; private set; }

        private readonly List<ProductPhoto> _photos;
        public IReadOnlyCollection<ProductPhoto> Photos => _photos;

        private int _statusId;
        public ProductStatus Status { get; private set; }

        public ProductDetail Detail { get; private set; }

        public Product(string name, int? warehouseId, ProductDetail detail, string createdBy) : base(createdBy)
        {
            Name = name;
            WarehouseId = warehouseId;
            _statusId = ProductStatus.ForSale.Id;
            Detail = detail;
            _photos = new List<ProductPhoto>();
        }

        public void AddProductPhoto(ProductPhoto photo)
        {
            if (Photos.Count > 5)
                throw new DomainException("Cannot add more than 5 photos");

            _photos.Add(photo);
        }

        public void Suspend()
        {
            if (_statusId == ProductStatus.ForSale.Id)
            {
                _statusId = ProductStatus.Suspended.Id;
            }
        }

        public void SetName(string name)
        {
            Name = name;
        }
    }
}
