﻿using System;
using DDD.Domain.Seedwork;

namespace DDD.Domain.Aggregates.ProductAggregate
{
    public class ProductPhoto : Entity
    {
        public string Source { get; private set; }

        public ProductPhoto(string source, string createdBy) : base(createdBy)
        {
            if (!IsValidSource(source))
                throw new Exception();

            Source = source;
        }

        private bool IsValidSource(string source)
        {
            return true;
        }
    }
}
