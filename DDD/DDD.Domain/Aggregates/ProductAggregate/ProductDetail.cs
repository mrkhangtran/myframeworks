﻿using System.Collections.Generic;
using DDD.Domain.Seedwork;

namespace DDD.Domain.Aggregates.ProductAggregate
{
    public class ProductDetail : ValueObject
    {
        public int Width  { get; private set; }

        public int Height { get; private set; }

        public ProductDetail(int width, int height)
        {
            Width = width;
            Height = height;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Width;
            yield return Height;
        }
    }
}
