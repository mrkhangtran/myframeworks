﻿using DDD.Domain.Seedwork;

namespace DDD.Domain.Aggregates.ProductAggregate
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
