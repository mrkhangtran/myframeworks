﻿using DDD.Domain.Seedwork;

namespace DDD.Domain.Aggregates.WarehouseAggregate
{
    public interface IWarehouseRepository : IRepository<Warehouse>
    {
    }
}
