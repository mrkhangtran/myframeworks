﻿using System;
using DDD.Domain.Events;
using DDD.Domain.Seedwork;

namespace DDD.Domain.Aggregates.WarehouseAggregate
{
    public class Warehouse : AggregateRoot
    {
        public string Name { get; private set; }

        public Warehouse(string name, string createdBy) : base(createdBy)
        {
            Name = name;
        }

        public void SetName(string name, string updatedBy)
        {
            Name = name;
            Audit(updatedBy);

            AddDomainEvent(new WarehouseNameChangedDomainEvent(Name, Id));
        }
    }
}
