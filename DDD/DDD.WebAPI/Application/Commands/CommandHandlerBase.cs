﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DDD.WebAPI.Infrastructure;
using MediatR;

namespace DDD.WebAPI.Application.Commands
{
    public abstract class CommandHandlerBase<TCommand, TResult> : IRequestHandler<TCommand, TResult> 
        where TCommand : IRequest<TResult>
    {
        protected RequestContext RequestContext;

        protected CommandHandlerBase(RequestContext requestContext)
        {
            RequestContext = requestContext;
        }

        public abstract Task<TResult> Handle(TCommand request, CancellationToken cancellationToken);
    }
}
