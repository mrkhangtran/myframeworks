﻿using System.Collections.Generic;
using MediatR;

namespace DDD.WebAPI.Application.Commands.ProductCommands
{
    public class CreateProductCommand : IRequest<bool>
    {
        public string Name { get; }

        public int? WarehouseId { get; set; }

        public int Width { get; }

        public int Height { get; }

        public IEnumerable<ProductPhotoDto> Photos { get; }

        public CreateProductCommand(string name, int width, int height, List<ProductPhotoDto> photos)
        {
            Name = name;
            Width = width;
            Height = height;
            Photos = photos;
        }

        public class ProductPhotoDto
        {
            public string Source { get; set; }
        }
    }
}
