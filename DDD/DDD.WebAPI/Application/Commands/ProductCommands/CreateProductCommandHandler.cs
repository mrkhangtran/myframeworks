﻿using System.Threading;
using System.Threading.Tasks;
using DDD.Domain.Aggregates.ProductAggregate;
using DDD.Infrastructure.Idempotency;
using DDD.WebAPI.Infrastructure;
using MediatR;

namespace DDD.WebAPI.Application.Commands.ProductCommands
{
    public class CreateProductCommandHandler : CommandHandlerBase<CreateProductCommand, bool>
    {
        private readonly IProductRepository _productRepository;

        public CreateProductCommandHandler(IProductRepository productRepository, RequestContext requestContext) 
            : base(requestContext)
        {
            _productRepository = productRepository;
        }

        public override async Task<bool> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var product = new Product(request.Name, request.WarehouseId, new ProductDetail(request.Width, request.Height), RequestContext.Principal.Username);
            foreach (var photo in request.Photos)
            {
                product.AddProductPhoto(new ProductPhoto(photo.Source, RequestContext.Principal.Username));
            }

            await _productRepository.InsertAsync(product);
            await _productRepository.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }

    public class CreateProductIdentifiedCommandHandler : IdentifiedCommandHandler<CreateProductCommand, bool>
    {
        public CreateProductIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicatedRequest()
        {
            return true; // Ignore duplicate requests.
        }
    }
}

