﻿using MediatR;

namespace DDD.WebAPI.Application.Commands.WarehouseCommands
{
    public class UpdateWarehouseCommand : IRequest<bool>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string RowVersion { get; set; }
    }
}
