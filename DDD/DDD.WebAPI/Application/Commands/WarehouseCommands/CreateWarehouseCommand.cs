﻿using MediatR;

namespace DDD.WebAPI.Application.Commands.WarehouseCommands
{
    public class CreateWarehouseCommand : IRequest<bool>
    {
        public string Name { get; set; }
    }
}
