﻿using System.Threading;
using System.Threading.Tasks;
using DDD.Domain.Aggregates.WarehouseAggregate;
using DDD.Infrastructure.Idempotency;
using DDD.WebAPI.Infrastructure;
using MediatR;

namespace DDD.WebAPI.Application.Commands.WarehouseCommands
{
    public class UpdateWarehouseCommandHandler : CommandHandlerBase<UpdateWarehouseCommand, bool>
    {
        private readonly IWarehouseRepository _warehouseRepo;

        public UpdateWarehouseCommandHandler(IWarehouseRepository warehouseRepo, RequestContext requestContext) : base(requestContext)
        {
            _warehouseRepo = warehouseRepo;
        }

        public override async Task<bool> Handle(UpdateWarehouseCommand request, CancellationToken cancellationToken)
        {
            var warehouse = await _warehouseRepo.GetByIdAsync(request.Id);
            warehouse.SetRowVersion(request.RowVersion);

            warehouse.SetName(request.Name, RequestContext.Principal.Username);
            await _warehouseRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }

    public class ChangeWarehouseNameIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateWarehouseCommand, bool>
    {
        public ChangeWarehouseNameIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicatedRequest()
        {
            return true; // Ignore duplicate requests for creating order.
        }
    }
}
