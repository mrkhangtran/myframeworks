﻿using System.Threading;
using System.Threading.Tasks;
using DDD.Domain.Aggregates.WarehouseAggregate;
using DDD.Infrastructure.Idempotency;
using DDD.WebAPI.Infrastructure;
using MediatR;

namespace DDD.WebAPI.Application.Commands.WarehouseCommands
{
    public class CreateWarehouseCommandHandler : CommandHandlerBase<CreateWarehouseCommand, bool>
    {
        private readonly IWarehouseRepository _warehouseRepo;

        public CreateWarehouseCommandHandler(IWarehouseRepository warehouseRepo, RequestContext requestContext) : base(requestContext)
        {
            _warehouseRepo = warehouseRepo;
        }

        public override async Task<bool> Handle(CreateWarehouseCommand request, CancellationToken cancellationToken)
        {
            var warehouse = new Warehouse(request.Name, RequestContext.Principal.Username);
            await _warehouseRepo.InsertAsync(warehouse);
            await _warehouseRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return true;
        }
    }

    public class CreateWarehouseIdentifiedCommandHandler : IdentifiedCommandHandler<CreateWarehouseCommand, bool>
    {
        public CreateWarehouseIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicatedRequest()
        {
            return true; // Ignore duplicate requests for creating order.
        }
    }
}
