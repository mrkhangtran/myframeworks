﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDD.WebAPI.Application.Commands.WarehouseCommands;
using FluentValidation;

namespace DDD.WebAPI.Application.Validators
{
    public class UpdateWarehouseCommandValidator : AbstractValidator<UpdateWarehouseCommand>
    {
        public UpdateWarehouseCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.RowVersion).NotEmpty();
        }
    }
}
