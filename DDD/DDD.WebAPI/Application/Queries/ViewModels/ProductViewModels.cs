﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDD.WebAPI.Application.Queries.ViewModels
{
    public class ProductViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public string Warehouse { get; set; }

        public List<ProductPhotoViewModel> Photos { get; set; }

        public string RowVersion { get; set; }
    }

    public class ProductPhotoViewModel
    {
        public string Id { get; set; }

        public string Source { get; set; }

        public string RowVersion { get; set; }
    }

    public class ProductSummaryViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
