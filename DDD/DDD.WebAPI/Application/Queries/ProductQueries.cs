﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DDD.Common.Helpers;
using DDD.WebAPI.Application.Queries.ViewModels;

namespace DDD.WebAPI.Application.Queries
{
    public class ProductQueries : IProductQueries
    {
        private readonly string _connectionString;

        public ProductQueries(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<ProductViewModel> GetProductAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryAsync<dynamic>(
                    @"SELECT p.*, pp.Source as PhotoSource, pp.RowVersion as PhotoRowVersion, ps.Name as StatusName, w.Name as WareHouseName
                    FROM Products p
	                     LEFT JOIN ProductPhotos pp ON p.Id = pp.ProductId
	                     LEFT JOIN ProductStatuses ps ON ps.Id = p.StatusId
	                     LEFT JOIN Warehouses w ON w.Id = p.Id
                    WHERE p.Id = @id"
                    , new { id }
                );

                if (!result.Any())
                    return null;

                return MapToProductViewModel(result);
            }
        }

        public Task<IEnumerable<ProductSummaryViewModel>> GetProductsAsync()
        {
            throw new System.NotImplementedException();
        }

        private ProductViewModel MapToProductViewModel(dynamic result)
        {
            var product = new ProductViewModel
            {
                Id = result[0].Id,
                Name = result[0].Name,
                Status = result[0].StatusName,
                Warehouse = result[0].WareHouseName,
                Photos = new List<ProductPhotoViewModel>(),
                RowVersion = ByteArrayConverter.ToString(result.RowVersion)
            };

            foreach (var record in result)
            {
                var photo = new ProductPhotoViewModel
                {
                    Source = record.PhotoSource,
                    RowVersion = ByteArrayConverter.ToString(result.PhotoRowVersion)
                };

                product.Photos.Add(photo);
            }

            return product;
        }
    }
}
