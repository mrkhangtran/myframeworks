﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DDD.WebAPI.Application.Queries.ViewModels;

namespace DDD.WebAPI.Application.Queries
{
    public interface IProductQueries
    {
        Task<ProductViewModel> GetProductAsync(int id);

        Task<IEnumerable<ProductSummaryViewModel>> GetProductsAsync();
    }
}
