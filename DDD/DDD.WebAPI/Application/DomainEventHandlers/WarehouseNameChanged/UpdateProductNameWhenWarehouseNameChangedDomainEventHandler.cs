﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DDD.Domain.Aggregates.ProductAggregate;
using DDD.Domain.Events;
using MediatR;

namespace DDD.WebAPI.Application.DomainEventHandlers.WarehouseNameChanged
{
    public class UpdateProductNameWhenWarehouseNameChangedDomainEventHandler
        : INotificationHandler<WarehouseNameChangedDomainEvent>
    {
        private readonly IProductRepository _productRepository;

        public UpdateProductNameWhenWarehouseNameChangedDomainEventHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task Handle(WarehouseNameChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            var products = await _productRepository.QueryAsync(x => x.WarehouseId.GetValueOrDefault() == notification.WarehouseId);
            foreach (var product in products)
            {
                product.SetName(product.Name + notification.WarehouseName);
            }
        }
    }
}
