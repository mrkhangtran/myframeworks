﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDD.WebAPI.Application.Commands;
using DDD.WebAPI.Application.Commands.WarehouseCommands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DDD.WebAPI.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    public class WarehousesController : Controller
    {
        private readonly IMediator _mediator;

        public WarehousesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> CreateProduct([FromBody]CreateWarehouseCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            bool commandResult = false;
            if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
            {
                var rq = new IdentifiedCommand<CreateWarehouseCommand, bool>(command, guid);
                commandResult = await _mediator.Send(rq);
            }

            return commandResult ? (IActionResult)Ok() : BadRequest();
        }
    }
}
