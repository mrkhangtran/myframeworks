﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDD.WebAPI.Application.Commands;
using DDD.WebAPI.Application.Commands.ProductCommands;
using DDD.WebAPI.Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DDD.WebAPI.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IProductQueries _productQueries;

        public ProductsController(IProductQueries productQueries, IMediator mediator)
        {
            _productQueries = productQueries;
            _mediator = mediator;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<IActionResult> GetProduct(int id)
        {
            var product = await _productQueries.GetProductAsync(id);
            if (product == null)
                return NotFound();

            return Json(product);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProduct([FromBody]CreateProductCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            bool commandResult = false;
            if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
            {
                var rq = new IdentifiedCommand<CreateProductCommand, bool>(command, guid);
                commandResult = await _mediator.Send(rq);
            }

            return commandResult ? (IActionResult) Ok() : BadRequest();
        }
    }
}
