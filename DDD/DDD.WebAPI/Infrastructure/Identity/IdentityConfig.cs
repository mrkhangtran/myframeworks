﻿using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace DDD.WebAPI.Infrastructure.Identity
{
    public class IdentityConfig
    {
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource> { new ApiResource("myapi", "My API") };
        }

        public static IEnumerable<Client> GetClients(IConfiguration config)
        {
            var spaUrl = config.GetSection("Client:SpaUrl").Value;

            return new List<Client>
            {
                new Client
                {
                    ClientName = "spa",
                    ClientId = "spa",
                    ClientSecrets = {new Secret("secret".Sha256())},
                    RequireConsent = false,
                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,
                    RedirectUris = {$"{spaUrl}/signin-oidc"},
                    PostLogoutRedirectUris = {$"{spaUrl}/signout-callback-oidc"},
                    AccessTokenLifetime = 36000, // 10 hours

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "myapi"
                    },
                    UpdateAccessTokenClaimsOnRefresh = true,
                    AllowOfflineAccess = true
                }
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static List<TestUser> GetUsers(IHostingEnvironment env)
        {
            if (!env.IsDevelopment())
            {
                return new List<TestUser>();
            }

            return new List<TestUser> { new TestUser { SubjectId = "1", Username = "apitest", Password = "1234" } };
        }
    }
}
