﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDD.WebAPI.Infrastructure
{
    public class RequestContext
    {
        public RequestPrincipal Principal { get; } = new RequestPrincipal();
    }

    public class RequestPrincipal
    {
        public string Username { get; set; }
    }
}
