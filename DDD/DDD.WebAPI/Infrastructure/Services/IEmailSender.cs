﻿using System.Threading.Tasks;

namespace DDD.WebAPI.Infrastructure.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
