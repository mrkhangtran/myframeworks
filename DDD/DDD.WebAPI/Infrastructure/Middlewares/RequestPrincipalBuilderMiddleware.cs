﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DDD.WebAPI.Infrastructure.Middlewares
{
    public class RequestPrincipalBuilderMiddleware
    {
        private readonly RequestDelegate _next;

        public RequestPrincipalBuilderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var ctx = (RequestContext)context.RequestServices.GetService(typeof(RequestContext));

            // TODO fake this for now
            ctx.Principal.Username = "admin";

            await _next.Invoke(context);
        }
    }
}
