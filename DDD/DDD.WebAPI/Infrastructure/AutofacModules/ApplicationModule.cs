﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using DDD.Domain.Aggregates.ProductAggregate;
using DDD.Domain.Aggregates.WarehouseAggregate;
using DDD.Infrastructure.Idempotency;
using DDD.Infrastructure.Repositories;
using DDD.WebAPI.Application.Queries;

namespace DDD.WebAPI.Infrastructure.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        public string QueriesConnectionString { get; }

        public ApplicationModule(string qconstr)
        {
            QueriesConnectionString = qconstr;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new ProductQueries(QueriesConnectionString))
                .As<IProductQueries>()
                .InstancePerLifetimeScope();

            builder.RegisterType<WarehouseRepository>()
                .As<IWarehouseRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProductRepository>()
                .As<IProductRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<RequestContext>().AsSelf().InstancePerLifetimeScope();
        }
    }
}
