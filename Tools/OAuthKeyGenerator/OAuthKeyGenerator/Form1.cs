﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Owin.Security.DataHandler.Encoder;

namespace OAuthKeyGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Generate();
        }

        private void Generate()
        {
            var clientId = Guid.NewGuid().ToString("N");

            txtAudienceId.Text = clientId;

            var key = new byte[32];
            RNGCryptoServiceProvider.Create().GetBytes(key);
            var base64Secret = TextEncodings.Base64Url.Encode(key);

            txtAudienceSecret.Text = base64Secret;
        }
    }
}
