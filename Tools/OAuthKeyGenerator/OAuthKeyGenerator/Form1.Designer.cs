﻿namespace OAuthKeyGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtAudienceId = new System.Windows.Forms.TextBox();
            this.txtAudienceSecret = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Audience ID";
            // 
            // txtAudienceId
            // 
            this.txtAudienceId.Location = new System.Drawing.Point(104, 97);
            this.txtAudienceId.Name = "txtAudienceId";
            this.txtAudienceId.Size = new System.Drawing.Size(303, 20);
            this.txtAudienceId.TabIndex = 1;
            // 
            // txtAudienceSecret
            // 
            this.txtAudienceSecret.Location = new System.Drawing.Point(104, 123);
            this.txtAudienceSecret.Name = "txtAudienceSecret";
            this.txtAudienceSecret.Size = new System.Drawing.Size(303, 20);
            this.txtAudienceSecret.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Audience Secret";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 261);
            this.Controls.Add(this.txtAudienceSecret);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAudienceId);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAudienceId;
        private System.Windows.Forms.TextBox txtAudienceSecret;
        private System.Windows.Forms.Label label2;
    }
}

