﻿using Microsoft.EntityFrameworkCore;
using Logistics = ModularMono.Modules.Logistics.DbMigrations;

// The runner for applying migrations on target environment
Console.WriteLine("Applying Migrations");

await using var ctx = new Logistics.LogisticsDbContextFactory().CreateDbContext(null);
await ctx.Database.MigrateAsync();

await using var ctx_1 = new Logistics.IntegrationOutboxDbContextFactory().CreateDbContext(null);
await ctx_1.Database.MigrateAsync();

Console.WriteLine("Finished!!!");
Console.Read();