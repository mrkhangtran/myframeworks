﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.Query
{
    public class IdNameModel<TId>
    {
        public IdNameModel(TId id, string name)
        {
            Id = id;
            Name = name;
        }

        public TId Id { get; set; }

        public string Name { get; set; }
    }
}
