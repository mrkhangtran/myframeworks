﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.Query
{
    public class DropdownItem<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
    }
}