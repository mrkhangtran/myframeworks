﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ModularMono.Shared;

namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.Query
{
    public interface IPaginator
    {
        Task<PaginatedResult<TSource>> CreateAsync<TSource>(IQueryable<TSource> source, QueryOption option);

        Task<PaginatedResult<TDes>> CreateAsync<TDes, TSource>(IQueryable<TSource> source, QueryOption option,
            IMapper mapper);

        PaginatedResult<TDes> Map<TDes, TSource>(PaginatedResult<TSource> source, IMapper mapper);
    }
}
