﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.Query
{
    public class QueryOption
    {
        public int PageSize { get; set; } = 10;
        public int PageNumber { get; set; } = 1;
        public bool IsTakeAll { get; set; }

        /// <summary>
        /// "Field1 asc, Field2, Field3 desc "
        /// </summary>
        public string Sort { get; set; }

        /// <summary>
        /// "Field1 eq 123;... Some supported operators: eq, lt, gt, le, ge, .Contains(), ..."
        /// </summary>
        public string Filter { get; set; }
    }
}
