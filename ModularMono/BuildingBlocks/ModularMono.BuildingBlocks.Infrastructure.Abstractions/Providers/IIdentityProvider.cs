﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers
{
    public interface IIdentityProvider
    {
        Task<User?> FindByUsernameAsync(string username);
        Task<Abstractions.Providers.User?> FindByUserIdAsync(Guid userId);
    }

    public class User
    {
        public required Guid Id { get; init; }

        public required string Username { get; init; }

        public required bool IsLocked { get; init; }
    }
}
