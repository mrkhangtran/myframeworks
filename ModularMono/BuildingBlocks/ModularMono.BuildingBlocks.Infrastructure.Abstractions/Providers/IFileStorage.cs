﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers
{
    public interface IFileStorage
    {
        Task<FileResult> WriteAsync(byte[] content, string address);

        Task<FileResult> WriteAsync(string content, string address);

        Task<FileResult> ReadAsync(string address);

        Task DeleteAsync(string address);
    }

    public class FileResult
    {
        public required string FileName { get; init; }
        public required string Address { get; init; }
        public string MimeType => GetContentTypeFile(Path.GetExtension(FileName));
        public required byte[] Content { get; init; }

        private string GetContentTypeFile(string fileType)
        {
            var result = string.Empty;
            if (string.IsNullOrWhiteSpace(result))
            {
                switch (fileType.ToLower())
                {
                    case ".doc":
                    case ".docx":
                        result = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        break;
                    case ".pdf":
                        result = System.Net.Mime.MediaTypeNames.Application.Pdf;
                        break;
                    case ".html":
                        result = System.Net.Mime.MediaTypeNames.Text.Html;
                        break;
                    case ".txt":
                        result = System.Net.Mime.MediaTypeNames.Text.Plain;
                        break;
                    case ".gif":
                        result = System.Net.Mime.MediaTypeNames.Image.Gif;
                        break;
                    case ".tiff":
                        result = System.Net.Mime.MediaTypeNames.Image.Tiff;
                        break;
                    case ".jpeg":
                    case ".jpg":
                        result = System.Net.Mime.MediaTypeNames.Image.Jpeg;
                        break;
                    case ".png":
                        result = "image/png";
                        break;
                    default:
                        break;
                }
            }
            return result;
        }
    }
}
