﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers
{
    public interface IConnectionStringProvider
    {
        string Get(string name);
    }
}
