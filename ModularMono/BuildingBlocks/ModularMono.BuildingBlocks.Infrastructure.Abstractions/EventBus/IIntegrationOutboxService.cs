﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus
{
	public interface IIntegrationOutboxService
	{
        Task<IEnumerable<IntegrationOutboxEntry>> RetrievePendingEventsAsync(string? transactionId = null);

        Task AddEventAsync(IntegrationEvent @event);

        Task MarkEventAsSentAsync(Guid eventId);
        Task MarkEventAsFailedAsync(Guid eventId, string reason);
    }
}

