﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus
{
    public interface IEventBus : IDisposable
    {
        Task PublishAsync<T>(T @event)
            where T : IntegrationEvent;

        void Subscribe<T>(IIntegrationEventHandler<T> handler)
            where T : IntegrationEvent;
    }
}
