﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus
{
    public class EventSubscriptionInfo
    {
        public Type HandlerType { get; }

        public EventSubscriptionInfo(Type handlerType)
        {
            HandlerType = handlerType;
        }
    }
}
