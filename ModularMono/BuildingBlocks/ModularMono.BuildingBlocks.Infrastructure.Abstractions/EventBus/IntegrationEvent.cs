﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus
{
    public abstract record IntegrationEvent
    {
        public Guid Id { get; } = Guid.NewGuid();

        public DateTime OccurredDate { get; } = DateTime.UtcNow;

        public required Guid OccurredBy { get; init; }
    }
}
