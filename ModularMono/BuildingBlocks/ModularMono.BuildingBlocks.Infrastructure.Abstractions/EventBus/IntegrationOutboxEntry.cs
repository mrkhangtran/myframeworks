﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus
{
    public class IntegrationOutboxEntry
    {
        public Guid EventId { get; private set; }

        public DateTime OccurredDate { get; private set; }

        public Guid OccurredBy { get; private set; }

        public string EventType { get; private set; }

        public string EventData { get; private set; }

        public DateTime? SentDate { get; set; }

        public string TransactionId { get; private set; }

        public string? FailedReason { get; set; }

        private IntegrationOutboxEntry() { }

        public IntegrationOutboxEntry(IntegrationEvent @event, string transactionId)
        {
            EventId = @event.Id;
            OccurredDate = @event.OccurredDate;
            OccurredBy = @event.OccurredBy;
            EventType = @event.GetType().AssemblyQualifiedName!;
            EventData = JsonSerializer.Serialize(@event, @event.GetType(), new JsonSerializerOptions
            {
                WriteIndented = true
            });

            TransactionId = transactionId;
        }

        public IntegrationEvent DeserializeJsonContent(Type type)
        {
            return (JsonSerializer.Deserialize(EventData, type, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true }) as IntegrationEvent)!;
        }
    }
}

