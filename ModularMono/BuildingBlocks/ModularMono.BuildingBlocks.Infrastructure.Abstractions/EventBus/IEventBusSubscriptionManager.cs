﻿namespace ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus
{
    public interface IEventBusSubscriptionManager
    {
        void AddSubscription<T>(IIntegrationEventHandler<T> handler)
            where T : IntegrationEvent;

        bool HasSubscriptionsForEvent<T>() where T : IntegrationEvent;

        bool HasSubscriptionsForEvent(string eventName);

        IEnumerable<IIntegrationEventHandler> GetHandlersForEvent<T>() where T : IntegrationEvent;

        IEnumerable<IIntegrationEventHandler> GetHandlersForEvent(string eventName);

        string GetEventKey<T>();
    }
}
