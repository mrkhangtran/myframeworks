﻿using System.Diagnostics;
using MediatR;
using Microsoft.Extensions.Logging;
using ModularMono.Shared;

namespace ModularMono.BuildingBlocks.Mediatr.Behaviors
{
    public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly Stopwatch _timer;
        private readonly ILogger<LoggingBehavior<TRequest, TResponse>> _logger;
        private readonly IExecutionContextAccessor _appContext;

        public LoggingBehavior(ILogger<LoggingBehavior<TRequest, TResponse>> logger, IExecutionContextAccessor appContext)
        {
            _timer = new Stopwatch();
            _logger = logger;
            _appContext = appContext;
        }

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            _logger.LogDebug("---- Handling {Command} for {User} ({@CommandData}) (CorrelationId: {CorrelationId})", 
                typeof(TRequest).FullName, _appContext.Username, request, _appContext.RequestId);

            _timer.Start();
            var response = await next();
            _timer.Stop();

            _logger.LogDebug("---- Finished handling {Command} ({Duration} milliseconds) for {User} ({@CommandData}) (CorrelationId: {CorrelationId})",
                typeof(TRequest).FullName, _timer.ElapsedMilliseconds, _appContext.Username, request, _appContext.RequestId);

            return response;
        }
    }
}
