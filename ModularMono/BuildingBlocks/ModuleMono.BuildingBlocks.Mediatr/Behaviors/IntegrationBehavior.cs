﻿using System.Text.Json;
using System.Transactions;
using MediatR;
using Microsoft.Extensions.Logging;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;
using ModularMono.BuildingBlocks.Mediatr.Abstractions;

namespace ModularMono.BuildingBlocks.Mediatr.Behaviors
{
    public class IntegrationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : ICommand<TResponse>
    {
        private readonly IEventBus _eventBus;
        private readonly IIntegrationOutboxService _outbox;
        private readonly ILogger<IntegrationBehavior<TRequest, TResponse>> _logger;

        public IntegrationBehavior(IEventBus eventBus, IIntegrationOutboxService outbox, ILogger<IntegrationBehavior<TRequest, TResponse>> logger)
        {
            _eventBus = eventBus;
            _outbox = outbox;
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            var response = await next();

            var transactionInfo = Transaction.Current!.TransactionInformation;
            var transactionId = transactionInfo.LocalIdentifier;

            var entries = await _outbox.RetrievePendingEventsAsync(transactionId);
            foreach (var entry in entries)
            {
                _logger.LogInformation("----- Publishing integration event: {IntegrationEventId} - ({@IntegrationEvent})", entry.EventId, entry.EventData);

                try
                {
                    var ev = (IntegrationEvent)JsonSerializer.Deserialize(entry.EventData, Type.GetType(entry.EventType)!,
                        new JsonSerializerOptions { PropertyNameCaseInsensitive = true })!;

                    await _eventBus.PublishAsync(ev);
                    await _outbox.MarkEventAsSentAsync(entry.EventId);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "ERROR publishing integration event: {IntegrationEventId}", entry.EventId);

                    await _outbox.MarkEventAsFailedAsync(entry.EventId, ex.Message);
                }
            }

            return response;
        }
    }
}
