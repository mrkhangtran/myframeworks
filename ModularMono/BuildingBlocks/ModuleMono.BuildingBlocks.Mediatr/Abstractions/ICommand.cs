﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularMono.BuildingBlocks.Mediatr.Abstractions
{
    public interface ICommand<out T> : IRequest<T>
    {
    }

    public interface ICommand : IRequest<Unit>
    {
    }
}
