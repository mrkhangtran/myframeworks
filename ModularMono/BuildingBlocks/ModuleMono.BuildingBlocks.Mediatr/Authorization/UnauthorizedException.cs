﻿namespace ModularMono.BuildingBlocks.Mediatr.Authorization
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException(string? message)
            : base(message)
        {
        }
    }
}
