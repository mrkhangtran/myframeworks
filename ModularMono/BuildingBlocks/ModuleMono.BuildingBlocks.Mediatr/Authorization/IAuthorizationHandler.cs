﻿using MediatR;

namespace ModularMono.BuildingBlocks.Mediatr.Authorization
{
    interface IAuthorizationHandler<in TRequest> : IRequestHandler<TRequest, AuthorizationResult>
        where TRequest : IRequest<AuthorizationResult>
    { }
}
