﻿namespace ModularMono.BuildingBlocks.Mediatr.Authorization.Requirements
{
    public class MustHaveRolesRequirement : IAuthorizationRequirement
    {
        public required string UserId { get; init; }

        public required IEnumerable<string> Roles { get; init; }

        class Handler : IAuthorizationHandler<MustHaveRolesRequirement>
        {
            public async Task<AuthorizationResult> Handle(MustHaveRolesRequirement rq, CancellationToken cancellationToken)
            {
                // TODO: implement logic
                return AuthorizationResult.Succeed();
            }
        }
    }
}
