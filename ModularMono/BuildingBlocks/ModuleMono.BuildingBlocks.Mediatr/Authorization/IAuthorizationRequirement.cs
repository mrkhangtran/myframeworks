﻿using MediatR;

namespace ModularMono.BuildingBlocks.Mediatr.Authorization
{
    public interface IAuthorizationRequirement : IRequest<AuthorizationResult>
    {
    }
}
