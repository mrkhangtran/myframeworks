﻿namespace ModularMono.BuildingBlocks.Mediatr.Authorization
{
    public interface IAuthorizer<in T>
    {
        IEnumerable<IAuthorizationRequirement> Requirements { get; }
        void BuildPolicy(T instance);
    }
}
