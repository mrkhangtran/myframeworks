﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;

namespace ModularMono.BuildingBlocks.Infrastructure.EventBus
{
    public class InProcessEventBus : IEventBus
    {
        private readonly IEventBusSubscriptionManager _subscriptionManager;

        public InProcessEventBus(IEventBusSubscriptionManager subscriptionManager)
        {
            _subscriptionManager = subscriptionManager;
        }

        public async Task PublishAsync<T>(T @event) where T : IntegrationEvent
        {
            var handlers = _subscriptionManager.GetHandlersForEvent(@event.GetType().AssemblyQualifiedName!);

            foreach (var handler in handlers)
            {
                var concreteType = typeof(IIntegrationEventHandler<>).MakeGenericType(@event.GetType());
                await (Task)concreteType.GetMethod(nameof(IIntegrationEventHandler<T>.Handle))!.Invoke(handler, new[] { @event });
            }
        }

        public void Subscribe<T>(IIntegrationEventHandler<T> handler) where T : IntegrationEvent
        {
            _subscriptionManager.AddSubscription(handler);
        }

        public void Dispose()
        { }
    }
}
