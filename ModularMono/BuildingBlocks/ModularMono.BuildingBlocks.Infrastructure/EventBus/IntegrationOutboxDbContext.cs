﻿using System;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;

namespace ModularMono.BuildingBlocks.Infrastructure.EventBus
{
	public class IntegrationOutboxDbContext : DbContext
    {
        private readonly IConnectionStringProvider _connectionStringProvider;
        private readonly string _schema;

        public DbSet<IntegrationOutboxEntry> OutboxEntries { get; set; }

        public IntegrationOutboxDbContext(DbContextOptions<IntegrationOutboxDbContext> options, string schema) : base(options)
        {
            _schema = schema;
        }

        public IntegrationOutboxDbContext(IConnectionStringProvider connectionStringProvider, string schema)
        {
            _connectionStringProvider = connectionStringProvider;
            _schema = schema;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured && !string.IsNullOrEmpty(_schema))
            {
                var connectionString = _connectionStringProvider.Get("DefaultConnection");
                optionsBuilder.EnableSensitiveDataLogging();
                optionsBuilder.UseInMemoryDatabase("BravoDb", options =>
                {

                });
                //optionsBuilder.UseSqlServer(connectionString,
                //    sqlOptions =>
                //    {
                //        sqlOptions.MigrationsHistoryTable("__EFMigrationsHistory", _schema);
                //    });
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema(_schema);

            builder.Entity<IntegrationOutboxEntry>(ConfigureIntegrationOutbox);
        }

        void ConfigureIntegrationOutbox(EntityTypeBuilder<IntegrationOutboxEntry> builder)
        {
            builder.ToTable("IntegrationOutbox");

            builder.HasKey(e => e.EventId);

            builder.Property(e => e.EventData)
                .IsRequired();

            builder.Property(e => e.EventType)
                .IsRequired();

            builder.Property(e => e.OccurredDate)
                .IsRequired();

            builder.Property(e => e.OccurredBy)
                .IsRequired();

            builder.Property(e => e.TransactionId)
                .IsRequired();
        }
    }
}

