﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;

namespace ModularMono.BuildingBlocks.Infrastructure.EventBus
{
    public class InMemoryEventBusSubscriptionManager : IEventBusSubscriptionManager
    {
        private static readonly Dictionary<string, List<IIntegrationEventHandler>> _handlers = new();

        public InMemoryEventBusSubscriptionManager()
        {
        }

        public void AddSubscription<T>(IIntegrationEventHandler<T> handler) where T : IntegrationEvent
        {
            var eventName = GetEventKey<T>();

            if (!HasSubscriptionsForEvent(eventName))
            {
                _handlers.Add(eventName, new List<IIntegrationEventHandler>());
            }

            if (_handlers[eventName].Any(s => s.GetType().FullName == handler.GetType().FullName))
            {
                throw new ArgumentException(
                    $"Handler Type {handler.GetType().FullName} are already registered for '{eventName}'", nameof(handler));
            }

            _handlers[eventName].Add(handler);
        }

        public bool HasSubscriptionsForEvent<T>() where T : IntegrationEvent
        {
            var key = GetEventKey<T>();
            return HasSubscriptionsForEvent(key);
        }

        public bool HasSubscriptionsForEvent(string eventName) => _handlers.ContainsKey(eventName);

        public IEnumerable<IIntegrationEventHandler> GetHandlersForEvent<T>() where T : IntegrationEvent
        {
            var key = GetEventKey<T>();
            return GetHandlersForEvent(key);
        }

        public IEnumerable<IIntegrationEventHandler> GetHandlersForEvent(string eventName) => _handlers[eventName];

        public string GetEventKey<T>() => typeof(T).AssemblyQualifiedName!;
    }
}
