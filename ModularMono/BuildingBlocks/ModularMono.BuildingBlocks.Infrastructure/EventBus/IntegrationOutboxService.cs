﻿using System;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;

namespace ModularMono.BuildingBlocks.Infrastructure.EventBus
{
	public class IntegrationOutboxService : IIntegrationOutboxService
    {
        private readonly IntegrationOutboxDbContext _dbContext;

		public IntegrationOutboxService(IntegrationOutboxDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<IntegrationOutboxEntry>> RetrievePendingEventsAsync(string? transactionId = null)
        {
            var entries = await _dbContext.OutboxEntries
                .OrderBy(x => x.OccurredDate)
                .Where(e => e.SentDate == null && (string.IsNullOrEmpty(transactionId) || e.TransactionId == transactionId))
                .ToListAsync();

            return entries;
        }

        public async Task AddEventAsync(IntegrationEvent @event)
        {
            var transactionInfo = Transaction.Current!.TransactionInformation;
            var transactionId = transactionInfo.LocalIdentifier;

            var entry = new IntegrationOutboxEntry(@event, transactionId);

            _dbContext.Add(entry);
            await _dbContext.SaveChangesAsync();
        }

        public async Task MarkEventAsSentAsync(Guid eventId)
        {
            var entry = await _dbContext.OutboxEntries.SingleOrDefaultAsync(x => x.EventId == eventId);
            if (entry == null)
            {
                throw new InvalidOperationException($"Event {eventId} does not exist");
            }

            entry.SentDate = DateTime.UtcNow;

            await _dbContext.SaveChangesAsync();
        }

        public async Task MarkEventAsFailedAsync(Guid eventId, string reason)
        {
            var entry = await _dbContext.OutboxEntries.SingleOrDefaultAsync(x => x.EventId == eventId);
            if (entry == null)
            {
                throw new InvalidOperationException($"Event {eventId} does not exist");
            }

            entry.FailedReason = reason;

            await _dbContext.SaveChangesAsync();
        }
    }
}

