﻿using Microsoft.EntityFrameworkCore;
using ModularMono.BuildingBlocks.Infrastructure.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;

namespace ModularMono.BuildingBlocks.Infrastructure.Identity
{
    public class IdentityDbContext : DbContext
    {
        private readonly IConnectionStringProvider _connectionStringProvider;

        internal DbSet<User> Users { get; set; }

        public IdentityDbContext(DbContextOptions<IntegrationOutboxDbContext> options) : base(options)
        {
        }

        public IdentityDbContext(IConnectionStringProvider connectionStringProvider)
        {
            _connectionStringProvider = connectionStringProvider;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured && _connectionStringProvider != null)
            {
                var connectionString = _connectionStringProvider.Get("DefaultConnection");
                optionsBuilder.UseSqlServer(connectionString,
                    sqlOptions =>
                    {
                        //sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                    });
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Identity.User>().ToTable("LoginUser", "Time");
        }
    }
}
