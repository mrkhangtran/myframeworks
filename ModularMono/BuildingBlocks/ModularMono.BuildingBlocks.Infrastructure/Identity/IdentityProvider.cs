﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;

namespace ModularMono.BuildingBlocks.Infrastructure.Identity
{
    public class IdentityProvider : IIdentityProvider
    {
        private readonly IdentityDbContext _dbContext;

        public IdentityProvider(IdentityDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Abstractions.Providers.User?> FindByUsernameAsync(string username)
        {
            var user = await _dbContext.Set<User>().SingleOrDefaultAsync(x => x.AzureId == username);
            if (user != null)
            {
                return new Abstractions.Providers.User
                {
                    Id = user.PersonId,
                    IsLocked = user.Locked,
                    Username = user.AzureId
                };
            }

            return null;
        }

        public async Task<Abstractions.Providers.User?> FindByUserIdAsync(Guid userId)
        {
            var user = await _dbContext.Set<User>().SingleOrDefaultAsync(x => x.PersonId == userId);
            if (user != null)
            {
                return new Abstractions.Providers.User
                {
                    Id = user.PersonId,
                    IsLocked = user.Locked,
                    Username = user.AzureId
                };
            }

            return null;
        }
    }
}
