﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularMono.BuildingBlocks.Infrastructure.Identity
{
    internal class User
    {
        [Key]
        public Guid PersonId { get; set; }

        public string AzureId { get; set; }

        public bool Locked { get; set; }
    }
}
