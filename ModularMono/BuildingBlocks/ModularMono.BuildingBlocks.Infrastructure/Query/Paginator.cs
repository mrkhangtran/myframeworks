﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DelegateDecompiler.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Query;
using ModularMono.Shared;

namespace ModularMono.BuildingBlocks.Infrastructure.Query
{
    public class Paginator : IPaginator
    {
        public async Task<PaginatedResult<TSource>> CreateAsync<TSource>(IQueryable<TSource> source, QueryOption option)
        {
            // Sorting
            if (!string.IsNullOrEmpty(option.Sort))
            {
                source = source.OrderBy(option.Sort);
            }

            // Filtering
            if (!string.IsNullOrEmpty(option.Filter))
            {
                source = source.Where(option.Filter);
            }

            var count = await source.CountAsync();
            if (option.IsTakeAll)
            {
                var allItems = await source.DecompileAsync().ToListAsync();
                return new PaginatedResult<TSource>(allItems, count, option.PageNumber, count);
            }
            var items = await source.Skip((option.PageNumber - 1) * option.PageSize).Take(option.PageSize).DecompileAsync().ToListAsync();
            return new PaginatedResult<TSource>(items, count, option.PageNumber, option.PageSize);
        }

        public async Task<PaginatedResult<TDes>> CreateAsync<TDes, TSource>(IQueryable<TSource> source, QueryOption option, IMapper mapper)
        {
            var result = await CreateAsync(source, option);
            var items = mapper.Map<IEnumerable<TSource>, IEnumerable<TDes>>(result.Items);
            return new PaginatedResult<TDes>(items, result.PageInfo.TotalElements, result.PageInfo.PageNumber, result.PageInfo.Size);
        }

        public PaginatedResult<TDes> Map<TDes, TSource>(PaginatedResult<TSource> source, IMapper mapper)
        {
            var items = mapper.Map<IEnumerable<TSource>, IEnumerable<TDes>>(source.Items);

            return new PaginatedResult<TDes>(items, source.PageInfo);
        }
    }
}
