﻿using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;

namespace ModularMono.BuildingBlocks.Infrastructure.FileStorages
{
    /// <summary>
    /// File system storage
    /// </summary>
    public class FileSystemStorage : IFileStorage
    {
        private readonly string _root;

        public FileSystemStorage(FileSystemStorageOption option)
        {
            if (option == null || string.IsNullOrEmpty(option.Root))
            {
                throw new InvalidOperationException($"Missing ${nameof(FileSystemStorage)} config: {nameof(FileSystemStorageOption.Root)} is required");
            }

            _root = option.Root;
        }

        public async Task<FileResult> WriteAsync(byte[] content, string address)
        {
            // validate
            if (string.IsNullOrEmpty(address))
            {
                throw new ArgumentNullException(nameof(address));
            }

            if (content == null)
            {
                throw new ArgumentNullException(nameof(content));
            }

            // create directory if not exists
            var folderPath = $"{_root}/{Path.GetDirectoryName(address)}";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            var fileName = GetValidFileName(Path.GetFileName(address));
            var fullPath = $"{folderPath}/{fileName}";
            await using (var fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
            {
                await fileStream.WriteAsync(content);
            }

            return new FileResult
            {
                FileName = fileName,
                Address = $"{Path.GetDirectoryName(address)}/{fileName}",
                Content = content
            };
        }

        public Task<FileResult> WriteAsync(string content, string address) =>
            WriteAsync(System.Text.Encoding.UTF8.GetBytes(content), address);

        public async Task<FileResult> ReadAsync(string address)
        {
            // validate
            if (string.IsNullOrEmpty(address))
            {
                throw new ArgumentNullException(nameof(address));
            }

            var fullPath = $"{_root}/{address}";

            if (!File.Exists(fullPath)) { throw new FileNotFoundException(); }

            var content = await File.ReadAllBytesAsync(fullPath);

            var result = new FileResult
            {
                FileName = Path.GetFileName(address),
                Address = address,
                Content = content
            };

            return result;
        }

        public Task DeleteAsync(string address)
        {
            // validate
            if (string.IsNullOrEmpty(address))
            {
                throw new ArgumentNullException(nameof(address));
            }

            var fullPath = $"{_root}/{address}";

            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }

            return Task.CompletedTask;
        }

        private string GetValidFileName(string fileName)
        {
            var invalidFileNameCharacterRegex = new Regex("[^abcdefghijklmnopqrstuvwxyzæøå0123456789_\\.\\s]+", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var validFileName = invalidFileNameCharacterRegex.Replace(fileName, "_").Trim();

            return $"{Path.GetFileNameWithoutExtension(validFileName)}_{DateTime.UtcNow.Ticks}{Path.GetExtension(validFileName)}";
        }
    }

    public class FileSystemStorageOption
    {
        public required string Root { get; init; }
    }
}
