﻿using MediatR;
using Microsoft.Extensions.Logging;
using ModularMono.Shared;

namespace ModularMono.Domain.Core.Decorators
{
    public class NotificationHandlerLoggingDecorator<T> : INotificationHandler<T> where T : INotification
    {
        private readonly INotificationHandler<T> _handler;
        private readonly ILogger<NotificationHandlerLoggingDecorator<T>> _logger;
        private readonly IExecutionContextAccessor _appContext;

        public NotificationHandlerLoggingDecorator(INotificationHandler<T> handler, ILogger<NotificationHandlerLoggingDecorator<T>> logger, IExecutionContextAccessor appContext)
        {
            _handler = handler;
            _logger = logger;
            _appContext = appContext;
        }

        public async Task Handle(T notification, CancellationToken cancellationToken)
        {
            var source = "";
            if (notification is DomainEvent domainEvent)
            {
                source = domainEvent.Source;
            }

            _logger.LogDebug("-- Handling event: {Event} (Source: {Source}) (CorrelationId: {CorrelationId})", notification.GetType().Name, source, _appContext.RequestId);

            await _handler.Handle(notification, cancellationToken);
        }
    }
}
