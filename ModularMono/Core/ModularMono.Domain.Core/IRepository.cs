﻿using Ardalis.Specification;

namespace ModularMono.Domain.Core
{
    public interface IRepository<T, TKey> where T : Entity<TKey>, IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }

        void Add(T entity);
        void AddRange(params T[] entities);

        /// <summary>
        /// Manually marking an entity as Modified state. 
        /// Useful when updating child entity without changing parent and we want to mark parent as Modified for concurrency check or audit.
        /// </summary>
        void MarkAsChanged(T entity);

        Task<T?> GetByIdAsync(Guid id);
        Task<IEnumerable<T>> ListAsync(ISpecification<T>? specification = null);
        Task<IEnumerable<T>> ListAsync(IEnumerable<TKey> ids);
        Task<int> CountAsync(ISpecification<T>? specification = null);
        Task<T?> SingleOrDefaultAsync(ISpecification<T>? specification = null);
        Task<T> SingleAsync(ISpecification<T>? specification = null);
        Task<T?> FirstOrDefaultAsync(ISpecification<T>? specification = null);
        Task<T> FirstAsync(ISpecification<T>? specification = null);
        void Remove(T entity);
    }
}
