﻿using System.Text.Json.Serialization;
using MediatR;

namespace ModularMono.Domain.Core
{
    public abstract class Entity<TKey>
    {
        /// <summary>
        /// Non-clustered PK if TKey is not int
        /// </summary>
        public TKey Id { get; protected set; }
    }

    public abstract class Entity : Entity<Guid>
    {
        /// <summary>
        /// Clustered identity column
        /// </summary>
        public int IdentityKey { get; private set; }

        [JsonIgnore]
        public bool IsTransient => IdentityKey == 0;

        private readonly List<INotification> _domainEvents = new();

        [JsonIgnore]
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents;

        public void AddDomainEvent(INotification @event)
        {
            _domainEvents.Add(@event);
        }

        public void RemoveDomainEvent(INotification @event)
        {
            _domainEvents.Remove(@event);
        }

        public void ClearDomainEvents()
        {
            _domainEvents.Clear();
        }
    }

    public abstract class SimpleEntity : Entity<int>
    {
    }
}
