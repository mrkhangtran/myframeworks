﻿using EnsureThat;
using MediatR;

namespace ModularMono.Domain.Core
{
    public abstract class DomainEvent : INotification
    {
        /// <summary>
        /// The source that triggers this event.
        /// Used for tracing.
        /// </summary>
        public string Source { get; }

        protected DomainEvent(string source)
        {
            Ensure.That(source, nameof(source)).IsNotNullOrEmpty();

            Source = source;
        }
    }
}
