﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ModularMono.Shared
{
    public interface IModule
    {
        string DefaultRoutePrefix { get; }

        IModuleStartup Startup { get; }
    }

    public interface IModuleStartup
    {
        void Initialize(IConfiguration configuration, IServiceCollection serviceCollection);
    }
}
