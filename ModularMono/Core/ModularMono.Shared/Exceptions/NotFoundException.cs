﻿namespace ModularMono.Shared.Exceptions
{
    public class NotFoundException : BusinessException
    {
        public NotFoundException(string entity) : base($"{entity} was not found")
        {
        }
    }
}
