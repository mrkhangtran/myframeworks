﻿namespace ModularMono.Shared.Exceptions
{
    public class DbConcurrencyException : Exception
    {
        public string LastUpdatedBy { get; set; } = string.Empty;

        public DbConcurrencyException() { }

        public DbConcurrencyException(string lastUpdatedBy) : base($"Data was updated by {lastUpdatedBy}")
        {
            LastUpdatedBy = lastUpdatedBy;
        }
    }
}
