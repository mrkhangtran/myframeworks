﻿namespace ModularMono.Shared
{
    public interface IExecutionContextAccessor
    {
        string Username { get; }

        string RequestId { get; }
    }
}
