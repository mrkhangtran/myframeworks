﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularMono.Shared
{
    public class PaginatedResult<T>
    {
        public IEnumerable<T> Items { get; set; }
        public PageInfo PageInfo { get; }

        public PaginatedResult(IEnumerable<T> items, PageInfo pageInfo)
        {
            Items = items;
            PageInfo = pageInfo;
        }

        public PaginatedResult(IEnumerable<T> items, int count, int pageNumber, int pageSize)
        {
            Items = items;
            PageInfo = new PageInfo(pageNumber, pageSize, count);
        }
    }

    public class PageInfo
    {
        public int PageNumber { get; set; }
        public int Size { get; set; }
        public int TotalElements { get; set; }
        public int TotalPages => (int)Math.Ceiling(TotalElements / (double)(Size == 0 ? TotalElements : Size));

        public PageInfo(int pageNumber, int size, int totalElements)
        {
            PageNumber = pageNumber;
            Size = size;
            TotalElements = totalElements;
        }
    }
}
