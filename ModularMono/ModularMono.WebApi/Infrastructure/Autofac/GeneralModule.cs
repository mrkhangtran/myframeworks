﻿using Autofac;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;
using ModularMono.BuildingBlocks.Infrastructure.ConnectionStrings;
using ModularMono.BuildingBlocks.Infrastructure.EventBus;
using ModularMono.BuildingBlocks.Infrastructure.FileStorages;
using ModularMono.BuildingBlocks.Infrastructure.Identity;
using ModularMono.Shared;

namespace ModularMono.WebApi.Infrastructure.Autofac
{
    public class GeneralModule : Module
    {
        private readonly IConfiguration _configuration;

        public GeneralModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            // Identity
            builder.RegisterType<IdentityDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<IdentityProvider>().As<IIdentityProvider>().InstancePerLifetimeScope();

            // Providers
            builder.RegisterType<AppSettingConnectionStringProvider>().As<IConnectionStringProvider>().SingleInstance();

            builder.RegisterType<FileSystemStorage>().As<IFileStorage>().SingleInstance();
            builder.Register(p => _configuration.GetSection(nameof(FileSystemStorage)).Get<FileSystemStorageOption>()).SingleInstance();

            // Execution Context
            builder.RegisterType<ExecutionContextAccessor>().As<IExecutionContextAccessor>().SingleInstance();
        }
    }
}
