﻿using System.Reflection;
using ModularMono.Shared;

namespace ModularMono.WebApi.Infrastructure.ModuleIntegration
{
    public class ModuleInfo
    {
        public string RoutePrefix { get; }

        public IModule Module { get; }

        public Assembly Assembly => Module.GetType().Assembly;

        public ModuleInfo(string routePrefix, IModule module)
        {
            RoutePrefix = routePrefix;
            Module = module;
        }
    }
}
