﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using ModularMono.Shared;

namespace ModularMono.WebApi.Infrastructure.ModuleIntegration
{
    public static class ModuleRegistration
    {
        public static IServiceCollection LoadModulePlugins(this IServiceCollection services,
            IConfiguration configuration)
        {
            configuration.GetSection("Plugins").GetChildren().ToList().ForEach(p =>
            {
                var path = p.GetValue<string>("Path");
                if (string.IsNullOrEmpty(path)) return;

                var assembly = Assembly.LoadFrom(path);
                var part = new AssemblyPart(assembly);

                services.AddControllers().ConfigureApplicationPartManager(apm => apm.ApplicationParts.Add(part));

                var pluginClass = assembly.GetTypes().SingleOrDefault(t => t.GetInterfaces().Any(m => m == typeof(IModule)));
                if (pluginClass == null) return;
                
                var module = (IModule) Activator.CreateInstance(pluginClass)!;
                var routePrefix = p.GetValue<string>("RoutePrefix") ?? module.DefaultRoutePrefix;
                services.AddSingleton(new ModuleInfo(routePrefix, module));
            });

            return services;
        }
    }
}
