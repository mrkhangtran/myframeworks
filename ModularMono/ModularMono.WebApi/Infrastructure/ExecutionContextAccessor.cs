﻿using ModularMono.BuildingBlocks.Infrastructure.Identity;
using System.Diagnostics;
using System.Security.Claims;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;
using ModularMono.Shared;

namespace ModularMono.WebApi.Infrastructure
{
    public class ExecutionContextAccessor : IExecutionContextAccessor
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public string Username
            => _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value
               ?? "test"; // throw new ApplicationException("User context is not available");

        public string RequestId { get; } = Activity.Current?.Id ?? Guid.NewGuid().ToString();

        public ExecutionContextAccessor(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
    }
}
