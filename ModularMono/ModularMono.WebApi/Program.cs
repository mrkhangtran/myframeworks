using Autofac;
using Serilog;
using Autofac.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ModularMono.WebApi.Infrastructure.ModuleIntegration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;
using ModularMono.BuildingBlocks.Infrastructure.ConnectionStrings;
using ModularMono.BuildingBlocks.Infrastructure.Identity;
using ModularMono.Shared;
using ModularMono.WebApi.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHttpContextAccessor();

// CORS
builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy",
        builder => builder
            .SetIsOriginAllowed((host) => true)
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
    );
});

// Serilog - this is the correct way instead of builder.Host.UseSerilog which causes problem with Application Insights (exception telemetry loss ...)
var logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    .CreateLogger();
builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);

// Autofac
builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Host.ConfigureContainer<ContainerBuilder>(container =>
{

});

// MVC
builder.Services.AddControllers(opts =>
{
    //opts.Filters.Add(typeof(GlobalExceptionFilter));
}).AddNewtonsoftJson(x =>
{
    x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
    x.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
    x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    x.SerializerSettings.Formatting = Formatting.Indented;
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Services
builder.Services.AddDbContext<IdentityDbContext>();
builder.Services.AddScoped<IIdentityProvider, IdentityProvider>();
builder.Services.AddSingleton<IExecutionContextAccessor, ExecutionContextAccessor>();
builder.Services.AddSingleton<IConnectionStringProvider, AppSettingConnectionStringProvider>();

// Modules
builder.Services.AddTransient<IPostConfigureOptions<MvcOptions>, ModuleRoutingMvcOptionsPostConfigure>();
builder.Services.LoadModulePlugins(builder.Configuration);

var app = builder.Build();

// Initialize modules
var modules = app.Services.GetRequiredService<IEnumerable<ModuleInfo>>();
foreach (var module in modules)
{
    module.Module.Startup.Initialize(builder.Configuration, builder.Services);
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors("CorsPolicy");
app.UseAuthorization();

app.MapControllers();

app.Run();
