﻿using Autofac;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.BuildingBlocks.Infrastructure.EventBus;

namespace ModularMono.Modules.Payroll.Configuration
{
    class EventBusModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Event bus
            builder.RegisterType<InProcessEventBus>()
                .As<IEventBus>()
                .SingleInstance();

            builder.RegisterType<InMemoryEventBusSubscriptionManager>()
                .As<IEventBusSubscriptionManager>()
                .SingleInstance();
        }
    }
}
