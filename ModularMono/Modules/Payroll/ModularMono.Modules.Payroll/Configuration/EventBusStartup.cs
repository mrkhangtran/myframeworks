﻿using Microsoft.Extensions.Logging;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using ModularMono.Modules.Payroll.EventHandlers;

namespace ModularMono.Modules.Payroll.Configuration
{
    internal static class EventBusStartup
    {
        internal static void Initialize()
        {
            SubscribeToIntegrationEvents();
        }

        private static void SubscribeToIntegrationEvents()
        {
            var eventBus = PayrollStartup.Container.Resolve<IEventBus>();

            eventBus.Subscribe(new ProductCodeChangedEventHandler());
        }
    }
}
