﻿using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using ModularMono.Modules.Payroll.Configuration;
using ModularMono.Shared;

namespace ModularMono.Modules.Payroll
{
    public class PayrollStartup : IModuleStartup
    {
        internal static IContainer Container { get; private set; }

        public void Initialize(IConfiguration configuration, IServiceCollection serviceCollection)
        {
            ConfigureContainer(configuration, serviceCollection);

            EventBusStartup.Initialize();
        }

        private void ConfigureContainer(IConfiguration configuration, IServiceCollection serviceCollection)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<EventBusModule>();

            builder.Populate(serviceCollection);

            Container = builder.Build();
        }
    }
}
