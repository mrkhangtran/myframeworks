﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using MediatR;
using Microsoft.Extensions.Logging;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;
using ModularMono.Modules.Logistics.IntegrationEvents;

namespace ModularMono.Modules.Payroll.EventHandlers
{
    internal class ProductCodeChangedEventHandler : IIntegrationEventHandler<ProductCodeChangedIntegrationEvent>
    {
        public async Task Handle(ProductCodeChangedIntegrationEvent @event)
        {
            await using var scope = PayrollStartup.Container.BeginLifetimeScope();

            var logger = scope.Resolve<ILogger<ProductCodeChangedEventHandler>>();
            logger.LogInformation(@event.NewCode);
        }
    }
}
