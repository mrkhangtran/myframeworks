﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using ModularMono.Shared;

namespace ModularMono.Modules.Payroll
{
    public class PayrollModule : IModule
    {
        public string DefaultRoutePrefix => "payroll";

        public IModuleStartup Startup => new PayrollStartup();
    }
}
