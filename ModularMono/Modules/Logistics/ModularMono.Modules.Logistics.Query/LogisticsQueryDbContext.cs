﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;
using ModularMono.Modules.Logistics.Application.Query.Interfaces;
using ModularMono.Modules.Logistics.Application.Query.DataModels;

namespace ModularMono.Modules.Logistics.Query
{
	public class LogisticsQueryDbContext : DbContext, ILogisticsQueryDbContext
	{
        public const string LOGISTICS_SCHEMA = "Logistics";

        private readonly IConnectionStringProvider _connectionStringProvider;

        public bool IncludeDeleted { get; set; }

        public LogisticsQueryDbContext()
		{
		}

        public LogisticsQueryDbContext(IConnectionStringProvider connectionStringProvider)
        {
            _connectionStringProvider = connectionStringProvider;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured && _connectionStringProvider != null)
            {
                var connectionString = _connectionStringProvider.Get("DefaultConnection");
                optionsBuilder.UseInMemoryDatabase("BravoDb", options =>
                {

                });
                //optionsBuilder.UseSqlServer(connectionString,
                //    sqlOptions =>
                //    {
                //        //sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                //    });
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Product>().ToTable(nameof(Product), LOGISTICS_SCHEMA);

            // Global
            ConfigureGlobal(modelBuilder);
        }

        private void ConfigureGlobal(ModelBuilder modelBuilder)
        {
            var types = modelBuilder.Model.GetEntityTypes();
            foreach (var type in types)
            {
                var configureMethodInfo = typeof(LogisticsQueryDbContext).GetMethod(nameof(ConfigureQueryFilter), BindingFlags.Instance | BindingFlags.NonPublic);
                configureMethodInfo!
                    .MakeGenericMethod(type.ClrType)
                    .Invoke(this, new object[] { modelBuilder });
            }
        }

        private void ConfigureQueryFilter<T>(ModelBuilder builder) where T : class
        {
            if (typeof(ISoftDeletable).IsAssignableFrom(typeof(T)))
            {
                Expression<Func<T, bool>> softDeleteFilter = e => IncludeDeleted || !((ISoftDeletable)e).IsDeleted;
                builder.Entity<T>().HasQueryFilter(softDeleteFilter);
            }
        }
    }
}

