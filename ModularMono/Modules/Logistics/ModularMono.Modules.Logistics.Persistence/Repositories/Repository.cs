﻿using System;
using System.Linq;
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using ModularMono.Domain.Core;

namespace ModularMono.Modules.Logistics.Persistence.Repositories
{
    public class Repository<T, TKey> : IRepository<T, TKey> where T : Entity<TKey>, IAggregateRoot
    {
        protected readonly LogisticsDbContext DbContext;

        public IUnitOfWork UnitOfWork => DbContext;

        public Repository(LogisticsDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public void Add(T entity)
        {
            DbContext.Set<T>().Add(entity);
        }

        public void AddRange(params T[] entities)
        {
            DbContext.Set<T>().AddRange(entities);
        }

        public void MarkAsChanged(T entity)
        {
            DbContext.Entry<T>(entity).State = EntityState.Modified;
        }

        public Task<int> CountAsync(ISpecification<T>? specification = null)
        {
            var query = ApplySpecification(specification);
            return query.CountAsync();
        }

        public Task<T?> GetByIdAsync(Guid id)
        {
            return DbContext.Set<T>().FindAsync(id).AsTask();
        }

        public async Task<IEnumerable<T>> ListAsync(ISpecification<T>? specification = null)
        {
            var query = ApplySpecification(specification);
            return await query.ToListAsync();
        }

        public async Task<IEnumerable<T>> ListAsync(IEnumerable<TKey> ids)
        {
            return await DbContext.Set<T>().Where(entity => ids.Contains(entity.Id)).ToListAsync();
        }

        public Task<T?> SingleOrDefaultAsync(ISpecification<T>? specification = null)
        {
            var query = ApplySpecification(specification);
            return query.SingleOrDefaultAsync();
        }

        public Task<T> SingleAsync(ISpecification<T>? specification = null)
        {
            var query = ApplySpecification(specification);
            return query.SingleAsync();
        }

        public Task<T?> FirstOrDefaultAsync(ISpecification<T>? specification = null)
        {
            var query = ApplySpecification(specification);
            return query.FirstOrDefaultAsync();
        }

        public Task<T> FirstAsync(ISpecification<T>? specification = null)
        {
            var query = ApplySpecification(specification);
            return query.FirstAsync();
        }

        public void Remove(T entity)
        {
            DbContext.Set<T>().Remove(entity);
        }

        private IQueryable<T> ApplySpecification(ISpecification<T>? spec)
        {
            return SpecificationEvaluator.Default.GetQuery(DbContext.Set<T>().AsQueryable(), spec);
        }
    }
}

