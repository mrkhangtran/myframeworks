﻿using System;
using ModularMono.Modules.Logistics.Domain.Entities.ProductAggregate;
namespace ModularMono.Modules.Logistics.Persistence.Repositories
{
	public class ProductRepository : Repository<Product, Guid>, IProductRepository
    {
        public ProductRepository(LogisticsDbContext dbContext) : base(dbContext)
        {
        }
    }
}

