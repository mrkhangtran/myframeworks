﻿using System;
using MediatR;
using ModularMono.Domain.Core;

namespace ModularMono.Modules.Logistics.Persistence
{
    static class MediatrExtensions
    {
        public static async Task DispatchDomainEventsAsync(this IMediator mediator, LogisticsDbContext ctx)
        {
            var domainEntities = ctx.ChangeTracker
                .Entries<Entity>()
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any())
                .ToList();

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .ToList();

            domainEntities.ToList()
                .ForEach(entity => entity.Entity.ClearDomainEvents());

            foreach (var domainEvent in domainEvents)
            {
                await mediator.Publish(domainEvent);
            }
        }
    }
}

