﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ModularMono.Modules.Logistics.Domain.Entities.ProductAggregate;

namespace ModularMono.Modules.Logistics.Persistence.EntityConfigurations
{
	public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            // table
            builder.ToTable(nameof(Product), LogisticsDbContext.SCHEMA);
            builder.ConfigureByConvention();

            // props
            builder.Property(x => x.Name).IsRequired();
        }
    }
}

