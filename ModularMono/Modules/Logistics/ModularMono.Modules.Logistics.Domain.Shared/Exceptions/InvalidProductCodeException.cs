﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.Shared.Exceptions;

namespace ModularMono.Modules.Logistics.Domain.Shared.Exceptions
{
    public class InvalidProductCodeException : BusinessException
    {
    }
}
