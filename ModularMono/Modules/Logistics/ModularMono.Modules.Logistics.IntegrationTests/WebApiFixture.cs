﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Respawn;

namespace ModularMono.Modules.Logistics.IntegrationTests
{
	public class WebApiFixture : IAsyncLifetime
    {
        private readonly Respawner _checkpoint;
        private readonly IConfiguration _configuration;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly WebApplicationFactory<Program> _factory;

        public WebApiFixture()
		{
		}

        public Task DisposeAsync()
        {
            throw new NotImplementedException();
        }

        public Task InitializeAsync()
        {
            throw new NotImplementedException();
        }
    }

    //class WebApiFactory : WebApplicationFactory<Program>
    //{
    //    protected override IHost CreateHost(IHostBuilder builder)
    //    {
    //        builder.ConfigureContainer<ContainerBuilder>(b => { /* test overrides here */ });
    //        return base.CreateHost(builder);
    //    }

    //    protected override void ConfigureWebHost(IWebHostBuilder builder)
    //    {
    //        builder.ConfigureAppConfiguration((_, configBuilder) =>
    //        {
    //            configBuilder
    //                .SetBasePath(Directory.GetCurrentDirectory())
    //                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    //            //configBuilder.AddInMemoryCollection(new Dictionary<string, string>
    //            //    {
    //            //        {"ConnectionStrings:DefaultConnection", _connectionString}
    //            //    });
    //        });
    //    }
    //}
}

