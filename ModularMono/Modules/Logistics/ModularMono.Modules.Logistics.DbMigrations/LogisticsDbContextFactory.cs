﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ModularMono.BuildingBlocks.Infrastructure.EventBus;
using ModularMono.Modules.Logistics.Persistence;

namespace ModularMono.Modules.Logistics.DbMigrations
{
    public class LogisticsDbContextFactory : IDesignTimeDbContextFactory<LogisticsDbContext>
    {
        public LogisticsDbContext CreateDbContext(string[] args)
        {
            var connectionString = args is { Length: > 0 } ? args[0] : "";
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                
                var configuration = config.Build();
                connectionString = configuration.GetConnectionString("DefaultConnection");
            }

            var builder = new DbContextOptionsBuilder<LogisticsDbContext>();
            builder.UseSqlServer(connectionString, sqlOptions =>
            {
                sqlOptions.MigrationsHistoryTable("__EFMigrationsHistory", LogisticsDbContext.SCHEMA);
                sqlOptions.MigrationsAssembly("ModularMono.Modules.Logistics.DbMigrations");
            });

            return new LogisticsDbContext(builder.Options);
        }
    }

    public class IntegrationOutboxDbContextFactory : IDesignTimeDbContextFactory<IntegrationOutboxDbContext>
    {
        public IntegrationOutboxDbContext CreateDbContext(string[] args)
        {
            var connectionString = args is { Length: > 0 } ? args[0] : "";
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                var config = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

                var configuration = config.Build();
                connectionString = configuration.GetConnectionString("DefaultConnection");
            }

            var builder = new DbContextOptionsBuilder<IntegrationOutboxDbContext>();
            builder.UseSqlServer(connectionString, sqlOptions =>
            {
                sqlOptions.MigrationsHistoryTable("__EFMigrationsHistory", LogisticsDbContext.SCHEMA);
                sqlOptions.MigrationsAssembly("ModularMono.Modules.Logistics.DbMigrations");
            });

            return new IntegrationOutboxDbContext(builder.Options, LogisticsDbContext.SCHEMA);
        }
    }
}
