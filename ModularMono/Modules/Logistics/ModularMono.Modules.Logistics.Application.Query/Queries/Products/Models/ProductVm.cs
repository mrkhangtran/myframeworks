﻿using AutoMapper;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.Modules.Logistics.Application.Query.DataModels;

namespace ModularMono.Modules.Logistics.Application.Query.Queries.Products.Models
{
    public class ProductVm
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<Product, ProductVm>();
            }
        }
    }
}
