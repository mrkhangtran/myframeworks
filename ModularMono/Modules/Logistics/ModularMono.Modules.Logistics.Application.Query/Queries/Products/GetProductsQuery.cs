﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Query;
using ModularMono.BuildingBlocks.Mediatr.Abstractions;
using ModularMono.Modules.Logistics.Application.Query.DataModels;
using ModularMono.Modules.Logistics.Application.Query.Interfaces;
using ModularMono.Modules.Logistics.Application.Query.Queries.Products.Models;
using ModularMono.Shared;

namespace ModularMono.Modules.Logistics.Application.Query.Queries.Products
{
    public class GetProductsQuery : IQuery<PaginatedResult<ProductVm>>
    {
        public QueryOption QueryOptions { get; set; } = new();

        public string Keyword { get; set; }

        class Handler : IRequestHandler<GetProductsQuery, PaginatedResult<ProductVm>>
        {
            private readonly ILogisticsQueryDbContext _dbContext;
            private readonly IMapper _mapper;
            private readonly IPaginator _paginator;

            public Handler(ILogisticsQueryDbContext dbContext, IMapper mapper, IPaginator paginator)
            {
                _dbContext = dbContext;
                _mapper = mapper;
                _paginator = paginator;
            }

            public async Task<PaginatedResult<ProductVm>> Handle(GetProductsQuery request, CancellationToken cancellationToken)
            {
                var query = _dbContext.Set<Product>().AsNoTracking().ProjectTo<ProductVm>(_mapper.ConfigurationProvider).AsQueryable();

                //Filter
                if (!string.IsNullOrEmpty(request.Keyword))
                {
                    query = query.Where(w => w.Name.Contains(request.Keyword));
                }

                var result = await _paginator.CreateAsync(query, request.QueryOptions);
                return result;
            }
        }
    }
}
