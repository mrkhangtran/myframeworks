﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.Modules.Logistics.Application.Query.DataModels;

namespace ModularMono.Modules.Logistics.Application.Query.Interfaces
{
    public interface ILogisticsQueryDbContext
    {
        DbSet<T> Set<T>() where T : class;

        /// <summary>
        /// If deleted data should be included in the query result
        /// </summary>
        bool IncludeDeleted { get; set; }
    }
}
