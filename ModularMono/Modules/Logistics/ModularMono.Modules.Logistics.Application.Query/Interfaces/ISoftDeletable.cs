﻿namespace ModularMono.Modules.Logistics.Application.Query.Interfaces
{
    public interface ISoftDeletable
    {
        bool IsDeleted { get; set; }
    }
}
