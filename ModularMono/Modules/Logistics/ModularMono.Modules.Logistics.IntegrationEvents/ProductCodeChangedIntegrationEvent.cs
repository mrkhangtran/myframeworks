﻿using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;

namespace ModularMono.Modules.Logistics.IntegrationEvents
{
    public record ProductCodeChangedIntegrationEvent : IntegrationEvent
    {
        public required Guid ProductId { get; init; }

        public required string NewCode { get; init; }
    }
}