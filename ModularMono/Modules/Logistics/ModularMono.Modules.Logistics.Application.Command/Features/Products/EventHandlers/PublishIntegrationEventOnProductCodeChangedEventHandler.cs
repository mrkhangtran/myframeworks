﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;
using ModularMono.Modules.Logistics.Domain.Entities.ProductAggregate;
using ModularMono.Modules.Logistics.IntegrationEvents;
using ModularMono.Shared;

namespace ModularMono.Modules.Logistics.Application.Command.Features.Products.EventHandlers
{
    internal class PublishIntegrationEventOnProductCodeChangedEventHandler : INotificationHandler<ProductCodeChangedEvent>
    {
        private readonly IIntegrationOutboxService _outbox;
        private readonly UserContext _userContext;

        public PublishIntegrationEventOnProductCodeChangedEventHandler(IIntegrationOutboxService outbox, UserContext userContext)
        {
            _outbox = outbox;
            _userContext = userContext;
        }

        public async Task Handle(ProductCodeChangedEvent notification, CancellationToken cancellationToken)
        {
            var @event = new ProductCodeChangedIntegrationEvent
            {
                ProductId = notification.ProductId,
                NewCode = notification.Code,
                OccurredBy = Guid.Parse(_userContext.UserId)
            };

            await _outbox.AddEventAsync(@event);
        }
    }
}
