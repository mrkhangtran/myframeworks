﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using ModularMono.BuildingBlocks.Mediatr.Abstractions;
using ModularMono.BuildingBlocks.Mediatr.Authorization;
using ModularMono.Modules.Logistics.Domain.Entities.ProductAggregate;

namespace ModularMono.Modules.Logistics.Application.Command.Features.Products.Commands
{
    public class CreateProductCommand : ICommand<Guid>
    {
        public required string Name { get; init; }

        public required string Code { get; init; }

        class Handler : IRequestHandler<CreateProductCommand, Guid>
        {
            private readonly IProductRepository _productRepo;

            public Handler(IProductRepository productRepo)
            {
                _productRepo = productRepo;
            }

            public async Task<Guid> Handle(CreateProductCommand request, CancellationToken cancellationToken)
            {
                var product = new Product(Guid.NewGuid(), request.Name, request.Code);
                _productRepo.Add(product);

                product.ChangeCode(Guid.NewGuid().ToString());

                await _productRepo.UnitOfWork.SaveChangesAsync(cancellationToken);

                return product.Id;
            }
        }

        class Validator : AbstractValidator<CreateProductCommand>
        {
            public Validator()
            {
                RuleFor(cmd => cmd.Name).NotEmpty();
                RuleFor(cmd => cmd.Code).NotEmpty();
            }
        }

        class Authorizer : AuthorizerBase<CreateProductCommand>
        {
            public override void BuildPolicy(CreateProductCommand request)
            {
                //UseRequirement(new MustHaveRolesRequirement { });
            }
        }
    }
}
