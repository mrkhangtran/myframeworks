﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularMono.Modules.Logistics.Application.Command
{
    public class UserContext
    {
        public string UserId { get; private set; }
        public string Username { get; private set; }
        public string Role { get; private set; }

        public void Set(string userId, string username, string role)
        {
            UserId = userId;
            Username = username;
            Role = role;
        }
    }
}
