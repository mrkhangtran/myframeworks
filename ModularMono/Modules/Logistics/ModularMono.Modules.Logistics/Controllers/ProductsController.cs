﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ModularMono.Modules.Logistics.Application.Command.Features.Products.Commands;
using ModularMono.Modules.Logistics.Application.Query.Queries.Products;
using ModularMono.Modules.Logistics.Application.Query.Queries.Products.Models;
using ModularMono.Shared;

namespace ModularMono.Modules.Logistics.Controllers
{
    [Route("[module]/[controller]")]
    public class ProductsController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<PaginatedResult<ProductVm>>> GetProduct()
        {
            return await RequestExecutor.ExecuteAsync(new GetProductsQuery());
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> CreateProduct(CreateProductCommand cmd)
        {
            return await RequestExecutor.ExecuteAsync(cmd);
        }
    }
}
