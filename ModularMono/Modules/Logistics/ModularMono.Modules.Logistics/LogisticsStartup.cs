﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core.Lifetime;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ModularMono.Modules.Logistics.Configuration;
using ModularMono.Shared;

namespace ModularMono.Modules.Logistics
{
    public class LogisticsStartup : IModuleStartup
    {
        internal static IContainer Container { get; private set; }

        public void Initialize(IConfiguration configuration, IServiceCollection serviceCollection)
        {
            ConfigureContainer(configuration, serviceCollection);
        }

        private void ConfigureContainer(IConfiguration configuration, IServiceCollection serviceCollection)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new EventBusModule());
            builder.RegisterModule(new MediatrModule());
            builder.RegisterModule(new MiscModule(configuration));
            builder.RegisterModule(new PersistenceModule());
            builder.RegisterModule(new QueryModule());

            builder.Populate(serviceCollection);

            Container = builder.Build();
        }
    }
}
