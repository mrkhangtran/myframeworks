﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using MediatR;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;
using ModularMono.BuildingBlocks.Infrastructure.Identity;
using ModularMono.Modules.Logistics.Application.Command;
using ModularMono.Shared;
using Guid = System.Guid;

namespace ModularMono.Modules.Logistics
{
    public class LogisticsModule : IModule
    {
        public string DefaultRoutePrefix => "logistics";
        public IModuleStartup Startup => new LogisticsStartup();
    }

    public static class RequestExecutor
    {
        public static async Task<TResult> ExecuteAsync<TResult>(IRequest<TResult> request, Guid? onBehalfOfUserId = null)
        {
            await using var scope = LogisticsStartup.Container.BeginLifetimeScope();

            var mediator = scope.Resolve<IMediator>();
            var applicationContext = scope.Resolve<UserContext>();
            await BuildApplicationContextAsync(applicationContext, onBehalfOfUserId);

            return await mediator.Send(request);
        }

        public static async Task ExecuteAsync(IRequest<Unit> request, Guid? onBehalfOfUserId = null)
        {
            await using var scope = LogisticsStartup.Container.BeginLifetimeScope();

            var mediator = scope.Resolve<IMediator>();
            var applicationContext = scope.Resolve<UserContext>();
            await BuildApplicationContextAsync(applicationContext, onBehalfOfUserId);

            await mediator.Send(request);
        }

        public static async Task PublishAsync(INotification notification, Guid? onBehalfOfUserId = null)
        {
            await using var scope = LogisticsStartup.Container.BeginLifetimeScope();

            var mediator = scope.Resolve<IMediator>();
            var applicationContext = scope.Resolve<UserContext>();
            await BuildApplicationContextAsync(applicationContext, onBehalfOfUserId);

            await mediator.Publish(notification);
        }

        private static async Task BuildApplicationContextAsync(UserContext userContext, Guid? onBehalfOfUserId)
        {
            //User? user;
            //if (onBehalfOfUserId == null)
            //{
            //    var username = _executionContextAccessor.Username;
            //    user = await _identityProvider.FindByUsernameAsync(username);
            //}
            //else
            //{
            //    user = await _identityProvider.FindByUserIdAsync(onBehalfOfUserId.Value);
            //}

            //if (user == null)
            //{
            //    throw new ApplicationException("User not found");
            //}

            //userContext.Set(user.Id.ToString(), user.Username, "");

            userContext.Set(Guid.NewGuid().ToString(), "test", "");
        }
    }
}
