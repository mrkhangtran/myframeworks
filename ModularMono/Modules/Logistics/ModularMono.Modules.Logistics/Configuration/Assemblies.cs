﻿using ModularMono.Modules.Logistics.Application.Query.Queries.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ModularMono.Modules.Logistics.Persistence;
using ModularMono.Modules.Logistics.Application.Command.Features.Products.Commands;

namespace ModularMono.Modules.Logistics.Configuration
{
    internal static class Assemblies
    {
        public static readonly Assembly[] Application = { typeof(CreateProductCommand).Assembly, typeof(GetProductsQuery).Assembly };
        public static readonly Assembly Persistence = typeof(LogisticsDbContext).Assembly;
    }
}
