﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using ModularMono.Domain.Core;
using ModularMono.Modules.Logistics.Persistence;
using ModularMono.Modules.Logistics.Persistence.Repositories;
using Module = Autofac.Module;

namespace ModularMono.Modules.Logistics.Configuration
{
    class PersistenceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // DbContext
            builder.RegisterType<LogisticsDbContext>().InstancePerLifetimeScope();

            // Repositories
            builder.RegisterAssemblyTypes(Assemblies.Persistence)
                .AsImplementedInterfaces()
            .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(Repository<,>))
                .As(typeof(IRepository<,>))
                .InstancePerLifetimeScope();
        }
    }
}
