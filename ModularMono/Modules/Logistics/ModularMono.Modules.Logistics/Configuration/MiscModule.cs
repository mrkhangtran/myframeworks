﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using DelegateDecompiler;
using Microsoft.Extensions.Configuration;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Providers;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.Query;
using ModularMono.BuildingBlocks.Infrastructure.ConnectionStrings;
using ModularMono.BuildingBlocks.Infrastructure.FileStorages;
using ModularMono.BuildingBlocks.Infrastructure.Identity;
using ModularMono.BuildingBlocks.Infrastructure.Query;
using ModularMono.Modules.Logistics.Application.Command;
using Module = Autofac.Module;

namespace ModularMono.Modules.Logistics.Configuration
{
    class MiscModule : Module
    {
        private readonly IConfiguration _configuration;

        public MiscModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            // AutoMapper
            builder.RegisterAutoMapper(false, Assemblies.Application);

            builder.RegisterType<UserContext>().AsSelf().InstancePerLifetimeScope();

            builder.RegisterType<FileSystemStorage>().As<IFileStorage>().SingleInstance();
            builder.Register(p => _configuration.GetSection(nameof(FileSystemStorage)).Get<FileSystemStorageOption>()).SingleInstance();

            builder.RegisterType<Paginator>().As<IPaginator>().SingleInstance();
        }
    }
}
