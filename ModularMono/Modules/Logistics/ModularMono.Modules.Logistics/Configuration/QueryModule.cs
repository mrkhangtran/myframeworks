﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using ModularMono.Modules.Logistics.Application.Query.Interfaces;
using ModularMono.Modules.Logistics.Query;

namespace ModularMono.Modules.Logistics.Configuration
{
    class QueryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // DbContext
            builder.RegisterType<LogisticsQueryDbContext>().As<ILogisticsQueryDbContext>().InstancePerLifetimeScope();
        }
    }
}
