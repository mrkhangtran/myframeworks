﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using FluentValidation;
using MediatR;
using ModularMono.BuildingBlocks.Mediatr.Authorization;
using ModularMono.BuildingBlocks.Mediatr.Behaviors;
using ModularMono.Domain.Core.Decorators;
using ModularMono.Modules.Logistics.Application.Command.Features.Products;
using ModularMono.Modules.Logistics.Application.Query.Queries.Products;
using Module = Autofac.Module;

namespace ModularMono.Modules.Logistics.Configuration
{
    class MediatrModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Register Mediator itself
            builder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();

            // Register Mediator request and notification handlers
            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            // Register Mediator behaviors (order matters!)
            builder.RegisterGeneric(typeof(LoggingBehavior<,>)).AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterGeneric(typeof(AuthorizationBehavior<,>)).AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterGeneric(typeof(ValidationBehavior<,>)).AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterGeneric(typeof(TransactionalBehavior<,>)).AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterGeneric(typeof(IntegrationBehavior<,>)).AsImplementedInterfaces().InstancePerDependency();

            // CommandHandlers
            builder.RegisterAssemblyTypes(Assemblies.Application)
                .AsClosedTypesOf(typeof(IRequestHandler<,>))
                .InstancePerDependency();

            // Command's Validators
            builder.RegisterAssemblyTypes(Assemblies.Application)
                .AsClosedTypesOf(typeof(IValidator<>))
                .InstancePerDependency();

            // DomainEventHandlers
            builder.RegisterAssemblyTypes(Assemblies.Application)
                .AsClosedTypesOf(typeof(INotificationHandler<>))
                .InstancePerDependency();

            // Authorization
            builder.RegisterAssemblyTypes(Assemblies.Application)
                .AsClosedTypesOf(typeof(IAuthorizer<>))
                .InstancePerDependency();

            // Decorators
            builder.RegisterGenericDecorator(typeof(NotificationHandlerLoggingDecorator<>), typeof(INotificationHandler<>));
        }
    }
}
