﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using ModularMono.BuildingBlocks.Infrastructure.Abstractions.EventBus;
using ModularMono.BuildingBlocks.Infrastructure.EventBus;
using ModularMono.Modules.Logistics.Persistence;

namespace ModularMono.Modules.Logistics.Configuration
{
    class EventBusModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Event bus
            builder.RegisterType<InProcessEventBus>()
                .As<IEventBus>()
                .SingleInstance();

            builder.RegisterType<InMemoryEventBusSubscriptionManager>()
                .As<IEventBusSubscriptionManager>()
                .SingleInstance();

            // Outbox
            builder.RegisterType<IntegrationOutboxDbContext>().WithParameter("schema", LogisticsDbContext.SCHEMA).InstancePerLifetimeScope();
            builder.RegisterType<IntegrationOutboxService>().As<IIntegrationOutboxService>().InstancePerLifetimeScope();
        }
    }
}
