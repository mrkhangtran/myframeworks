﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.Domain.Core;

namespace ModularMono.Modules.Logistics.Domain.Entities.ProductAggregate
{
    public interface IProductRepository : IRepository<Product, Guid>
    {
    }
}
