﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.Domain.Core;

namespace ModularMono.Modules.Logistics.Domain.Entities.ProductAggregate
{
    public class ProductCodeChangedEvent : DomainEvent
    {
        public required Guid ProductId { get; init; }

        public required string Code { get; init; }

        public ProductCodeChangedEvent(string source) : base(source)
        {
        }
    }
}
