﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularMono.Domain.Core;

namespace ModularMono.Modules.Logistics.Domain.Entities.ProductAggregate
{
    public class Product : Entity, IAggregateRoot, IConcurrencyCheck, IAuditable, ISoftDeletable
    {
        public string Name { get; set; }

        public string Code { get; private set; }

        public byte[]? RowVersion { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string? LastUpdatedBy { get; set; }

        private Product() { }

        public Product(Guid id, string name, string code)
        {
            Id = id;
            Name = name;
            Code = code;
        }

        public void ChangeCode(string code)
        {
            Code = code;

            AddDomainEvent(new ProductCodeChangedEvent(nameof(ChangeCode)) { ProductId = Id, Code = code });
        }
    }
}
