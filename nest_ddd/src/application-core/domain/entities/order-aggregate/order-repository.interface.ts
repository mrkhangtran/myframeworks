
import { IRepository } from "../../seed-work/_index";
import { Order } from "./order";

export const ORDER_REPO_TOKEN = 'ORDER_REPO';

export interface IOrderRepository extends IRepository<Order> {

}