import { Guid } from "guid-typescript";

export class OrderLine {
    private _id: string;
    public get id(): string {
        return this._id;
    }

    public readonly product: string;
    public readonly price: number;

    constructor(id: string, product: string, price: number) {
        this._id = id;
        this.product = product;
        this.price = price;
    }
}