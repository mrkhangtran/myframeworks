import { Notification } from "nestjs-mediator";

export class OrderLineAddedEvent extends Notification {
    constructor(readonly orderId: string, readonly orderNumber: string) {
        super();
    }
}