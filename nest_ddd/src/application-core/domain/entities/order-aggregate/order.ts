import { Expose } from "class-transformer";
import { Guid } from "guid-typescript";
import { AggregateRoot } from "../../seed-work/_index";
import { OrderLine } from "./order-line";
import { OrderLineAddedEvent } from "./order.events";

export class Order extends AggregateRoot {
    public readonly orderNumber: string;

    private _total = 0;

    @Expose()
    public get total(): number {
        return this._total;
    }

    private _lines: Array<OrderLine> = [];

    @Expose()
    public get lines(): ReadonlyArray<OrderLine> {
        return this._lines;
    }

    constructor(id: string, orderNumber: string) {
        super();

        this._id = id;
        this.orderNumber = orderNumber;
    }

    public addLine(line: OrderLine): void {
        this._total += line.price;
        this._lines.push(line);

        this.addDomainEvent(new OrderLineAddedEvent(this.id, this.orderNumber));
    }
}