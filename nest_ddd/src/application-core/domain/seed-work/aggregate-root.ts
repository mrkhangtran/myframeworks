import { Expose } from "class-transformer";
import { Notification } from "nestjs-mediator";

export class AggregateRoot {
    protected _id: string;

    @Expose({ name: '_id' })
    public get id(): string {
        return this._id;
    }

    protected _events: Array<Notification> = [];
    public get domainEvents(): ReadonlyArray<Notification> {
        return this._events;
    }

    public addDomainEvent(event: Notification): void {
        this._events.push(event);
    }

    public clearDomainEvents(): void {
        this._events = [];
    }
}