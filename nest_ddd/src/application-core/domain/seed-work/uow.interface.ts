export interface IUnitOfWork {
    save(): Promise<void>;
}