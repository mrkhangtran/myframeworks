import { Guid } from "guid-typescript";
import { AggregateRoot } from "./aggregate-root";
import { IUnitOfWork } from "./uow.interface";

export interface IRepository<T extends AggregateRoot> {
    unitOfWork: IUnitOfWork;

    add(entity: T): Promise<void>;
    update(entity: T): Promise<void>;
    remove(entity: T): Promise<void>;
    getById(id: string): Promise<T>;
}