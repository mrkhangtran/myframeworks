import { NotificationHandler, INotificationHandler } from "nestjs-mediator"
import { OrderLineAddedEvent } from "src/application-core/domain/entities/order-aggregate/order.events";

@NotificationHandler(OrderLineAddedEvent)
export class LogSomethingOnOrderLineAddedEventHandler implements INotificationHandler<OrderLineAddedEvent>{
    handle(data: OrderLineAddedEvent) {
        //throw new Error("test error");
        console.log(`Handling OrderLineAddedEvent: ${data}`);
    }
}