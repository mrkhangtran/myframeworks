import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IRequestHandler, Request } from "nestjs-mediator"
import { RequestHandler } from "nestjs-mediator/dist/mediator.decorator";
import { OrderDoc } from "src/infrastructure/database/schemas/order.schema";

export class SearchOrdersQuery extends Request<any> {

}

@RequestHandler(SearchOrdersQuery)
export class SearchOrdersQueryHandler implements IRequestHandler<SearchOrdersQuery, any> {

    constructor(@InjectModel(OrderDoc.name) private entityModel: Model<OrderDoc>) {
    }

    async handle(data: SearchOrdersQuery): Promise<any> {
        const result = await this.entityModel.find();
        return result;
    }
}