import { Inject } from "@nestjs/common";
import { IOrderRepository, Order, OrderLine, ORDER_REPO_TOKEN } from "src/application-core/domain/entities/_index";
import { IRequestHandler, Request } from "nestjs-mediator"
import { RequestHandler } from "nestjs-mediator/dist/mediator.decorator";
import { IdGenerator } from "src/infrastructure/database/id-generator";

export class CreateOrderCommand extends Request<any> {
    orderNumber: string;
    lines: { product: string, price: number }[];
}

@RequestHandler(CreateOrderCommand)
export class CreateOrderCommandHandler implements IRequestHandler<CreateOrderCommand, any> {

    constructor(
        @Inject(ORDER_REPO_TOKEN) private readonly orderRepo: IOrderRepository
    ) { }

    async handle(rq: CreateOrderCommand): Promise<any> {
        const { orderNumber, lines } = rq;

        // create order
        const order = new Order(IdGenerator.create(), orderNumber);
        lines.forEach(x => order.addLine(new OrderLine(IdGenerator.create(), x.product, x.price)));
        await this.orderRepo.add(order);

        // persist
        await this.orderRepo.unitOfWork.save();
    }
}