import { CreateOrderCommandHandler } from "./orders/commands/create-order.command";
import { LogSomethingOnOrderLineAddedEventHandler } from "./orders/event-handlers/log-something-on-order-line-added.event-handler";
import { SearchOrdersQueryHandler } from "./orders/queries/search-orders.query";


export const OrderCommandHandlers = [
    CreateOrderCommandHandler
];

export const OrderQueryHandlers = [
    SearchOrdersQueryHandler
];

export const OrderEventHandlers = [
    LogSomethingOnOrderLineAddedEventHandler
];