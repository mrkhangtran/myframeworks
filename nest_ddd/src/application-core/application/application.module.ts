import { Module } from "@nestjs/common";
import { MediatorModule } from "nestjs-mediator";
import { DatabaseModule } from "src/infrastructure/database/database.module";
import { OrderCommandHandlers, OrderEventHandlers, OrderQueryHandlers } from "./_index";

@Module({
    imports: [
        MediatorModule,
        DatabaseModule
    ],
    providers: [
        ...OrderCommandHandlers,
        ...OrderQueryHandlers,
        ...OrderEventHandlers
    ],
    exports: [
        MediatorModule
    ]
})
export class ApplicationModule { }
