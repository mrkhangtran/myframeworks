import mongoose from "mongoose";

export class IdGenerator {
    static create(): string {
        return new mongoose.Types.ObjectId().toHexString();
    }
}