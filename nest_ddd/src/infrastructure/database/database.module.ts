import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MediatorModule } from 'nestjs-mediator';
import { ORDER_REPO_TOKEN } from 'src/application-core/domain/entities/_index';
import { OrderDocMapper, OrderRepository } from './repositories/order.repository';
import { OrderDoc, OrderSchema } from './schemas/order.schema';
import { UnitOfWork } from './uow';

const mongooseFeatureModules = [
    MongooseModule.forFeature([
        {
            name: OrderDoc.name,
            schema: OrderSchema,
        }
    ]),
];

const repoProviders = [
    {
        useClass: OrderRepository,
        provide: ORDER_REPO_TOKEN
    },
];

@Module({
    imports: [
        MediatorModule,
        MongooseModule.forRoot('mongodb+srv://mern_admin:1234@cluster0.lyisxg6.mongodb.net/?retryWrites=true&w=majority'),
        ...mongooseFeatureModules
    ],
    providers: [
        ...repoProviders,
        OrderDocMapper,
        UnitOfWork,
    ],
    exports: [
        ...mongooseFeatureModules,
        ...repoProviders,
    ]
})
export class DatabaseModule { }
