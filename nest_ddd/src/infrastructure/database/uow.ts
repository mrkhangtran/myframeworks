import { IUnitOfWork } from "src/application-core/domain/seed-work/uow.interface";
import { Injectable } from "@nestjs/common/decorators/core/injectable.decorator";
import { Scope } from "@nestjs/common";
import { InjectConnection } from "@nestjs/mongoose";
import { Connection, ClientSession } from "mongoose";
import { Notification, Mediator } from "nestjs-mediator"

@Injectable({ scope: Scope.REQUEST })
export class UnitOfWork implements IUnitOfWork {

    private _session: ClientSession;
    private _domainEvents: Notification[] = [];

    constructor(@InjectConnection() private connection: Connection, private mediator: Mediator) {
    }

    public async getSession(): Promise<ClientSession> {
        if (this._session) {
            return this._session;
        }

        this._session = await this.connection.startSession();
        this._session.startTransaction();
        return this._session;
    }

    attachDomainEvents(events: Notification[]): void {
        this._domainEvents.push(...events);
    }

    async save(): Promise<void> {
        if (this._session && this._session.inTransaction()) {
            try {
                for (const e of this._domainEvents) {
                    await this.mediator.publish(e)
                }

                await this._session.commitTransaction();
                this._session.endSession();
            } catch (error) {
                await this._session.abortTransaction();
                this._session.endSession();
                throw error;
            }

        }
    }
}