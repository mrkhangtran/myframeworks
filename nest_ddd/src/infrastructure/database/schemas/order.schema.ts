import { Prop } from "@nestjs/mongoose";
import { Schema, SchemaFactory } from "@nestjs/mongoose/dist";
import { Expose, Type } from "class-transformer";
import { HydratedDocument } from "mongoose";
import { BaseDoc } from "./base.schema";

export type OrderDocument = HydratedDocument<OrderDoc>;

// Order Line
@Schema()
export class OrderLineDoc extends BaseDoc {

    @Expose()
    @Prop({ required: true })
    product: string;

    @Expose()
    @Prop({ required: true })
    price: number;

    test() {
        console.log('aasd');
    }
}

export const OrderLineSchema = SchemaFactory.createForClass(OrderLineDoc);

// Order
@Schema({ collection: 'orders' })
export class OrderDoc extends BaseDoc {
    @Expose()
    @Prop({ required: true })
    orderNumber: string;

    @Expose()
    @Prop()
    total: number;

    @Expose()
    @Type(() => OrderLineDoc)
    @Prop({ type: [OrderLineSchema] })
    lines: OrderLineDoc[];
}

export const OrderSchema = SchemaFactory.createForClass(OrderDoc);

