import { Prop } from "@nestjs/mongoose";
import { Expose } from "class-transformer";
import { ObjectId } from 'mongodb';

export abstract class BaseDoc {
    @Expose()
    @Prop()
    _id: ObjectId;
}