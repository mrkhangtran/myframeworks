import { Scope } from "@nestjs/common";
import { Injectable } from "@nestjs/common/decorators/core/injectable.decorator";
import { InjectModel } from "@nestjs/mongoose";
import { instanceToPlain, plainToClass } from "class-transformer";
import { Model } from "mongoose";
import { IOrderRepository, Order } from "src/application-core/domain/entities/_index";
import { OrderDoc } from "../schemas/order.schema";
import { UnitOfWork } from "../uow";
import { EntityDocMapper, IEntityDocMapper, Repository } from "./base.repository";

@Injectable({ scope: Scope.REQUEST })
export class OrderDocMapper extends EntityDocMapper<Order, OrderDoc> implements IEntityDocMapper<Order, OrderDoc> {
    override mapToDoc(entity: Order): OrderDoc {
        const doc = plainToClass(OrderDoc, instanceToPlain(entity, { excludePrefixes: ['_'] }), { excludeExtraneousValues: true });
        return doc;
    }

    mapToEntity(doc: OrderDoc): Order {
        return plainToClass(Order, doc);
    }
}

@Injectable({ scope: Scope.REQUEST })
export class OrderRepository extends Repository<Order, OrderDoc> implements IOrderRepository {
    constructor(@InjectModel(OrderDoc.name) protected entityModel: Model<OrderDoc>,
        protected entityDocMapper: OrderDocMapper,
        public unitOfWork: UnitOfWork) {
        super(entityModel, entityDocMapper, unitOfWork);
    }
}

