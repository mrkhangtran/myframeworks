import { NotFoundException } from "@nestjs/common";
import mongoose, { Model } from "mongoose";
import { AggregateRoot, IRepository } from "../../../application-core/domain/seed-work/_index";
import { BaseDoc } from "../schemas/base.schema";
import { UnitOfWork } from "../uow";

export abstract class Repository<TEntity extends AggregateRoot, TDoc extends BaseDoc> implements IRepository<TEntity> {
    constructor(protected entityModel: Model<TDoc>,
        protected entityDocMapper: IEntityDocMapper<TEntity, TDoc>,
        public unitOfWork: UnitOfWork) {

    }

    async add(entity: TEntity): Promise<void> {
        const session = await this.unitOfWork.getSession();

        const doc = this.entityDocMapper.mapFromEntity(entity);
        const createdEntity = new this.entityModel(doc);
        await createdEntity.save({ session });

        this.handleDomainEvents(entity);
    }

    async update(entity: TEntity): Promise<void> {
        const session = await this.unitOfWork.getSession();

        const doc = this.entityDocMapper.mapFromEntity(entity);
        const updatedDoc = await this.entityModel.findOneAndReplace({ _id: entity.id }, doc, { lean: true, new: true, session });

        if (!updatedDoc) {
            throw new NotFoundException('Unable to find the entity to replace.');
        }

        this.handleDomainEvents(entity);
    }

    async remove(entity: TEntity): Promise<void> {
        const session = await this.unitOfWork.getSession();

        await this.entityModel.findByIdAndDelete(entity.id, { session });

        this.handleDomainEvents(entity);
    }

    async getById(id: string): Promise<TEntity> {
        const doc = await this.entityModel.findById(id, {}, { lean: true });
        if (!doc) {
            return null;
        }

        const entity = this.entityDocMapper.mapToEntity(doc);
        return entity;
    }

    private handleDomainEvents(entity: TEntity): void {
        this.unitOfWork.attachDomainEvents([...entity.domainEvents]);
        entity.clearDomainEvents();
    }
}

export interface IEntityDocMapper<TEntity extends AggregateRoot, TDoc extends BaseDoc> {
    mapToEntity(doc: TDoc): TEntity;
    mapFromEntity(entity: TEntity): TDoc;
}

export abstract class EntityDocMapper<T extends AggregateRoot, TDoc extends BaseDoc> {
    protected abstract mapToDoc(entity: T): TDoc;

    mapFromEntity(entity: T): TDoc {
        const doc = this.mapToDoc(entity);
        doc._id = new mongoose.Types.ObjectId(doc._id);
        return doc;
    }
}