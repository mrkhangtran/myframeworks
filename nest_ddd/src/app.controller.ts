import { Controller, Get, Scope } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { Mediator } from 'nestjs-mediator';
import { AppService } from './app.service';
import { CreateOrderCommand } from './application-core/application/orders/commands/create-order.command';
import { SearchOrdersQuery } from './application-core/application/orders/queries/search-orders.query';

@Controller({
  scope: Scope.REQUEST,
})
export class AppController {
  constructor(private readonly appService: AppService, private mediator: Mediator) { }

  @Get()
  async getHello(): Promise<string> {
    const command = new CreateOrderCommand();
    command.orderNumber = '1231313';
    command.lines = [{ product: 'iphone xxxx', price: 2000 }];

    await this.mediator.send(command);

    return this.appService.getHello();
  }

  @Get('/test-data')
  async search(): Promise<any> {
    const result = await this.mediator.send(new SearchOrdersQuery());
    return result;
  }
}
