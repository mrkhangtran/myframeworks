﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Application;
using Microsoft.AspNetCore.Http;

namespace WebAPI.Infrastructure.Middlewares
{
    public class ApplicationContextBuilderMiddleware
    {
        private readonly RequestDelegate _next;

        public ApplicationContextBuilderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var ctx = (ApplicationContext)context.RequestServices.GetService(typeof(ApplicationContext));

            var username = "anonymous";
            string userId = null;
            string role = null;

            if (context.User.Identity.IsAuthenticated)
            {
                foreach (var claim in context.User.Claims)
                {
                    switch (claim.Type)
                    {
                        case ClaimTypes.Name:
                            username = claim.Value;
                            break;
                        case ClaimTypes.NameIdentifier:
                            userId = claim.Value;
                            break;
                        case ClaimTypes.Role:
                            role = claim.Value;
                            break;
                    }
                }
            }

            ctx.SetPrincipal(new UserPrincipal(userId, username, role));

            await _next.Invoke(context);
        }
    }
}
