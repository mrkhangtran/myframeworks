﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Application.Providers
{
    public interface IConnectionStringProvider
    {
        string Get(string name = "DefaultConnection");
    }
}
