﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Application.Providers
{
    public interface IConfigProvider
    {
        SystemConfig GetSystemConfig();
    }
}
