﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Application.RecordLock
{
    public class RecordLockException : Exception
    {
        public RecordLockException()
        { }

        public RecordLockException(string message)
            : base(message)
        { }
    }
}
