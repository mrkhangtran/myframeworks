﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Application.RecordLock
{
    public interface ILockManager
    {
        Task AcquireLockAsync<T>(string entityId, string ownerId);

        Task ReleaseLockAsync<T>(string entityId, string ownerId);

        Task<bool> HasLockAsync<T>(string entityId, string ownerId);

        Task ReleaseAllLocksAsync(string ownerId);

        Task RenewLockAsync<T>(string entityId, string ownerId);
    }
}
