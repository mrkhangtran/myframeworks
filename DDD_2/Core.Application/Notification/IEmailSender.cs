﻿using System.Threading.Tasks;

namespace Core.Application.Notification
{
    public interface IEmailSender
    {
        Task SendAsync(string subject, string body, string from, string to);
    }
}
