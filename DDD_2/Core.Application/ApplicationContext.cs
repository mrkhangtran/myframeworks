﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Application
{
    public class ApplicationContext
    {
        public UserPrincipal Principal { get; private set; }

        public void SetPrincipal(UserPrincipal principal)
        {
            Principal = principal;
        }
    }

    public class UserPrincipal
    {
        public string UserId { get; }

        public string Username { get; }

        public string Role { get; }

        // TODO: temporarily hard-coded for vietnam
        public TimeZoneInfo TimeZone { get; } = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");

        public UserPrincipal(string userId, string userName, string role)
        {
            UserId = userId;
            Username = userName;
            Role = role;
        }

        public DateTime ConvertUtcToUserLocalTime(DateTime? utc = null)
        {
            if (!utc.HasValue)
            {
                utc = DateTime.UtcNow;
            }

            return TimeZoneInfo.ConvertTimeFromUtc(utc.Value, TimeZone);
        }

        public DateTime ConvertUserLocalTimeToUtc(DateTime local)
        {
            return TimeZoneInfo.ConvertTimeToUtc(local, TimeZone);
        }
    }
}
