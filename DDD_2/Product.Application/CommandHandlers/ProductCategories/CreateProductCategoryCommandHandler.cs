﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Product.Domain.Aggregates.ProductCategoryAggregate;
using Product.Messages;

namespace Product.Application.CommandHandlers.ProductCategories
{
    public class CreateProductCategoryCommandHandler : IRequestHandler<ProductCategoryCommands.Create, Guid>
    {
        private readonly IProductCategoryRepository _productCategoryRepo;

        public CreateProductCategoryCommandHandler(IProductCategoryRepository productCategoryRepo)
        {
            _productCategoryRepo = productCategoryRepo;
        }

        public async Task<Guid> Handle(ProductCategoryCommands.Create rq, CancellationToken cancellationToken)
        {
            var productCategory = new ProductCategory(rq.Name);

            _productCategoryRepo.Add(productCategory);
            await _productCategoryRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return productCategory.Id;
        }
    }
}
