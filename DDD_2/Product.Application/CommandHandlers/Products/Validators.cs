﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Product.Messages;

namespace Product.Application.CommandHandlers.Products
{
    public class CreateProductCommandValidator : AbstractValidator<ProductCommands.Create>
    {
        public CreateProductCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }

    public class ActivateProductCommandValidator : AbstractValidator<ProductCommands.Activate>
    {
        public ActivateProductCommandValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
        }
    }
}
