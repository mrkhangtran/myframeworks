﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Application;
using MediatR;
using Product.Domain.Aggregates.ProductAggregate;
using Product.Domain.Shared;
using Product.Messages;

namespace Product.Application.CommandHandlers.Products
{
    public class CreateProductCommandHandler :  IRequestHandler<ProductCommands.Create, Guid>
    {
        private readonly IProductRepository _productRepo;
        private readonly ICurrencyProvider _currencyProvider;

        public CreateProductCommandHandler(IProductRepository productRepo, ICurrencyProvider currencyProvider)
        {
            _productRepo = productRepo;
            _currencyProvider = currencyProvider;
        }

        public async Task<Guid> Handle(ProductCommands.Create rq, CancellationToken cancellationToken)
        {
            var product = new Domain.Aggregates.ProductAggregate.Product(rq.Name, rq.ProductCategoryId);

            // set price
            if (rq.Price != 0 && !string.IsNullOrEmpty(rq.Currency))
            {
                var currency = await _currencyProvider.GetAsync(rq.Currency);
                var price = new Price(rq.Price, currency.CurrencyCode);
                product.SetPrice(price);
            }

            _productRepo.Add(product);
            await _productRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return product.Id;
        }
    }
}
