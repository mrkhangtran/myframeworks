﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Product.Application.Queries.Models
{
    public static class ProductCategoryReturnModels
    {
        public class ProductCategory
        {
            public Guid Id { get; set; }

            public string Name { get; set; }
        }
    }
}
