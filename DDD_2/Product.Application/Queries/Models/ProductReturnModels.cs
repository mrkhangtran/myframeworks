﻿using System;
using System.Collections.Generic;

namespace Product.Application.Queries.Models
{
    public static class ProductReturnModels
    {
        public class ProductSummary
        {
            public Guid Id { get; set; }

            public string Name { get; set; }
        }

        public class ProductDetails
        {
            public Guid Id { get; set; }

            public string Name { get; set; }

            public IEnumerable<Picture> Pictures { get; set; }
        }

        public class Picture
        {
            public string Url { get; set; }

            public PictureSize Size { get; set; }
        }

        public class PictureSize
        {
            public decimal Width { get; set; }

            public decimal Height { get; set; }
        }
    }
}
