﻿using System;
using System.Threading.Tasks;
using Product.Application.Queries.Models;

namespace Product.Application.Queries
{
    public interface IProductCategoryQuery
    {
        Task<ProductCategoryReturnModels.ProductCategory> GetProductCategoryByIdAsync(Guid id);
    }
}
