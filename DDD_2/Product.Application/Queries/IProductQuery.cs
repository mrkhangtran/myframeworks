﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Product.Application.Queries.Models;

namespace Product.Application.Queries
{
    public interface IProductQuery
    {
        public Task<ProductReturnModels.ProductDetails> GetProductByIdAsync(Guid id);
    }
}
