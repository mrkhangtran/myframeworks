﻿using System;
using System.Reflection;
using Autofac;
using Core.Application;
using Core.Application.CommandPipeline;
using Core.Application.Notification;
using Core.Application.Providers;
using Core.Application.RecordLock;
using Infrastructure.Configuration;
using Infrastructure.Notification.Email;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Providers;
using Infrastructure.RecordLock;
using MediatR;
using Product.API;
using Product.Domain.Shared;

namespace AutofacModule
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterCore(builder);
            builder.RegisterProductModule();
        }

        private void RegisterCore(ContainerBuilder builder)
        {
            // Register Mediator itself
            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly).AsImplementedInterfaces().InstancePerLifetimeScope();

            // Register Mediator request and notification handlers for Command/CommandHandler and DomainEvent/DomainEventHandler
            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            // Register Mediator behaviors
            builder.RegisterGeneric(typeof(ValidatorBehavior<,>)).As(typeof(IPipelineBehavior<,>)).InstancePerDependency();

            // DbContext
            builder.RegisterType<ApplicationDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<RecordLockDbContext>().InstancePerLifetimeScope();

            // Providers & Services
            builder.RegisterType<AppSettingConnectionStringProvider>().As<IConnectionStringProvider>().SingleInstance();
            builder.RegisterType<SendGridEmailSender>().As<IEmailSender>().SingleInstance();
            builder.RegisterType<CurrencyProvider>().As<ICurrencyProvider>().InstancePerLifetimeScope();
            builder.RegisterType<LockManager>().As<ILockManager>().InstancePerLifetimeScope();

            // Application Context
            builder.RegisterType<ApplicationContext>().AsSelf().InstancePerLifetimeScope();
        }
    }
}
