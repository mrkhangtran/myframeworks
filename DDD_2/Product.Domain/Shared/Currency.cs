﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Domain;

namespace Product.Domain.Shared
{
    public class Currency : ValueObject
    {
        public string CurrencyCode { get; private set; }
        public bool InUse { get; private set; }
        public int DecimalPlaces { get; private set; }

        private Currency()
        {
            
        }

        public Currency(string currencyCode, bool inUse, int decimalPlaces)
        {
            CurrencyCode = currencyCode;
            InUse = inUse;
            DecimalPlaces = decimalPlaces;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return DecimalPlaces;
            yield return InUse;
            yield return CurrencyCode;
        }
    }
}
