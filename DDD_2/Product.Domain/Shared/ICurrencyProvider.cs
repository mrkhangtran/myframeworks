﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Product.Domain.Shared
{
    public interface ICurrencyProvider
    {
        Task<Currency> GetAsync(string currencyCode);
    }
}
