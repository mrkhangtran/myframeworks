﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Domain;
using EnsureThat;

namespace Product.Domain.Shared
{
    public class Money : ValueObject
    {
        public decimal Amount { get; private set; }

        public string CurrencyCode { get; private set; }

        protected Money() { }

        public Money(decimal amount, string currencyCode)
        {
            //EnsureValid(amount, currencyCode);

            Amount = amount;
            CurrencyCode = currencyCode;
        }

        public Money Add(Money val)
        {
            if (CurrencyCode != val.CurrencyCode)
            {
                throw new DomainExceptions.CurrencyMismatchException(
                    "Cannot sum amounts with different currencies"
                );
            }

            return new Money(Amount + val.Amount, CurrencyCode);
        }

        public Money Subtract(Money val)
        {
            if (CurrencyCode != val.CurrencyCode)
            {
                throw new DomainExceptions.CurrencyMismatchException(
                    "Cannot subtract amounts with different currencies"
                );
            }

            return new Money(Amount - val.Amount, CurrencyCode);
        }

        public static Money operator +(Money val1, Money val2) => val1.Add(val2);

        public static Money operator -(Money val1, Money val2) => val1.Subtract(val2);

        public override string ToString() => $"{CurrencyCode} {Amount}";

        public static Money FromString(string amountText, string currencyCode)
        {
            var amount = decimal.Parse(amountText);
            return new Money(amount, currencyCode);
        }

        //private void EnsureValid(decimal amount, Currency currency)
        //{
        //    Ensure.That(currency.InUse, nameof(currency.InUse),
        //            options => options.WithMessage($"Currency {currency.CurrencyCode} is not in use"))
        //        .IsTrue();

        //    Ensure.That(amount, nameof(amount),
        //            options => options.WithMessage($"Amount in {currency.CurrencyCode} cannot have more than {currency.DecimalPlaces} decimals"))
        //        .Is(decimal.Round(amount, currency.DecimalPlaces));
        //}

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Amount;
            yield return CurrencyCode;
        }
    }
}
