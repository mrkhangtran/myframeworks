﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Domain;

namespace Product.Domain.Shared
{
    public static class DomainExceptions
    {
        public class CurrencyMismatchException : DomainException
        {
            public CurrencyMismatchException(string message) : base(message) { }
        }

        public class CurrencyNotInUseException : DomainException
        {
            public CurrencyNotInUseException() : base("Currency is not in use") { }
        }

        public class InvalidEntityState : DomainException
        {
            public InvalidEntityState(object entity, string message)
                : base($"Entity {entity.GetType().Name} state change rejected, {message}")
            {
            }
        }
    }
}
