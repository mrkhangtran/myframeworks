﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Domain;

namespace Product.Domain.Aggregates.ProductAggregate
{
    public interface IProductRepository : IRepository<Product>
    {
        Product Add(Product product);

        void Update(Product product, byte[] currentRowVersion);

        Task<IEnumerable<Product>> GetAsync(QueryObject<Product> queryObject);

        Task<Product> GetByIdAsync(Guid id);
    }

    public class ProductsWithMinimumNameLengthQueryObject : QueryObject<Product>
    {
        public int MinimumNameLength { get; set; }

        public override Expression<Func<Product, bool>> AsExpression()
        {
            return p => p.Name.Length >= MinimumNameLength;
        }
    }

    public class InactiveProductsQueryObject : QueryObject<Product>
    {
        public override Expression<Func<Product, bool>> AsExpression()
        {
            return p => !p.IsActive;
        }
    }
}
