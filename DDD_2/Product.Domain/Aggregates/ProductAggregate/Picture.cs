﻿using System;
using Core.Domain;
using EnsureThat;
using Product.Domain.Shared;

namespace Product.Domain.Aggregates.ProductAggregate
{
    public class Picture : Entity
    {
        public string Url { get; private set; }

        public PictureSize Size { get; private set; }

        private Picture()
        {

        }

        public Picture(Guid id, string url)
        {
            Ensure.That(id).IsNotEmpty();
            Ensure.That(url).IsNotNullOrWhiteSpace();

            Id = id;
            Url = url;
        }

        public void SetSize(PictureSize size)
        {
            ChangeAndEnsure(() => { Size = size; });
        }

        protected override void EnsureValidState()
        {
            var isValid = !string.IsNullOrWhiteSpace(Url);

            if (!isValid)
            {
                throw new DomainExceptions.InvalidEntityState(this, $"Invalid {nameof(Picture)} state");
            }
        }
    }
}
