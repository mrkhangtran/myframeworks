﻿using System;
using System.Collections.Generic;
using Core.Domain;
using EnsureThat;

namespace Product.Domain.Aggregates.ProductAggregate
{
    public class PictureSize : ValueObject
    {
        public int Width { get; internal set; }
        public int Height { get; internal set; }

        private PictureSize() { }

        public PictureSize(int width, int height)
        {
            Ensure.That(width).IsGt(0);
            Ensure.That(height).IsGt(0);

            Width = width;
            Height = height;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Height;
            yield return Width;
        }
    }
}
