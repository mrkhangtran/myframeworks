﻿using System;
using System.Threading.Tasks;
using EnsureThat;
using Product.Domain.Shared;

namespace Product.Domain.Aggregates.ProductAggregate
{
    public class Price : Money
    {
        private Price() { }

        public Price(decimal amount, string currencyCode) : base(amount, currencyCode)
        {
            Ensure.That(amount, nameof(amount), 
                    options => options.WithMessage("Price must be positive"))
                .IsGt(0);
        }

        ///// <summary>
        ///// In the concept of Event Sourcing, the data(event) being saved needs to be in primitive or simple complex types instead of Value Object
        ///// The reason behind this is that the logic in Value Object can be changed but the data(event) being saved must always be able to be loaded after
        ///// For instance:
        ///// - The price amount doesn't have to be greater than 0 before, events with those below 0 amount were allowed to save
        ///// - Then, logic changed, price amount needs to be above 0, which makes the old events data being invalid data
        ///// - If the normal constructor is used, then, the data can't never passed the logic and is unable to be loaded
        ///// - This behavior is not correct because a single event in the past might have been invalid at present
        /////   but that is real data and combining the series of events still populates a valid price
        ///// - This constructor serves that purpose, receives simple types and no logic applied
        ///// </summary>
        ///// <param name="amount"></param>
        ///// <param name="currencyCode"></param>
        //internal Price(decimal amount, string currencyCode) : base(amount, new Currency (currencyCode, true, 2))
        //{
        //}
    }
}
