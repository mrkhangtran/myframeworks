﻿using System;
using System.Collections.Generic;
using Core.Domain;
using EnsureThat;
using Product.Domain.Shared;
using Product.Messages;

namespace Product.Domain.Aggregates.ProductAggregate
{
    public class Product : AggregateRoot
    {
        public string Name { get; private set; }

        public Price Price { get; private set; }

        public bool IsActive { get; private set; }

        private readonly List<Picture> _pictures = new List<Picture>();
        public IReadOnlyCollection<Picture> Pictures => _pictures;

        public Guid ProductCategoryId { get; private set; }

        private Product()
        {

        }

        public Product(string name, Guid categoryId)
        {
            Ensure.That(name).IsNotNullOrWhiteSpace();
            Ensure.That(categoryId).IsNotEmpty();

            ChangeAndEnsure(() =>
            {
                Id = Guid.NewGuid();
                Name = name;
                ProductCategoryId = categoryId;
            }, new ProductEvents.Created(Id));
        }

        public void SetPrice(Price price)
        {
            Ensure.That(price).IsNotNull();

            ChangeAndEnsure(() =>
            {
                Price = price;
            });
        }

        public void AddPicture(Picture pic)
        {
            Ensure.That(pic).IsNotNull();

            ChangeAndEnsure(() =>
            {
                _pictures.Add(pic);
            });
        }

        public void Activate()
        {
            ChangeAndEnsure(() => { IsActive = true; });
        }

        public void Deactivate()
        {
            ChangeAndEnsure(() => { IsActive = false; });
        }

        public void AssignToCategory(Guid categoryId)
        {
            Ensure.That(categoryId).IsNotEmpty();

            ChangeAndEnsure(() => { ProductCategoryId = categoryId; });
        }

        protected override void EnsureValidState()
        {
            var isValid = Id != Guid.Empty && !string.IsNullOrWhiteSpace(Name);

            if (IsActive)
            {
                isValid = isValid && Price != null;
            }

            if (!isValid)
            {
                throw new DomainExceptions.InvalidEntityState(this, $"Invalid {nameof(Product)} state");
            }
        }
    }
}
