﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Domain;
using EnsureThat;
using Product.Domain.Shared;
using Product.Messages;

namespace Product.Domain.Aggregates.ProductCategoryAggregate
{
    public class ProductCategory : AggregateRoot
    {
        public string Name { get; private set; }

        private ProductCategory() { }

        public ProductCategory(string name)
        {
            Ensure.That(name).IsNotNullOrWhiteSpace();

            ChangeAndEnsure(() =>
            {
                Id = Guid.NewGuid();
                Name = name;
            }, new ProductCategoryEvents.Created(Id));
        }

        protected override void EnsureValidState()
        {
            var isValid = Id != Guid.Empty && !string.IsNullOrWhiteSpace(Name);

            if (!isValid)
            {
                throw new DomainExceptions.InvalidEntityState(this, $"Invalid {nameof(ProductCategory)} state after changed");
            }
        }
    }
}
