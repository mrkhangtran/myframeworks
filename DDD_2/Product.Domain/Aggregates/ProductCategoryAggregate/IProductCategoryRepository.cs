﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Domain;

namespace Product.Domain.Aggregates.ProductCategoryAggregate
{
    public interface IProductCategoryRepository : IRepository<ProductCategory>
    {
        ProductCategory Add(ProductCategory productCategory);

        void Update(ProductCategory productCategory);

        Task<ProductCategory> GetByIdAsync(Guid id);
    }
}
