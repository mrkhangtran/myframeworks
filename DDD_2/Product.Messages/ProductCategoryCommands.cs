﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Product.Messages
{
    public static class ProductCategoryCommands
    {
        public class Create : IRequest<Guid>
        {
            public string Name { get; }

            public Create(string name)
            {
                Name = name;
            }
        }
    }
}
