﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Product.Messages
{
    public static class ProductEvents
    {
        public class Created : INotification
        {
            public Guid Id { get; }

            public Created(Guid id)
            {
                Id = id;
            }
        }
    }
}
