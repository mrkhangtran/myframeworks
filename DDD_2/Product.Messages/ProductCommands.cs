﻿using System;
using MediatR;

namespace Product.Messages
{
    public static class ProductCommands
    {
        public class Create : IRequest<Guid>
        {
            public string Name { get; }

            public decimal Price { get; }

            public string Currency { get; }

            public Guid ProductCategoryId { get; }

            public Create(string name, decimal price, string currency, Guid productCategoryId)
            {
                Name = name;
                Price = price;
                Currency = currency;
                ProductCategoryId = productCategoryId;
            }
        }

        public class Activate : IRequest
        {
            public Guid Id { get; }

            public Activate(Guid id)
            {
                Id = id;
            }
        }
    }
}
