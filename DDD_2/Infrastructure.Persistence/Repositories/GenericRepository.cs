﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repositories
{
    /// <summary>
    /// Generic Repository is considered as an anti-pattern so we don't want to expose a generic repository interface or base class.
    /// However there are still some common codes we can reuse so the idea is to have an internal one for handling common functionalities
    /// </summary>
    public class GenericRepository<T> where T : AggregateRoot
    {
        private readonly DbSet<T> _dbSet; 

        public GenericRepository(DbSet<T> dbSet)
        {
            _dbSet = dbSet;
        }

        public T Add(T product)
        {
            return _dbSet.Add(product).Entity;
        }

        public void Update(T product)
        {
            _dbSet.Update(product);
        }

        public async Task<IEnumerable<T>> GetAsync(QueryObject<T> queryObject)
        {
            var result = await _dbSet.Where(queryObject.AsExpression()).ToListAsync();
            return result;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var result = await _dbSet.FindAsync(id);
            return result;
        }
    }
}
