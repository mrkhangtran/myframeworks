﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Product.Domain.Shared;

namespace Infrastructure.Persistence.Providers
{
    public class CurrencyProvider : ICurrencyProvider
    {
        private readonly ApplicationDbContext _context;

        public CurrencyProvider(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Currency> GetAsync(string currencyCode)
        {
            var currency = await _context.Set<Currency>().FindAsync(currencyCode);
            if (currency == null || !currency.InUse)
            {
                throw new DomainExceptions.CurrencyNotInUseException();
            }

            return currency;
        }
    }
}
