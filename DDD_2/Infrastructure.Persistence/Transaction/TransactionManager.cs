﻿using System;
using System.Data;
using System.Threading.Tasks;
using Core.Application.Transaction;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Transaction
{
    public class TransactionManager : ITransactionManager
    {
        private readonly ApplicationDbContext _dbContext;

        public TransactionManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task ExecuteAsync(Func<Task> action, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            //Use of an EF Core resiliency strategy when using multiple DbContexts within an explicit BeginTransaction():
            //See: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency
            var strategy = _dbContext.Database.CreateExecutionStrategy();
            await strategy.ExecuteAsync(async () =>
            {
                await using var transaction = _dbContext.Database.BeginTransaction(isolationLevel);
                await action();
                transaction.Commit();
            });
        }
    }
}
