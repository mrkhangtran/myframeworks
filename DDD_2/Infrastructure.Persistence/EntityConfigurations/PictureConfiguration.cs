﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Product.Domain.Aggregates.ProductAggregate;

namespace Infrastructure.Persistence.EntityConfigurations
{
    class PictureConfiguration : IEntityTypeConfiguration<Picture>
    {
        public void Configure(EntityTypeBuilder<Picture> builder)
        {
            // table
            builder.ToTable("Pictures");

            // key
            builder.HasKey(x => x.Id);
            builder.Property<Guid>("ProductId")
                .IsRequired();

            // value object
            builder.OwnsOne(x => x.Size);

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Url).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
