﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Product.Domain.Aggregates.ProductAggregate;
using Product.Domain.Aggregates.ProductCategoryAggregate;
using Product.Domain.Shared;

namespace Infrastructure.Persistence.EntityConfigurations
{
    class ProductConfiguration : IEntityTypeConfiguration<Product.Domain.Aggregates.ProductAggregate.Product>
    {
        public void Configure(EntityTypeBuilder<Product.Domain.Aggregates.ProductAggregate.Product> builder)
        {
            // table
            builder.ToTable("Products");

            // key
            builder.HasKey(x => x.Id);
            builder.HasOne<ProductCategory>()
                .WithMany()
                .HasForeignKey(x => x.ProductCategoryId);

            // value object
            builder.OwnsOne(x => x.Price, p => p.HasOne<Currency>().WithMany().HasForeignKey(x => x.CurrencyCode));

            // collection
            builder.Metadata.FindNavigation(nameof(Product.Domain.Aggregates.ProductAggregate.Product.Pictures)).SetPropertyAccessMode(PropertyAccessMode.Field);

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
