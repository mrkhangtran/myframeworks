﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Product.Domain.Aggregates.ProductCategoryAggregate;

namespace Infrastructure.Persistence.EntityConfigurations
{
    class ProductCategoryConfiguration : IEntityTypeConfiguration<ProductCategory>
    {
        public void Configure(EntityTypeBuilder<ProductCategory> builder)
        {
            // table
            builder.ToTable("ProductCategories");

            // key
            builder.HasKey(x => x.Id);
            builder.HasMany<Product.Domain.Aggregates.ProductAggregate.Product>()
                .WithOne();

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
