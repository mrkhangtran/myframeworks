﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Product.Domain.Shared;

namespace Infrastructure.Persistence.EntityConfigurations
{
    class CurrencyConfiguration : IEntityTypeConfiguration<Currency>
    {
        public void Configure(EntityTypeBuilder<Currency> builder)
        {
            // table
            builder.ToTable("Currencies");

            // key
            builder.HasKey(x => x.CurrencyCode);
        }
    }
}
