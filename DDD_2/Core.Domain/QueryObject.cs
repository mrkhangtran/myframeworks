﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqKit;

namespace Core.Domain
{
    public abstract class QueryObject<T> where T : AggregateRoot
    {
        private readonly Dictionary<string, string> _orderBy = new Dictionary<string, string>();

        public abstract Expression<Func<T, bool>> AsExpression();

        public Expression<Func<T, bool>> And(QueryObject<T> queryObject)
        {
            return And(queryObject.AsExpression());
        }

        public void AddOrderBy(string fieldName, bool isDescending)
        {
            var direction = "ASC";
            if (isDescending)
            {
                direction = "DESC";
            }

            _orderBy.Add(fieldName, direction);
        }

        public string GetOrderBy()
        {
            var result = string.Join(",", _orderBy.Select(x => $"{x.Key} {x.Value}"));
            return result;
        }

        public Expression<Func<T, bool>> Or(QueryObject<T> queryObject)
        {
            return Or(queryObject.AsExpression());
        }

        private Expression<Func<T, bool>> And(Expression<Func<T, bool>> query)
        {
            var originQuery = AsExpression();
            return originQuery == null ? query : originQuery.And(query.Expand());
        }

        private Expression<Func<T, bool>> Or(Expression<Func<T, bool>> query)
        {
            var originQuery = AsExpression();
            return originQuery == null ? query : originQuery.Or(query.Expand());
        }
    }
}
