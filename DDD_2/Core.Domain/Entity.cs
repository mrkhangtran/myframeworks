﻿using System;
using System.Collections.Generic;
using MediatR;

namespace Core.Domain
{
    public abstract class Entity : IAuditable
    {
        public Guid Id { get; protected set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        private readonly List<INotification> _domainEvents = new List<INotification>();
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents;

        public bool IsTransient => Id == Guid.Empty;

        /// <summary>
        /// Ensures the state of aggregate is valid after state changed.
        /// </summary>
        protected abstract void EnsureValidState();

        /// <summary>
        /// Executes the state changes, ensure validity and raises additional domain events.
        /// Any state changes in aggregate should be wrapped in this method.
        /// </summary>
        /// <param name="changeState"></param>
        /// <param name="domainEvents"></param>
        protected void ChangeAndEnsure(Action changeState, params INotification[] domainEvents)
        {
            changeState();
            EnsureValidState();
            _domainEvents.AddRange(domainEvents);
        }

        public void AddDomainEvent(INotification @event)
        {
            _domainEvents.Add(@event);
        }

        public void RemoveDomainEvent(INotification @event)
        {
            _domainEvents.Remove(@event);
        }

        public void ClearDomainEvents()
        {
            _domainEvents.Clear();
        }
    }
}
