﻿namespace Core.Domain
{
    public abstract class AggregateRoot : Entity
    {
        public byte[] RowVersion { get; set; }        
    }
}
