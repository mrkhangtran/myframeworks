﻿using System;

namespace Infrastructure.RecordLock
{
    public class RecordLock
    {
        public string EntityId { get; set; }

        public string EntityName { get; set; }

        public string OwnerId { get; set; }

        public DateTime AcquiredDate { get; set; }
    }
}
