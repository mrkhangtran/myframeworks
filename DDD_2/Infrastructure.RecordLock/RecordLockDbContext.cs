﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Core.Application.Providers;
using Infrastructure.RecordLock.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.RecordLock
{
    public class RecordLockDbContext : DbContext
    {
        public DbSet<RecordLock> RecordLocks { get; set; }

        private readonly IConnectionStringProvider _connectionStringProvider;

        public RecordLockDbContext(DbContextOptions<RecordLockDbContext> options) : base(options)
        {
        }

        public RecordLockDbContext(IConnectionStringProvider connectionStringProvider)
        {
            _connectionStringProvider = connectionStringProvider;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_connectionStringProvider != null)
            {
                var connectionString = _connectionStringProvider.Get();
                optionsBuilder.UseSqlServer(connectionString,
                    sqlOptions =>
                    {
                        sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                    });
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new RecordLockConfiguration());
        }
    }

    // For Migrations
    public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<RecordLockDbContext>
    {
        public RecordLockDbContext CreateDbContext(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var builder = new DbContextOptionsBuilder<RecordLockDbContext>();
            var configuration = config.Build();

            builder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            return new RecordLockDbContext(builder.Options);
        }
    }
}
