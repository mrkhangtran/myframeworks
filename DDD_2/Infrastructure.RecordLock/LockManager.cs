﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Application;
using Core.Application.Providers;
using Core.Application.RecordLock;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.RecordLock
{
    public class LockManager : ILockManager
    {
        private readonly SystemConfig _systemConfig;
        private readonly RecordLockDbContext _dbContext;
        private readonly DbSet<RecordLock> _locks;

        public LockManager(RecordLockDbContext dbContext, IConfigProvider configProvider)
        {
            _systemConfig = configProvider.GetSystemConfig();
            _dbContext = dbContext;
            _locks = dbContext.Set<RecordLock>();
        }

        public async Task AcquireLockAsync<T>(string entityId, string ownerId)
        {
            await RemoveExpiredLock<T>(entityId);

            var hasLock = await HasLockAsync<T>(entityId, ownerId);
            if (!hasLock)
            {
                try
                {
                    var @lock = new RecordLock
                    {
                        EntityId = entityId,
                        EntityName = typeof(T).Name,
                        OwnerId = ownerId,
                        AcquiredDate = DateTime.UtcNow
                    };

                    _locks.Add(@lock);
                    await _dbContext.SaveChangesAsync();
                }
                catch (Exception)
                {
                    throw new RecordLockException("Cannot acquire lock. Entity could have been locked by another user.");
                }
            }
        }

        public async Task ReleaseLockAsync<T>(string entityId, string ownerId)
        {
            var @lock = await _locks.FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityName == typeof(T).Name && x.OwnerId == ownerId);
            if (@lock != null)
            {
                try
                {
                    _locks.Remove(@lock);
                    await _dbContext.SaveChangesAsync();
                }
                catch (Exception)
                {
                    throw new RecordLockException("Unexpected error releasing lock.");
                }
            }
        }

        public async Task<bool> HasLockAsync<T>(string entityId, string ownerId)
        {
            var @lock = await _locks.AsNoTracking().FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityName == typeof(T).Name && x.OwnerId == ownerId);
            return @lock != null;
        }

        public async Task ReleaseAllLocksAsync(string ownerId)
        {
            var locks = await _locks.Where(x => x.OwnerId == ownerId).ToListAsync();
            locks.ForEach(x => _locks.Remove(x));

            await _dbContext.SaveChangesAsync();
        }

        public async Task RenewLockAsync<T>(string entityId, string ownerId)
        {
            var @lock = await _locks.FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityName == typeof(T).Name && x.OwnerId == ownerId);
            if (@lock != null)
            {
                if (@lock.AcquiredDate.AddSeconds(_systemConfig.RecordLockExpirySeconds) <= DateTime.UtcNow)
                {
                    throw new RecordLockException("Lock expired");
                }

                @lock.AcquiredDate = DateTime.UtcNow;
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new RecordLockException("Lock not found for user");
            }
        }

        private async Task RemoveExpiredLock<T>(string entityId)
        {
            var @lock = await _locks.FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityName == typeof(T).Name);
            if (@lock != null && @lock.AcquiredDate.AddSeconds(_systemConfig.RecordLockExpirySeconds) <= DateTime.UtcNow)
            {
                _locks.Remove(@lock);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
