﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Product.Application.Queries;
using Product.Application.Queries.Models;
using Product.Messages;

namespace Product.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductCategoriesController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IProductCategoryQuery _productCategoryQuery;

        public ProductCategoriesController(IMediator mediator, IProductCategoryQuery productCategoryQuery)
        {
            _mediator = mediator;
            _productCategoryQuery = productCategoryQuery;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductCategoryReturnModels.ProductCategory>> Get(Guid id)
        {
            var category = await _productCategoryQuery.GetProductCategoryByIdAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            return category;
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> Post([FromBody]ProductCategoryCommands.Create cmd)
        {
            var categoryId = await _mediator.Send(cmd);
            return categoryId;
        }
    }
}
