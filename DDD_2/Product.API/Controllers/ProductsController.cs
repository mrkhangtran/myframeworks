﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Product.Application.Queries;
using Product.Application.Queries.Models;
using Product.Messages;

namespace Product.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IProductQuery _productQuery;

        public ProductsController(IMediator mediator, IProductQuery productQuery)
        {
            _mediator = mediator;
            _productQuery = productQuery;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductReturnModels.ProductDetails>> Get(Guid id)
        {
            var product = await _productQuery.GetProductByIdAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> Post([FromBody]ProductCommands.Create cmd)
        {
            var productId = await _mediator.Send(cmd);
            return productId;
        }
    }
}
