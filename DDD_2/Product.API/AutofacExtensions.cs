﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Autofac;
using FluentValidation;
using MediatR;
using Product.Application.CommandHandlers.Products;
using Product.Application.EventHandlers;
using Product.Infrastructure.Queries;
using Product.Infrastructure.Repositories;

namespace Product.API
{
    public static class AutofacExtensions
    {
        public static void RegisterProductModule(this ContainerBuilder builder)
        {
            // Persistence
            builder.RegisterAssemblyTypes(typeof(ProductRepository).GetTypeInfo().Assembly)
                .InNamespace("Product.Infrastructure.Repositories")
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            // Query
            builder.RegisterAssemblyTypes(typeof(ProductQuery).GetTypeInfo().Assembly)
                .InNamespace("Product.Infrastructure.Queries")
                .AsImplementedInterfaces()
                .SingleInstance();

            // finally register our custom code (individually, or via assembly scanning)
            // - requests & handlers as transient, i.e. InstancePerDependency()
            // - pre/post-processors as scoped/per-request, i.e. InstancePerLifetimeScope()
            // - behaviors as transient, i.e. InstancePerDependency()

            // CommandHandlers
            builder.RegisterAssemblyTypes(typeof(CreateProductCommandHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>))
                .InstancePerDependency();

            // Command's Validators
            builder.RegisterAssemblyTypes(typeof(CreateProductCommandValidator).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IValidator<>))
                .InstancePerDependency();

            // DomainEventHandlers
            builder.RegisterAssemblyTypes(typeof(NotifyAdminWhenProductCreatedEventHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(INotificationHandler<>))
                .InstancePerDependency();
        }
    }
}
