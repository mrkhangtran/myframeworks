﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Application;
using Core.Application.Providers;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.Configuration
{
    public class AppSettingConfigProvider : IConfigProvider
    {
        private readonly IConfiguration _configuration;
        private SystemConfig _systemConfig;

        public AppSettingConfigProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SystemConfig GetSystemConfig()
        {
            if (_systemConfig != null)
            {
                return _systemConfig;
            }

            _systemConfig = new SystemConfig();
            _configuration.GetSection("SystemConfig").Bind(_systemConfig);

            return _systemConfig;
        }
    }
}
