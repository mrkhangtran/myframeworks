﻿using System;
using Core.Application.Providers;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.Configuration
{
    public class AppSettingConnectionStringProvider : IConnectionStringProvider
    {
        private readonly IConfiguration _configuration;

        public AppSettingConnectionStringProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string Get(string name = "DefaultConnection")
        {
            return _configuration.GetConnectionString(name);
        }
    }
}
