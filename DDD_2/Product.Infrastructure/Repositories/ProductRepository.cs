﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Domain;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Repositories;
using Product.Domain.Aggregates.ProductAggregate;

namespace Product.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly GenericRepository<Product.Domain.Aggregates.ProductAggregate.Product> _genericRepo;

        public IUnitOfWork UnitOfWork => _context;

        public ProductRepository(ApplicationDbContext context)
        {
            _context = context;
            _genericRepo = new GenericRepository<Product.Domain.Aggregates.ProductAggregate.Product>(_context.Set<Product.Domain.Aggregates.ProductAggregate.Product>());
        }

        public Product.Domain.Aggregates.ProductAggregate.Product Add(Product.Domain.Aggregates.ProductAggregate.Product product)
        {
            return _genericRepo.Add(product);
        }

        public void Update(Product.Domain.Aggregates.ProductAggregate.Product product, byte[] currentRowVersion)
        {
            product.RowVersion = currentRowVersion;
            _genericRepo.Update(product);
        }

        public Task<IEnumerable<Product.Domain.Aggregates.ProductAggregate.Product>> GetAsync(QueryObject<Product.Domain.Aggregates.ProductAggregate.Product> queryObject)
        {
            return _genericRepo.GetAsync(queryObject);
        }

        public Task<Product.Domain.Aggregates.ProductAggregate.Product> GetByIdAsync(Guid id)
        {
            return _genericRepo.GetByIdAsync(id);
        }
    }
}
