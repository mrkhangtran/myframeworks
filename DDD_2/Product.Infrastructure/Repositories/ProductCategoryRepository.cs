﻿using System;
using System.Threading.Tasks;
using Core.Domain;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Repositories;
using Product.Domain.Aggregates.ProductCategoryAggregate;

namespace Product.Infrastructure.Repositories
{
    public class ProductCategoryRepository : IProductCategoryRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly GenericRepository<ProductCategory> _genericRepo;

        public IUnitOfWork UnitOfWork => _context;

        public ProductCategoryRepository(ApplicationDbContext context)
        {
            _context = context;
            _genericRepo = new GenericRepository<ProductCategory>(_context.Set<ProductCategory>());
        }

        public ProductCategory Add(ProductCategory productCategory)
        {
            return _genericRepo.Add(productCategory);
        }

        public void Update(ProductCategory productCategory)
        {
            _genericRepo.Update(productCategory);
        }

        public Task<ProductCategory> GetByIdAsync(Guid id)
        {
            return _genericRepo.GetByIdAsync(id);
        }
    }
}
