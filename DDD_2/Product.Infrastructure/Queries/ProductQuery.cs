﻿using System;
using System.Threading.Tasks;
using Core.Application.Providers;
using Dapper;
using Microsoft.Data.SqlClient;
using Product.Application.Queries;
using Product.Application.Queries.Models;

namespace Product.Infrastructure.Queries
{
    public class ProductQuery : IProductQuery
    {
        private readonly string _connString;

        public ProductQuery(IConnectionStringProvider connectionStringProvider)
        {
            _connString = connectionStringProvider.Get();
        }

        public async Task<ProductReturnModels.ProductDetails> GetProductByIdAsync(Guid id)
        {
            await using var connection = new SqlConnection(_connString);
            var query = @"SELECT *
                          FROM Products
                          WHERE Id = @id";

            var product = await connection.QueryFirstOrDefaultAsync<ProductReturnModels.ProductDetails>(query, new { id });

            return product;
        }
    }
}
