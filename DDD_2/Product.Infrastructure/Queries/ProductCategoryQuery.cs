﻿using System;
using System.Threading.Tasks;
using Core.Application.Providers;
using Dapper;
using Microsoft.Data.SqlClient;
using Product.Application.Queries;
using Product.Application.Queries.Models;

namespace Product.Infrastructure.Queries
{
    public class ProductCategoryQuery : IProductCategoryQuery
    {
        private readonly string _connString;

        public ProductCategoryQuery(IConnectionStringProvider connectionStringProvider)
        {
            _connString = connectionStringProvider.Get();
        }

        public async Task<ProductCategoryReturnModels.ProductCategory> GetProductCategoryByIdAsync(Guid id)
        {
            await using var connection = new SqlConnection(_connString);
            var query = @"SELECT *
                          FROM ProductCategories
                          WHERE Id = @id";

            var product = await connection.QueryFirstOrDefaultAsync<ProductCategoryReturnModels.ProductCategory>(query, new { id });

            return product;
        }
    }
}
