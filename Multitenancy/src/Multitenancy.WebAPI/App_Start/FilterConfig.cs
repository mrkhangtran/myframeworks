﻿using System.Web;
using System.Web.Mvc;
using Multitenancy.WebAPI.Infrastructure;

namespace Multitenancy.WebAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
