﻿using System.ComponentModel.DataAnnotations;

namespace Multitenancy.Entity.Domain
{
    /// <summary>
    /// Base class for all entities
    /// </summary>
    public abstract class BaseEntity
    {
        [Key]
        public virtual int Id { get; set; }

        [Timestamp]
        [ConcurrencyCheck]
        public byte[] RowVersion { get; set; }
    }
}
