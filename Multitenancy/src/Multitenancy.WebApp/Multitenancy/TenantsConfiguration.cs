﻿using System.Collections.Generic;

namespace Multitenancy.WebApp.Multitenancy
{
    public class TenantsConfiguration
    {
        public List<Tenant> Tenants { get; set; }
    }
}
