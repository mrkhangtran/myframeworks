﻿using System.Collections.Generic;

namespace Multitenancy.WebApp.Multitenancy
{
    public class Tenant
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public List<string> Hostnames { get; set; }

        public string Theme { get; set; }
    }
}
