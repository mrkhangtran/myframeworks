﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Seedwork;

namespace DDD.Application.Ports.Infrastructure.Concurrency
{
    public interface ILockManager
    {
        Task AcquireLockAsync<T>(int entityId, string ownerId) where T : Entity;

        Task ReleaseLockAsync<T>(int entityId, string ownerId) where T : Entity;

        Task<bool> HasLockAsync<T>(int entityId, string ownerId) where T : Entity;

        Task ReleaseAllLocksAsync(string ownerId);
        Task RenewLockAsync<T>(int entityId, string ownerId);
    }
}
