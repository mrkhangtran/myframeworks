﻿using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace DDD.Application.Ports.Infrastructure.DbTransactionAdapter
{
    public interface IDbTransactionManager
    {
        Task<IDbTransactionAdapter> BeginTransactionAsync(IsolationLevel isolationLevel);
        DbConnection GetDbConnection();
    }
}
