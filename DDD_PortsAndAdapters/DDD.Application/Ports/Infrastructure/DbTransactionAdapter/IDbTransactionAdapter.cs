﻿using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace DDD.Application.Ports.Infrastructure.DbTransactionAdapter
{
    public interface IDbTransactionAdapter : IDisposable
    {
        Task CommitAsync();

        Task RollbackAsync();

        DbTransaction GetDbTransaction();
    }
}
