﻿namespace DDD.Application.Ports.Infrastructure.Providers
{
    public interface IConnectionStringProvider
    {
        string GetDbConnectionString();
    }
}
