﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Application.Ports.Infrastructure.Providers
{
    public interface IConfigProvider
    {
        SystemConfig GetSystemConfig();
    }
}
