﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Application.Ports.Infrastructure.Persistence
{
    public interface IIdentifiedRequestManager
    {
        Task<bool> ExistsAsync(Guid id);

        Task CreateRequestForCommandAsync<T>(Guid id);
    }
}
