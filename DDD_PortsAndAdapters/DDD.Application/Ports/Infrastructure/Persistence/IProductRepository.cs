﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using DDD.Application.Seedwork;
using DDD.Domain.Aggregates.ProductAggregate;

namespace DDD.Application.Ports.Infrastructure.Persistence
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
