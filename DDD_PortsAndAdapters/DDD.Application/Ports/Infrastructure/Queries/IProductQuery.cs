﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DDD.Application.Queries.ReturnModels;

namespace DDD.Application.Ports.Infrastructure.Queries
{
    public interface IProductQuery
    {
        Task<ProductReturnModel> GetProductByIdAsync(int id);
        Task<ProductReturnModel> GetProductByIdAsync(int id, System.Data.Common.DbTransaction transaction);
    }
}
