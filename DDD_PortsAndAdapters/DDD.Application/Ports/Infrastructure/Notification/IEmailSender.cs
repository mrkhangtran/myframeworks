﻿using System.Threading.Tasks;

namespace DDD.Application.Ports.Infrastructure.Notification
{
    public interface IEmailSender
    {
        Task SendAsync(string subject, string body, string to);
    }
}
