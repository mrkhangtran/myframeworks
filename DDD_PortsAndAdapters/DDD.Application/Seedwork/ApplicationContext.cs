﻿using System;
using DDD.Domain.Seedwork;

namespace DDD.Application.Seedwork
{
    public class ApplicationContext
    {
        public UserPrincipal Principal { get; } = new UserPrincipal();
    }

    public class UserPrincipal
    {
        public string UserId { get; set; }

        public string Username { get; set; }

        public string Role { get; set; }

        // TODO: temporarily hard-coded for vietnam
        public TimeZoneInfo TimeZone { get; set; } = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");

        public DateTime ConvertUtcToUserLocalTime(DateTime? utc = null)
        {
            if (!utc.HasValue)
            {
                utc = DateTime.UtcNow;
            }

            return TimeZoneInfo.ConvertTimeFromUtc(utc.Value, TimeZone);
        }

        public DateTime ConvertUserLocalTimeToUtc(DateTime local)
        {
            return TimeZoneInfo.ConvertTimeToUtc(local, TimeZone);
        }
    }
}
