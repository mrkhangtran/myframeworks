﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DDD.Application.Ports.Infrastructure.Persistence;
using MediatR;

namespace DDD.Application.Seedwork
{
    public class IdentifiedCommand<T, R> : IRequest<R> where T : IRequest<R>
    {
        public T Command { get; }

        public Guid Id { get; }

        public IdentifiedCommand(T command, Guid id)
        {
            Command = command;
            Id = id;
        }
    }

    public class IdentifiedCommandHandler<T, R> : IRequestHandler<IdentifiedCommand<T, R>, R>
        where T : IRequest<R>
    {
        private readonly IMediator _mediator;
        private readonly IIdentifiedRequestManager _requestManager;

        public IdentifiedCommandHandler(IMediator mediator, IIdentifiedRequestManager requestManager)
        {
            _mediator = mediator;
            _requestManager = requestManager;
        }

        protected virtual R CreateResultForDuplicatedRequest()
        {
            return default(R);
        }

        public async Task<R> Handle(IdentifiedCommand<T, R> request, CancellationToken cancellationToken)
        {
            var alreadyExists = await _requestManager.ExistsAsync(request.Id);
            if (alreadyExists)
            {
                return CreateResultForDuplicatedRequest();
            }

            await _requestManager.CreateRequestForCommandAsync<T>(request.Id);
            var result = await _mediator.Send(request.Command, cancellationToken);

            return result;
        }
    }
}
