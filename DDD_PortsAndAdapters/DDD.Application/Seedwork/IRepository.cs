﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DDD.Domain.Seedwork;

namespace DDD.Application.Seedwork
{
    public interface IRepository<T> where T : AggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }

        Task<IEnumerable<T>> QueryAsync(QueryObject<T> queryObject, List<string> includes, int? maxItems);

        Task<PagedQueryResult<T>> PagedQueryAsync(QueryObject<T> queryObject, List<string> includes, int pageNumber, int pageSize);

        Task<T> GetByIdAsync(int id);

        Task InsertAsync(T entity);

        Task InsertRangeAsync(IEnumerable<T> entities);

        void Update(T entity, string currentRowVersion);

        void Update(T entity, byte[] currentRowVersion);

        void Delete(T entity, string currentRowVersion);

        void Delete(T entity, byte[] currentRowVersion);
    }
}
