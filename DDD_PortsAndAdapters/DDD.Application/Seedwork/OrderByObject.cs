﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDD.Application.Seedwork
{
    public class OrderByObject
    {
        private readonly Dictionary<string, string> _orderBy = new Dictionary<string, string>();

        public void AddOrderBy(string fieldName, bool isDescending)
        {
            var direction = "ASC";
            if (isDescending)
            {
                direction = "DESC";
            }

            _orderBy.Add(fieldName, direction);
        }

        public string AsString()
        {
            if (!_orderBy.Any())
            {
                throw new InvalidOperationException("OrderBy list cannot be empty. Use AddOrderBy method to add at least 1 OrderBy");
            }

            var result = string.Join(",", _orderBy.Select(x => $"{x.Key} {x.Value}"));
            return result;
        }
    }
}
