﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Application.Seedwork
{
    public class PagedQueryResult<T>
    {
        public IEnumerable<T> Items { get; set; }

        public int ItemCount { get; set; }

        public int PageCount { get; set; }
    }
}
