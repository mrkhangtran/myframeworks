﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DDD.Application.Ports.Infrastructure.Persistence;
using MediatR;

namespace DDD.Application.Seedwork
{
    public abstract class CommandHandlerBase<TCommand, TResult> : IRequestHandler<TCommand, TResult>
        where TCommand : IRequest<TResult>
    {
        protected ApplicationContext AppContext;

        protected CommandHandlerBase(ApplicationContext appContext)
        {
            AppContext = appContext;
        }

        public abstract Task<TResult> Handle(TCommand rq, CancellationToken cancellationToken);
    }
}
