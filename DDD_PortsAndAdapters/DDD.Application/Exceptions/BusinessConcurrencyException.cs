﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Application.Exceptions
{
    public class BusinessConcurrencyException : Exception
    {
        public BusinessConcurrencyException()
        { }

        public BusinessConcurrencyException(string message)
            : base(message)
        { }
    }
}
