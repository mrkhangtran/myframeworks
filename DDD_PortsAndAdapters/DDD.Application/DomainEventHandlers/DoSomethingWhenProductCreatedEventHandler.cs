﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DDD.Domain.Events.ProductEvents;
using MediatR;

namespace DDD.Application.DomainEventHandlers
{
    public class DoSomethingWhenProductCreatedEventHandler : INotificationHandler<ProductCreatedEvent>
    {
        public Task Handle(ProductCreatedEvent @event, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
