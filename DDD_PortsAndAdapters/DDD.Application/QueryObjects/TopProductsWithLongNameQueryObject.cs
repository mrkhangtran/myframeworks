﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using DDD.Application.Seedwork;
using DDD.Domain.Aggregates.ProductAggregate;

namespace DDD.Application.QueryObjects
{
    public class TopProductsWithLongNameQueryObject : QueryObject<Product>
    {
        public int MinimumNameLength { get; set; }

        public override Expression<Func<Product, bool>> AsExpression()
        {
            return p => p.Name.Length >= MinimumNameLength;
        }
    }
}
