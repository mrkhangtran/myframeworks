﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Application.Commands.ProductCommands;
using FluentValidation;

namespace DDD.Application.Commands.Validators
{
    public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
    {
        public CreateProductCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
