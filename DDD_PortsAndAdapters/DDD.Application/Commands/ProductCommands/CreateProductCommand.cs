﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace DDD.Application.Commands.ProductCommands
{
    public class CreateProductCommand : IRequest<int>
    {
        public string Name { get; }

        public string Description { get; }

        public CreateProductCommand(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}
