﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DDD.Application.Ports.Infrastructure.Persistence;
using DDD.Application.Seedwork;
using DDD.Domain.Aggregates.ProductAggregate;

namespace DDD.Application.Commands.ProductCommands
{
    public class UpdateProductCommandHandler : CommandHandlerBase<UpdateProductCommand, int>
    {
        private readonly IProductRepository _productRepo;

        public UpdateProductCommandHandler(ApplicationContext appContext, IProductRepository productRepo) : base(appContext)
        {
            _productRepo = productRepo;
        }

        public override async Task<int> Handle(UpdateProductCommand rq, CancellationToken cancellationToken)
        {
            var product = new Product(rq.Id, rq.Name, rq.Description);
            _productRepo.Update(product, rq.RowVersion);

            await _productRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return product.Id;
        }
    }
}
