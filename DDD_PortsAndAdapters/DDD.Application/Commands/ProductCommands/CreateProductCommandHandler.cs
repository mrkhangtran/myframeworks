﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DDD.Application.Ports.Infrastructure.Persistence;
using DDD.Application.Seedwork;
using DDD.Domain.Aggregates.ProductAggregate;

namespace DDD.Application.Commands.ProductCommands
{
    public class CreateProductCommandHandler : CommandHandlerBase<CreateProductCommand, int>
    {
        private readonly IProductRepository _productRepo;

        public CreateProductCommandHandler(ApplicationContext appContext, IProductRepository productRepo) : base(appContext)
        {
            _productRepo = productRepo;
        }

        public override async Task<int> Handle(CreateProductCommand rq, CancellationToken cancellationToken)
        {
            var product = new Product(rq.Name, rq.Description);
            await _productRepo.InsertAsync(product);

            await _productRepo.UnitOfWork.SaveChangesAsync(cancellationToken: cancellationToken);

            return product.Id;
        }
    }
}
