﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace DDD.Application.Commands.ProductCommands
{
    public class UpdateProductCommand : IRequest<int>
    {
        public int Id { get; }

        public string Name { get; }

        public string Description { get; }

        public string RowVersion { get; }

        public UpdateProductCommand(int id, string name, string description, string rowVersion)
        {
            Id = id;
            Name = name;
            Description = description;
            RowVersion = rowVersion;
        }
    }
}
