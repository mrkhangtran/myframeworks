﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Application
{
    public class SystemConfig
    {
        public int RecordLockExpirySeconds { get; }

        public SystemConfig(int recordLockExpirySeconds = 30)
        {
            RecordLockExpirySeconds = recordLockExpirySeconds;
        }
    }
}
