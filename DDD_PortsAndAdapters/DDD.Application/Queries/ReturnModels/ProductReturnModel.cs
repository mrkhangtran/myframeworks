﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Infrastructure.Utilities.Helpers;
using Newtonsoft.Json;

namespace DDD.Application.Queries.ReturnModels
{
    public class ProductReturnModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [JsonProperty("RowVersion")]
        public string RowVersionString => ByteArrayHelper.ToHex(RowVersion);

        [JsonIgnore]
        public byte[] RowVersion { get; set; }
    }
}
