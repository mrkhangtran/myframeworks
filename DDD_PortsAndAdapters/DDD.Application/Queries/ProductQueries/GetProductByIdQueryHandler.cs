﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DDD.Application.Ports.Infrastructure.Providers;
using DDD.Application.Ports.Infrastructure.Queries;
using DDD.Application.Queries.ReturnModels;
using DDD.Application.Seedwork;

namespace DDD.Application.Queries.ProductQueries
{
    public class GetProductByIdQueryHandler : QueryHandlerBase<GetProductByIdQuery, ProductReturnModel>
    {
        private readonly IProductQuery _productQuery;

        public GetProductByIdQueryHandler(ApplicationContext appContext, IProductQuery productQuery) : base(appContext)
        {
            _productQuery = productQuery;
        }

        public override async Task<ProductReturnModel> Handle(GetProductByIdQuery rq, CancellationToken cancellationToken)
        {
            var product = await _productQuery.GetProductByIdAsync(rq.Id);
            return product;
        }
    }
}
