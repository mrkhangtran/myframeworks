﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Application.Queries.ReturnModels;
using MediatR;

namespace DDD.Application.Queries.ProductQueries
{
    public class GetProductByIdQuery : IRequest<ProductReturnModel>
    {
        public int Id { get; set; }
    }
}
