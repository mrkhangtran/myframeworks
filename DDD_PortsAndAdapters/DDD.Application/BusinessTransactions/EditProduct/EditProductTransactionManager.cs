﻿using System;
using System.Data;
using System.Threading.Tasks;
using DDD.Application.Exceptions;
using DDD.Application.Ports.Infrastructure.Concurrency;
using DDD.Application.Ports.Infrastructure.DbTransactionAdapter;
using DDD.Application.Ports.Infrastructure.Persistence;
using DDD.Application.Ports.Infrastructure.Queries;
using DDD.Application.Seedwork;
using DDD.Domain.Aggregates.ProductAggregate;
using DDD.Domain.Exceptions;

namespace DDD.Application.BusinessTransactions.EditProduct
{
    public class EditProductTransactionManager
    {
        private readonly ILockManager _lockManager;
        private readonly IProductQuery _productQuery;
        private readonly ApplicationContext _appContext;
        private readonly IProductRepository _productRepo;
        private readonly IDbTransactionManager _dbTransactionManager;

        public EditProductTransactionManager(ILockManager lockManager, IProductQuery productQuery, ApplicationContext appContext, IProductRepository productRepo, IDbTransactionManager dbTransactionManager)
        {
            _lockManager = lockManager;
            _productQuery = productQuery;
            _appContext = appContext;
            _productRepo = productRepo;
            _dbTransactionManager = dbTransactionManager;
        }

        public async Task<EditProductViewModel> StartTransactionAsync(int productId)
        {
            // Serializable here to make sure we have a lock on the latest version of data being locked
            using (var transaction = await _dbTransactionManager.BeginTransactionAsync(IsolationLevel.Serializable))
            {
                try
                {
                    await _lockManager.AcquireLockAsync<Product>(productId, _appContext.Principal.UserId);

                    var product = await _productQuery.GetProductByIdAsync(productId, transaction.GetDbTransaction());
                    if (product == null)
                    {
                        throw new DomainException("Product not found");
                    }

                    var vm = new EditProductViewModel
                    {
                        Product = new EditProductViewModel.ProductViewModel
                        {
                            Id = product.Id,
                            Name = product.Name,
                            Description = product.Description,
                            RowVersion = product.RowVersionString
                        }
                    };

                    await transaction.CommitAsync();

                    return vm;
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
        }

        public async Task CompleteTransactionAsync(EditProductViewModel vm)
        {
            using (var transaction = await _dbTransactionManager.BeginTransactionAsync(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var hasLock = await _lockManager.HasLockAsync<Product>(vm.Product.Id, _appContext.Principal.UserId);
                    if (!hasLock)
                    {
                        throw new BusinessConcurrencyException("User does not have lock on product");
                    }

                    var product = new Product(vm.Product.Id, vm.Product.Name, vm.Product.Description);
                    _productRepo.Update(product, vm.Product.RowVersion);
                    await _productRepo.UnitOfWork.SaveChangesAsync();

                    await _lockManager.ReleaseLockAsync<Product>(product.Id, _appContext.Principal.UserId);

                    await transaction.CommitAsync();
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
        }
    }
}
