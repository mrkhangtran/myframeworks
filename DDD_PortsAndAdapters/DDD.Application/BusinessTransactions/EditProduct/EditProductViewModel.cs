﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Application.BusinessTransactions.EditProduct
{
    public class EditProductViewModel
    {
        public ProductViewModel Product { get; set; }

        public class ProductViewModel
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string Description { get; set; }

            public string RowVersion { get; set; }
        }
    }
}
