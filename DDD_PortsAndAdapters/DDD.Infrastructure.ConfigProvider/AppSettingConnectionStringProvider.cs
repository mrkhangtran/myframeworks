﻿using System;
using DDD.Application.Ports.Infrastructure.Providers;
using Microsoft.Extensions.Configuration;

namespace DDD.Infrastructure.ConfigProvider
{
    public class AppSettingConnectionStringProvider : IConnectionStringProvider
    {
        private readonly IConfiguration _configuration;

        public AppSettingConnectionStringProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GetDbConnectionString()
        {
            return _configuration.GetConnectionString("AppConnection");
        }
    }
}
