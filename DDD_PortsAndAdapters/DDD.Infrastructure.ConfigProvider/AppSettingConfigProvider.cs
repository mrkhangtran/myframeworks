﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Application;
using DDD.Application.Ports.Infrastructure.Providers;
using Microsoft.Extensions.Configuration;

namespace DDD.Infrastructure.ConfigProvider
{
    public class AppSettingConfigProvider : IConfigProvider
    {
        private readonly IConfiguration _configuration;
        private SystemConfig _systemConfig;

        public AppSettingConfigProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SystemConfig GetSystemConfig()
        {
            if (_systemConfig != null)
            {
                return _systemConfig;
            }

            _systemConfig = new SystemConfig();
            _configuration.GetSection("SystemConfig").Bind(_systemConfig);

            return _systemConfig;
        }
    }
}
