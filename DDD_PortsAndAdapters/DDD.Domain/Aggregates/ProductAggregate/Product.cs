﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Domain.Seedwork;

namespace DDD.Domain.Aggregates.ProductAggregate
{
    public class Product : AggregateRoot, ISoftDeletable
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        public Product(string name, string description = null)
        {
            Guards.NotNullOrEmpty(name, nameof(name));

            Name = name;
            Description = description;
        }

        public Product(int id, string name, string description = null) : this(name, description)
        {
            Id = id;
        }
    }
}
