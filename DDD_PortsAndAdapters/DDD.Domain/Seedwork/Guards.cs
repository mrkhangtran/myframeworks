﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Domain.Seedwork
{
    public static class Guards
    {
        public static void NotNullOrEmpty(string input, string paramName)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentException($"{paramName} cannot be null or empty!");
            }
        }
    }
}
