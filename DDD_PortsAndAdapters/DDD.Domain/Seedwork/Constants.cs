﻿namespace DDD.Domain.Seedwork
{
    public class RoleConstants
    {
        public const string BOD = "BOD";
        public const string SA = "SA";
        public const string GENERAL_MANAGER = "GeneralManager";
        public const string MANAGER = "Manager";
        public const string NORMAL_USER = "NormalUser";
    }

    public class AuthorizationPolicyConstants
    {
        public const string INTERNAL_USER = "InternalUser";
        public const string SA_BOD_GM = "SA_BOD_GM";
        public const string SA_BOD = "SA_BOD";
    }
}
