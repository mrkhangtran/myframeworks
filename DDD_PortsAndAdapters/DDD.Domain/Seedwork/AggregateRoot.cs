﻿namespace DDD.Domain.Seedwork
{
    public abstract class AggregateRoot : Entity
    {
        public byte[] RowVersion { get; set; }
    }
}
