﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Domain.Aggregates.ProductAggregate;
using MediatR;

namespace DDD.Domain.Events.ProductEvents
{
    public class ProductCreatedEvent : INotification
    {
        public Product CreatedProduct { get; set; }
    }
}
