﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDD.Application.Commands.ProductCommands;
using DDD.Application.Queries.ProductQueries;
using DDD.Application.Queries.ReturnModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DDD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductReturnModel>> Get(int id)
        {
            var query = new GetProductByIdQuery
            {
                Id = id
            };

            var product = await _mediator.Send(query);
            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post([FromBody]CreateProductCommand cmd)
        {
            var productId = await _mediator.Send(cmd);
            return productId;
        }
    }
}
