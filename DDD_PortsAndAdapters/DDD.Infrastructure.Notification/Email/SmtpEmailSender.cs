﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DDD.Application.Ports.Infrastructure.Notification;
using MailKit.Net.Smtp;
using MimeKit;

namespace DDD.Infrastructure.Notification.Email
{
    public class SmtpEmailSender : IEmailSender
    {
        public async Task SendAsync(string subject, string body, string to)
        {
            // TODO - remove hardcode

            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress("info@epswifi.com"));
            mimeMessage.To.Add(new MailboxAddress(to));
            mimeMessage.Subject = subject;
            var bodyBuilder = new BodyBuilder { HtmlBody = body };
            mimeMessage.Body = bodyBuilder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587, false);
                await client.AuthenticateAsync("info@epswifi.com", "Epswifi@2019");
                await client.SendAsync(mimeMessage);
                await client.DisconnectAsync(true);
            }
        }
    }
}
