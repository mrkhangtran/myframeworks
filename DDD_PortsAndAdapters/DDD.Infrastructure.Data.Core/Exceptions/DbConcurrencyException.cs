﻿using System;

namespace DDD.Infrastructure.Persistence.Core.Exceptions
{
    public class DbConcurrencyException : Exception
    {
    }
}
