﻿using System;

namespace DDD.Infrastructure.Persistence.Core.Concurrency
{
    public class RecordLock
    {
        public int EntityId { get; set; }

        public string EntityName { get; set; }

        public string OwnerId { get; set; }

        public DateTime AcquiredDate { get; set; }
    }
}
