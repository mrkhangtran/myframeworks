﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DDD.Application.Ports.Infrastructure.Providers;
using DDD.Application.Ports.Infrastructure.Queries;
using DDD.Application.Queries.ReturnModels;
using DDD.Application.Seedwork;

namespace DDD.Infrastructure.Query.Dapper
{
    public class ProductQuery : IProductQuery
    {
        private readonly string _connectionString;

        public ProductQuery(IConnectionStringProvider connectionStringProvider)
        {
            _connectionString = connectionStringProvider.GetDbConnectionString();
        }

        public Task<ProductReturnModel> GetProductByIdAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    return GetProductByIdAsync(id, transaction);
                }
            }
        }

        public async Task<ProductReturnModel> GetProductByIdAsync(int id, DbTransaction transaction)
        {
            var connection = transaction.Connection;

            var query = @"SELECT *
                          FROM Products
                          WHERE Id = @id";

            var product = await connection.QueryFirstOrDefaultAsync<ProductReturnModel>(query, new { id }, transaction);

            return product;
        }
    }
}
