﻿using System;
using System.Reflection;
using Autofac;
using DDD.Application.Behaviors;
using DDD.Application.Commands.ProductCommands;
using DDD.Application.Commands.Validators;
using DDD.Application.DomainEventHandlers;
using FluentValidation;
using MediatR;
using Module = Autofac.Module;

namespace DDD.DI.Autofac
{
    public class MediatorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Register Mediator itself
            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly).AsImplementedInterfaces().InstancePerLifetimeScope();

            // Register request and notification handlers for Command/CommandHandler and DomainEvent/DomainEventHandler
            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            // finally register our custom code (individually, or via assembly scanning)
            // - requests & handlers as transient, i.e. InstancePerDependency()
            // - pre/post-processors as scoped/per-request, i.e. InstancePerLifetimeScope()
            // - behaviors as transient, i.e. InstancePerDependency()

            // Register all the CommandHandlers
            builder.RegisterAssemblyTypes(typeof(CreateProductCommandHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>))
                .InstancePerDependency();

            // Register all the Command's Validators
            builder.RegisterAssemblyTypes(typeof(CreateProductCommandValidator).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IValidator<>))
                .InstancePerDependency();

            // Register the DomainEventHandlers
            builder.RegisterAssemblyTypes(typeof(DoSomethingWhenProductCreatedEventHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(INotificationHandler<>))
                .InstancePerDependency();

            // Register behaviors
            builder.RegisterGeneric(typeof(ValidatorBehavior<,>)).As(typeof(IPipelineBehavior<,>)).InstancePerDependency();
        }
    }
}
