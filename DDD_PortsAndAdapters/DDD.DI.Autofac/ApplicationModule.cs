﻿using System.Reflection;
using Autofac;
using DDD.Application.Ports.Infrastructure.Providers;
using DDD.Application.Seedwork;
using DDD.Infrastructure.ConfigProvider;
using DDD.Infrastructure.Persistence.EF;
using DDD.Infrastructure.Persistence.EF.DbTransactionAdapter;
using DDD.Infrastructure.Persistence.EF.Repositories;
using DDD.Infrastructure.Query.Dapper;
using Module = Autofac.Module;

namespace DDD.DI.Autofac
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // DbContext
            builder.RegisterType<ApplicationDbContext>().InstancePerLifetimeScope();

            // Persistence
            builder.RegisterAssemblyTypes(typeof(ProductRepository).GetTypeInfo().Assembly)
                .AsImplementedInterfaces()
                .Except<DbTransactionAdapter>()
                .InstancePerLifetimeScope();

            // Query
            builder.RegisterAssemblyTypes(typeof(ProductQuery).GetTypeInfo().Assembly)
                .AsImplementedInterfaces()
                .SingleInstance();

            // Providers
            builder.RegisterType<AppSettingConnectionStringProvider>().As<IConnectionStringProvider>().SingleInstance();

            // Application Context
            builder.RegisterType<ApplicationContext>().AsSelf().InstancePerLifetimeScope();
        }
    }
}
