﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DDD.Application;
using DDD.Application.BusinessTransactions.EditProduct;
using DDD.Application.Ports.Infrastructure.Concurrency;
using DDD.Application.Ports.Infrastructure.Providers;
using DDD.Application.Seedwork;
using DDD.Domain.Aggregates.ProductAggregate;
using DDD.Domain.Exceptions;
using DDD.Infrastructure.ConfigProvider;
using DDD.Infrastructure.Persistence.Core.Concurrency;
using DDD.Infrastructure.Persistence.EF;
using DDD.Infrastructure.Persistence.EF.Concurrency;
using DDD.Infrastructure.Persistence.EF.DbTransactionAdapter;
using DDD.Infrastructure.Persistence.EF.Repositories;
using DDD.Infrastructure.Query.Dapper;
using DDD.Tests.Setup;
using DDD.Tests.Setup.Fixtures;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Xunit;

namespace DDD.Tests.Application.BusinessTransactions
{
    public class EditProductTransactionManagerTests : IClassFixture<SqlServerApplicationDbContextFixture>, IDisposable
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ApplicationContext _appContext;
        private readonly EditProductTransactionManager _editProductTransactionManager;
        private readonly ILockManager _lockManager;

        /// <summary>
        /// Setup per test
        /// </summary>
        public EditProductTransactionManagerTests(SqlServerApplicationDbContextFixture dbContextFixture)
        {
            _appContext = Helper.CreateAdminApplicationContext();

            // setup SQL Server db context (due to transactional testing then we need to test with real database instead of in-memory db)
            _dbContext = dbContextFixture.DbContext;

            // setup LockManager
            var configProvider = Substitute.For<IConfigProvider>();
            configProvider.GetSystemConfig().Returns(new SystemConfig(recordLockExpirySeconds: 5));
            _lockManager = new LockManager(_dbContext, configProvider);

            // setup ProductQuery
            var connectionStringProvider = Helper.CreateConnectionStringProvider();
            var productQuery = new ProductQuery(connectionStringProvider);

            // Setup ProductRepository
            var productRepo = new ProductRepository(_dbContext);

            // Setup DbTransactionManager
            var transactionManager = new DbTransactionManager(_dbContext);

            // Setup EditProductTransactionManager
            _editProductTransactionManager = new EditProductTransactionManager(_lockManager, productQuery, _appContext, productRepo, transactionManager);
        }

        [Fact]
        public async Task StartTransactionAsync_Success()
        {
            // Arrange
            var product = new Product(1, "aaa");
            await _dbContext.Set<Product>().AddAsync(product);
            await _dbContext.SaveChangesAsync();

            // Act
            await _editProductTransactionManager.StartTransactionAsync(product.Id);
            var hasLock = await _lockManager.HasLockAsync<Product>(product.Id, _appContext.Principal.UserId);

            // Assert
            Assert.True(hasLock);
        }

        [Fact]
        public async Task StartTransactionAsync_Failed_Product_Not_Found()
        {
            // Arrange
            var productId = 1;

            // Act
            var exception = await Assert.ThrowsAsync<DomainException>(() => _editProductTransactionManager.StartTransactionAsync(productId));
            var locksCount = await _dbContext.Set<RecordLock>().CountAsync();

            // Assert
            Assert.Contains("Product not found", exception.Message);
            Assert.Equal(0, locksCount);
        }

        /// <summary>
        /// Teardown per test
        /// </summary>
        public void Dispose()
        {
            CleanLockTable();
            CleanProductTable();
        }

        private void CleanLockTable()
        {
            var connectionString = Helper.CreateConnectionStringProvider().GetDbConnectionString();
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                connection.Execute("DELETE RecordLocks");
            }
        }

        private void CleanProductTable()
        {
            var connectionString = Helper.CreateConnectionStringProvider().GetDbConnectionString();
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                connection.Execute("DELETE Products");
            }
        }
    }
}
