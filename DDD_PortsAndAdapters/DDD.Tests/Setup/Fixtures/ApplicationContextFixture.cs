﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Application.Seedwork;

namespace DDD.Tests.Setup.Fixtures
{
    public class ApplicationContextFixture
    {
        public ApplicationContext AdminApplicationContext { get; }

        public ApplicationContextFixture()
        {
            AdminApplicationContext = Helper.CreateAdminApplicationContext();
        }
    }
}
