﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Application.Seedwork;
using DDD.Infrastructure.Persistence.EF;
using Microsoft.EntityFrameworkCore;

namespace DDD.Tests.Setup.Fixtures
{
    public class SqlServerApplicationDbContextFixture : IDisposable
    {
        public ApplicationDbContext DbContext { get; } 

        public SqlServerApplicationDbContextFixture()
        {
            var applicationContext = Helper.CreateAdminApplicationContext();

            DbContext = Helper.CreateSqlServerDbContext(applicationContext);
            DbContext.Database.Migrate();
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}
