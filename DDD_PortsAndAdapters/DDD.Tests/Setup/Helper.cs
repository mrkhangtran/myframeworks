﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DDD.Application.Ports.Infrastructure.Providers;
using DDD.Application.Seedwork;
using DDD.Infrastructure.ConfigProvider;
using DDD.Infrastructure.Persistence.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DDD.Tests.Setup
{
    public class Helper
    {
        public static ApplicationDbContext CreateInMemoryDbContext(string dbName, ApplicationContext appContext)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
            return new ApplicationDbContext(builder.Options, appContext);
        }

        public static ApplicationDbContext CreateSqlServerDbContext(ApplicationContext appContext)
        {
            var connectionStringProvider = CreateConnectionStringProvider();
            var connectionString = connectionStringProvider.GetDbConnectionString();
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseSqlServer(connectionString);

            return new ApplicationDbContext(builder.Options, appContext);
        }

        public static IConnectionStringProvider CreateConnectionStringProvider()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            IConfiguration config = builder.Build();

            return new AppSettingConnectionStringProvider(config);
        }

        public static ApplicationContext CreateAdminApplicationContext()
        {
            var appContext = new ApplicationContext();
            appContext.Principal.UserId = "unit_test";
            appContext.Principal.Role = "Administrator";
            appContext.Principal.Username = "unit_test";

            return appContext;
        }
    }
}
