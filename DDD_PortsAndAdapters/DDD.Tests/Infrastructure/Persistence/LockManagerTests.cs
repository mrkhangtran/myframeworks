﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Application;
using DDD.Application.Exceptions;
using DDD.Application.Ports.Infrastructure.Concurrency;
using DDD.Application.Ports.Infrastructure.Providers;
using DDD.Application.Seedwork;
using DDD.Domain.Aggregates.ProductAggregate;
using DDD.Infrastructure.Persistence.Core.Concurrency;
using DDD.Infrastructure.Persistence.EF;
using DDD.Infrastructure.Persistence.EF.Concurrency;
using DDD.Tests.Setup;
using DDD.Tests.Setup.Fixtures;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Xunit;

namespace DDD.Tests.Infrastructure.Persistence
{
    public class LockManagerTests : IDisposable
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILockManager _lockManager;
        private readonly ApplicationContext _appContext;

        /// <summary>
        /// Setup per test
        /// </summary>
        public LockManagerTests()
        {
            _appContext = Helper.CreateAdminApplicationContext();

            // setup in-memory db context 
            _dbContext = Helper.CreateInMemoryDbContext(Guid.NewGuid().ToString(), _appContext);
            _dbContext.Database.EnsureCreated();

            // setup LockManager
            var configProvider = Substitute.For<IConfigProvider>();
            configProvider.GetSystemConfig().Returns(new SystemConfig(recordLockExpirySeconds: 5));
            _lockManager = new LockManager(_dbContext, configProvider);
        }

        [Fact]
        public async Task AcquireLockAsync_Success()
        {
            // Arrange
            var productId = 1;
            var userId = "123";

            // Act
            await _lockManager.AcquireLockAsync<Product>(productId, userId);
            var @lock = await _dbContext.Set<RecordLock>().FirstOrDefaultAsync(x => x.EntityId == productId && x.EntityName == nameof(Product) && x.OwnerId == userId);

            // Assert
            Assert.NotNull(@lock);
        }

        [Fact]
        public async Task AcquireLockAsync_Failed_As_Locked_By_Another_User()
        {
            // Arrange
            var productId = 1;
            var userId = "123";
            var anotherUserId = "456";
            var existLock = new RecordLock { EntityId = productId, EntityName = nameof(Product), OwnerId = userId, AcquiredDate = DateTime.UtcNow };
            _dbContext.Set<RecordLock>().Add(existLock);
            await _dbContext.SaveChangesAsync();

            // Act


            // Assert
            var exception = await Assert.ThrowsAsync<BusinessConcurrencyException>(() => _lockManager.AcquireLockAsync<Product>(existLock.EntityId, anotherUserId));
            Assert.Contains("Cannot acquire lock", exception.Message);
        }

        [Fact]
        public async Task ReleaseLockAsync()
        {
            // Arrange
            var productId = 1;
            var userId = "123";
            var existLock = new RecordLock { EntityId = productId, EntityName = nameof(Product), OwnerId = userId, AcquiredDate = DateTime.UtcNow };
            _dbContext.Set<RecordLock>().Add(existLock);
            await _dbContext.SaveChangesAsync();

            // Act
            await _lockManager.ReleaseLockAsync<Product>(productId, userId);
            var @lock = await _dbContext.Set<RecordLock>().FirstOrDefaultAsync(x => x.EntityId == productId && x.EntityName == nameof(Product) && x.OwnerId == userId);

            // Assert
            Assert.Null(@lock);
        }

        [Fact]
        public async Task HasLockAsync_User_Has_Lock()
        {
            // Arrange
            var productId = 1;
            var userId = "123";
            var existLock = new RecordLock { EntityId = productId, EntityName = nameof(Product), OwnerId = userId, AcquiredDate = DateTime.UtcNow };
            _dbContext.Set<RecordLock>().Add(existLock);
            await _dbContext.SaveChangesAsync();

            // Act
            var hasLock = await _lockManager.HasLockAsync<Product>(productId, userId);

            // Assert
            Assert.True(hasLock);
        }

        [Fact]
        public async Task HasLockAsync_User_Not_Has_Lock()
        {
            // Arrange
            var productId = 1;
            var userId = "123";
            var anotherUserId = "456";
            var existLock = new RecordLock { EntityId = productId, EntityName = nameof(Product), OwnerId = anotherUserId, AcquiredDate = DateTime.UtcNow };
            _dbContext.Set<RecordLock>().Add(existLock);
            await _dbContext.SaveChangesAsync();

            // Act
            var hasLock = await _lockManager.HasLockAsync<Product>(productId, userId);

            // Assert
            Assert.False(hasLock);
        }

        [Fact]
        public async Task ReleaseAllLocksAsync_User_Has_Lock()
        {
            // Arrange
            var userId = "123";
            var existLock_1 = new RecordLock { EntityId = 1, EntityName = nameof(Product), OwnerId = userId, AcquiredDate = DateTime.UtcNow };
            _dbContext.Set<RecordLock>().Add(existLock_1);
            var existLock_2 = new RecordLock { EntityId = 2, EntityName = nameof(Product), OwnerId = userId, AcquiredDate = DateTime.UtcNow };
            _dbContext.Set<RecordLock>().Add(existLock_2);
            await _dbContext.SaveChangesAsync();

            // Act
            await _lockManager.ReleaseAllLocksAsync(userId);
            var locksCount = await _dbContext.Set<RecordLock>().CountAsync();

            // Assert
            Assert.Equal(0, locksCount);
        }

        [Fact]
        public async Task RenewLockAsync_Success()
        {
            // Arrange
            var productId = 1;
            var userId = "123";
            var existLock = new RecordLock { EntityId = productId, EntityName = nameof(Product), OwnerId = userId, AcquiredDate = DateTime.UtcNow };
            _dbContext.Set<RecordLock>().Add(existLock);
            await _dbContext.SaveChangesAsync();
            _dbContext.Entry(existLock).State = EntityState.Detached;

            // Act
            await Task.Delay(TimeSpan.FromSeconds(2));
            await _lockManager.RenewLockAsync<Product>(productId, userId);
            var @lock = await _dbContext.Set<RecordLock>().FirstOrDefaultAsync();

            // Assert
            Assert.NotNull(@lock);
            Assert.True(@lock.AcquiredDate > existLock.AcquiredDate);
        }

        [Fact]
        public async Task RenewLockAsync_Failed_Lock_Expired()
        {
            // Arrange
            var productId = 1;
            var userId = "123";
            var existLock = new RecordLock { EntityId = productId, EntityName = nameof(Product), OwnerId = userId, AcquiredDate = DateTime.UtcNow };
            _dbContext.Set<RecordLock>().Add(existLock);
            await _dbContext.SaveChangesAsync();
            _dbContext.Entry(existLock).State = EntityState.Detached;

            // Act
            await Task.Delay(TimeSpan.FromSeconds(7));

            // Assert
            var exception = await Assert.ThrowsAsync<BusinessConcurrencyException>(() => _lockManager.RenewLockAsync<Product>(productId, userId));
            Assert.Contains("Lock expired", exception.Message);
        }

        [Fact]
        public async Task RenewLockAsync_Failed_Lock_Not_Found()
        {
            // Arrange
            var productId = 1;
            var userId = "123";
            var existLock = new RecordLock { EntityId = productId, EntityName = nameof(Product), OwnerId = userId, AcquiredDate = DateTime.UtcNow };
            _dbContext.Set<RecordLock>().Add(existLock);
            await _dbContext.SaveChangesAsync();
            _dbContext.Entry(existLock).State = EntityState.Detached;

            // Act

            // Assert
            var exception = await Assert.ThrowsAsync<BusinessConcurrencyException>(() => _lockManager.RenewLockAsync<Product>(productId, "456"));
            Assert.Contains("Lock not found for user", exception.Message);
        }

        /// <summary>
        /// Teardown per test
        /// </summary>
        public void Dispose()
        {
            _dbContext.Database.EnsureDeleted();
        }
    }
}
