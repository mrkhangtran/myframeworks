﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDD.Infrastructure.Persistence.EF.Migrations
{
    public partial class AddRecordLock : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RecordLocks",
                columns: table => new
                {
                    EntityId = table.Column<int>(nullable: false),
                    EntityName = table.Column<string>(nullable: false),
                    OwnerId = table.Column<string>(nullable: true),
                    AcquiredDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecordLocks", x => new { x.EntityId, x.EntityName });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RecordLocks");
        }
    }
}
