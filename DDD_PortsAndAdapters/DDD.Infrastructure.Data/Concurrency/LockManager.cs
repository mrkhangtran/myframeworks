﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Application;
using DDD.Application.Exceptions;
using DDD.Application.Ports.Infrastructure.Concurrency;
using DDD.Application.Ports.Infrastructure.Providers;
using DDD.Domain.Seedwork;
using DDD.Infrastructure.Persistence.Core.Concurrency;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infrastructure.Persistence.EF.Concurrency
{
    public class LockManager : ILockManager
    {
        private readonly SystemConfig _systemConfig;
        private readonly ApplicationDbContext _dbContext;
        private readonly DbSet<RecordLock> _locks;

        public LockManager(ApplicationDbContext dbContext, IConfigProvider configProvider)
        {
            _systemConfig = configProvider.GetSystemConfig();
            _dbContext = dbContext;
            _locks = dbContext.Set<RecordLock>();
        }

        public async Task AcquireLockAsync<T>(int entityId, string ownerId) where T : Entity
        {
            await RemoveExpiredLock<T>(entityId);

            var hasLock = await HasLockAsync<T>(entityId, ownerId);
            if (!hasLock)
            {
                try
                {
                    var @lock = new RecordLock
                    {
                        EntityId = entityId,
                        EntityName = typeof(T).Name,
                        OwnerId = ownerId,
                        AcquiredDate = DateTime.UtcNow
                    };

                    _locks.Add(@lock);
                    await _dbContext.SaveChangesAsync();
                }
                catch (Exception)
                {
                    throw new BusinessConcurrencyException("Cannot acquire lock. Entity could have been locked by another user.");
                }
            }
        }

        public async Task ReleaseLockAsync<T>(int entityId, string ownerId) where T : Entity
        {
            var @lock = await _locks.FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityName == typeof(T).Name && x.OwnerId == ownerId);
            if (@lock != null)
            {
                try
                {
                    _locks.Remove(@lock);
                    await _dbContext.SaveChangesAsync();
                }
                catch (Exception)
                {
                    throw new BusinessConcurrencyException("Unexpected error releasing lock.");
                }
            }
        }

        public async Task<bool> HasLockAsync<T>(int entityId, string ownerId) where T : Entity
        {
            var @lock = await _locks.AsNoTracking().FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityName == typeof(T).Name && x.OwnerId == ownerId);
            return @lock != null;
        }

        public async Task ReleaseAllLocksAsync(string ownerId)
        {
            var locks = await _locks.Where(x => x.OwnerId == ownerId).ToListAsync();
            locks.ForEach(x => _locks.Remove(x));

            await _dbContext.SaveChangesAsync();
        }

        public async Task RenewLockAsync<T>(int entityId, string ownerId)
        {
            var @lock = await _locks.FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityName == typeof(T).Name && x.OwnerId == ownerId);
            if (@lock != null)
            {
                if (@lock.AcquiredDate.AddSeconds(_systemConfig.RecordLockExpirySeconds) <= DateTime.UtcNow)
                {
                    throw new BusinessConcurrencyException("Lock expired");
                }

                @lock.AcquiredDate = DateTime.UtcNow;
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new BusinessConcurrencyException("Lock not found for user");
            }
        }

        private async Task RemoveExpiredLock<T>(int entityId)
        {
            var @lock = await _locks.FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityName == typeof(T).Name);
            if (@lock != null && @lock.AcquiredDate.AddSeconds(_systemConfig.RecordLockExpirySeconds) <= DateTime.UtcNow)
            {
                _locks.Remove(@lock);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
