﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using DDD.Application.Exceptions;
using DDD.Application.Ports.Infrastructure.Concurrency;
using Microsoft.EntityFrameworkCore;
using DDD.Application.Ports.Infrastructure.Persistence;
using DDD.Domain.Aggregates.ProductAggregate;
using DDD.Domain.Exceptions;
using DDD.Infrastructure.Persistence.EF.Seedwork;

namespace DDD.Infrastructure.Persistence.EF.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        //public async Task<Product> GetProductWithLockAsync(int id, string ownerUserId)
        //{
        //    using (var transaction = DbContext.Database.BeginTransaction(IsolationLevel.Serializable))
        //    {
        //        try
        //        {
        //            await _lockManager.AcquireLockAsync<Product>(id, ownerUserId);

        //            var product = await GetByIdAsync(id);
        //            if (product != null)
        //            {
        //                transaction.Commit();
        //            }
        //            else
        //            {
        //               throw new DomainException("Product not found");
        //            }

        //            return product;
        //        }
        //        catch (Exception)
        //        {
        //            transaction.Rollback();
        //            throw;
        //        }
        //    }
        //}

        //public async Task<Product> UpdateProductWithLockAsync(Product product, string ownerUserId)
        //{
        //    using (var transaction = DbContext.Database.BeginTransaction(IsolationLevel.Serializable))
        //    {
        //        try
        //        {
        //            var hasLock = await _lockManager.HasLockAsync<Product>(product.Id, ownerUserId);
        //            if (hasLock)
        //            {
        //                Update(product, product.RowVersion);
        //                await DbContext.SaveChangesAsync();

        //                transaction.Commit();

        //                return product;
        //            }
        //            else
        //            {
        //                throw new BusinessConcurrencyException("User does not have lock on product");
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            transaction.Rollback();
        //            throw;
        //        }
        //    }
        //}
    }
}
