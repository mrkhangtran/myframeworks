﻿using System;
using System.Threading.Tasks;
using DDD.Application.Ports.Infrastructure.Persistence;
using DDD.Infrastructure.Persistence.Core.Exceptions;
using DDD.Infrastructure.Persistence.Core.Idempotency;

namespace DDD.Infrastructure.Persistence.EF.Idempotency
{
    public class RequestManager : IIdentifiedRequestManager
    {
        private readonly ApplicationDbContext _context;

        public RequestManager(ApplicationDbContext context)
        {
            _context = context;
        }


        public async Task<bool> ExistsAsync(Guid id)
        {
            var request = await _context.FindAsync<ClientRequest>(id);

            return request != null;
        }

        public async Task CreateRequestForCommandAsync<T>(Guid id)
        {
            var exists = await ExistsAsync(id);

            var request = exists
                ? throw new IdempotencyRequestDuplicatedException()
                : new ClientRequest
                {
                    Id = id,
                    Name = typeof(T).Name,
                    Time = DateTime.Now
                };

            _context.Add(request);

            await _context.SaveChangesAsync();
        }
    }
}
