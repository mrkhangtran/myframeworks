﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DDD.Application.Ports.Infrastructure.DbTransactionAdapter;

namespace DDD.Infrastructure.Persistence.EF.DbTransactionAdapter
{
    public class DbTransactionManager : IDbTransactionManager
    {
        private readonly ApplicationDbContext _dbContext;

        public DbTransactionManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IDbTransactionAdapter> BeginTransactionAsync(IsolationLevel isolationLevel)
        {
            var transaction = await _dbContext.Database.BeginTransactionAsync(isolationLevel);
            return new DbTransactionAdapter(transaction);
        }

        public DbConnection GetDbConnection()
        {
            return _dbContext.Database.GetDbConnection();
        }
    }
}
