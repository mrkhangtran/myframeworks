﻿using System.Data.Common;
using System.Threading.Tasks;
using DDD.Application.Ports.Infrastructure.DbTransactionAdapter;
using Microsoft.EntityFrameworkCore.Storage;

namespace DDD.Infrastructure.Persistence.EF.DbTransactionAdapter
{
    public class DbTransactionAdapter : IDbTransactionAdapter
    {
        private readonly IDbContextTransaction _transaction;

        public DbTransactionAdapter(IDbContextTransaction transaction)
        {
            _transaction = transaction;
        }

        public Task CommitAsync()
        {
            _transaction.Commit();

            return Task.CompletedTask;
        }

        public Task RollbackAsync()
        {
            _transaction.Rollback();

            return Task.CompletedTask;
        }

        public DbTransaction GetDbTransaction()
        {
            return _transaction.GetDbTransaction();
        }

        public void Dispose()
        {
            _transaction?.Dispose();
        }
    }
}
