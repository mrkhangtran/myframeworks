﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Infrastructure.Persistence.Core.Concurrency;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDD.Infrastructure.Persistence.EF.EntityConfigurations
{
    class RecordLockConfiguration : IEntityTypeConfiguration<RecordLock>
    {
        public void Configure(EntityTypeBuilder<RecordLock> builder)
        {
            // table
            builder.ToTable("RecordLocks");

            // key
            builder.HasKey(x => new { x.EntityId, x.EntityName });
        }
    }
}
