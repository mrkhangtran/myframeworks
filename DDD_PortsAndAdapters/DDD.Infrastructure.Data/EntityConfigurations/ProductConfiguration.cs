﻿using System;
using System.Collections.Generic;
using System.Text;
using DDD.Domain.Aggregates.ProductAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDD.Infrastructure.Persistence.EF.EntityConfigurations
{
    class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            // table
            builder.ToTable("Products");

            // key
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ForSqlServerUseSequenceHiLo("ProductsSequenceHiLo");

            // row version
            builder.Property(x => x.RowVersion).IsRowVersion();

            // ignore
            builder.Ignore(x => x.DomainEvents);

            // validation
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.CreatedBy).IsRequired();
        }
    }
}
