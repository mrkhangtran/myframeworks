﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DDD.Application.Ports.Infrastructure.Providers;
using DDD.Application.Seedwork;
using DDD.Domain.Seedwork;
using DDD.Infrastructure.Persistence.Core.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace DDD.Infrastructure.Persistence.EF
{
    public class ApplicationDbContext : DbContext, IUnitOfWork
    {
        private readonly IMediator _mediator;
        private readonly ApplicationContext _applicationContext;
        private readonly IConnectionStringProvider _connectionStringProvider;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, ApplicationContext applicationContext) : base(options)
        {
            _applicationContext = applicationContext;
        }

        public ApplicationDbContext(IMediator mediator, ApplicationContext applicationContext, IConnectionStringProvider connectionStringProvider)
        {
            _mediator = mediator;
            _applicationContext = applicationContext;
            _connectionStringProvider = connectionStringProvider;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_connectionStringProvider != null)
            {
                var connectionString = _connectionStringProvider.GetDbConnectionString();
                optionsBuilder.UseSqlServer(connectionString,
                    sqlOptions =>
                    {
                        sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                    });
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            ConfigureEntities(builder);
            ConfigureSoftDelete(builder);
        }

        public async Task SaveChangesAsync(ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (_mediator != null)
            {
                await _mediator.DispatchDomainEventsAsync(this);
            }

            bool saveFailed;

            switch (strategy)
            {
                case ConcurrencyResolutionStrategy.None:
                    try
                    {
                        // https://stackoverflow.com/questions/4402586/optimisticconcurrencyexception-does-not-work-in-entity-framework-in-certain-situ
                        var isRowVersionChanged = ChangeTracker.Entries()
                            .Any(x => x.Properties.Any(m => m.Metadata.Name == "RowVersion") && x.CurrentValues.GetValue<byte[]>("RowVersion") != null && !x.CurrentValues.GetValue<byte[]>("RowVersion").SequenceEqual(x.OriginalValues.GetValue<byte[]>("RowVersion")));
                        if (isRowVersionChanged)
                        {
                            throw new DbConcurrencyException();
                        }

                        PreSaveChanges();

                        await SaveChangesAsync(cancellationToken);
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        throw new DbConcurrencyException();
                    }

                    break;
                case ConcurrencyResolutionStrategy.DatabaseWin:
                    do
                    {
                        saveFailed = false;

                        try
                        {
                            PreSaveChanges();

                            await SaveChangesAsync(cancellationToken);
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update the values of the Entity that failed to save from the store 
                            ex.Entries.Single().Reload();
                        }

                    } while (saveFailed);

                    break;
                case ConcurrencyResolutionStrategy.ClientWin:
                    do
                    {
                        saveFailed = false;
                        try
                        {
                            PreSaveChanges();

                            await SaveChangesAsync(cancellationToken);
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update original values from the database 
                            var entry = ex.Entries.Single();
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                        }

                    } while (saveFailed);


                    break;
            }
        }

        private void PreSaveChanges()
        {
            HandleSoftDelete();
            HandleAudit();
        }

        private void HandleSoftDelete()
        {
            foreach (var entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Deleted))
            {
                if (entry.Entity is ISoftDeletable)
                {
                    entry.Property("IsDeleted").CurrentValue = true;
                    entry.State = EntityState.Modified;
                }
            }
        }

        private void HandleAudit()
        {
            foreach (var entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            {
                if (entry.Entity is IAuditable auditable)
                {
                    if (entry.State == EntityState.Added)
                    {
                        auditable.CreatedBy = _applicationContext.Principal.Username;
                        auditable.CreatedDate = DateTime.UtcNow;
                    }
                    else
                    {
                        auditable.UpdatedBy = _applicationContext.Principal.Username;
                        auditable.UpdatedDate = DateTime.UtcNow;
                    }
                }
            }
        }

        private void ConfigureEntities(ModelBuilder builder)
        {
            var types = typeof(ApplicationDbContext).GetTypeInfo().Assembly.GetTypes();
            var maps = (from t in types
                        where t.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>))
                              && !t.GetTypeInfo().IsAbstract
                              && !t.GetTypeInfo().IsInterface
                        select Activator.CreateInstance(t)).ToArray();

            foreach (var map in maps)
            {
                /*
                 a bit hack here which just gets the first ApplyConfiguration method
                 there are several overloads of this method => find a way to get the "correct" one instead of the "first" one
                 */
                var methodInfo = typeof(ModelBuilder).GetMethods().First(x => x.Name == nameof(builder.ApplyConfiguration));
                methodInfo = methodInfo.MakeGenericMethod(map.GetType().GetInterfaces().First().GenericTypeArguments);
                methodInfo.Invoke(builder, new[] { map });
            }
        }

        private void ConfigureSoftDelete(ModelBuilder builder)
        {
            foreach (var entity in builder.Model.GetEntityTypes())
            {
                if (typeof(ISoftDeletable).IsAssignableFrom(entity.ClrType) && entity.BaseType == null)
                {
                    builder
                        .Entity(entity.ClrType)
                        .HasDiscriminator("IsDeleted", typeof(bool))
                        .HasValue(false);

                    builder
                        .Entity(entity.ClrType)
                        .Property(typeof(bool), "IsDeleted")
                        .IsRequired()
                        .HasDefaultValue(false);

                    builder
                        .Entity(entity.ClrType)
                        .Property(typeof(bool), "IsDeleted")
                        .Metadata
                        .AfterSaveBehavior = PropertySaveBehavior.Save;
                }
            }
        }
    }

    // For Migrations
    public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var configuration = config.Build();

            builder.UseSqlServer(configuration.GetConnectionString("AppConnection"));
            return new ApplicationDbContext(builder.Options);
        }
    }
}
