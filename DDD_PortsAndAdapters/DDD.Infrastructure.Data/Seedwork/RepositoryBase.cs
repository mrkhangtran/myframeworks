﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DDD.Application.Seedwork;
using DDD.Domain.Seedwork;
using DDD.Infrastructure.Utilities.Helpers;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace DDD.Infrastructure.Persistence.EF.Seedwork
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : AggregateRoot
    {
        protected readonly ApplicationDbContext DbContext;

        public IUnitOfWork UnitOfWork => DbContext;

        protected RepositoryBase(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        protected virtual Task LoadChildrenAsync(T entity)
        {
            return Task.CompletedTask;
        }

        public async Task<IEnumerable<T>> QueryAsync(QueryObject<T> queryObject, List<string> includes, int? maxItems)
        {
            var query = DbContext.Set<T>().AsQueryable();

            // filter
            if (queryObject != null)
            {
                query = query.Where(queryObject.AsExpression());

                // order
                var orderBy = queryObject.GetOrderBy();
                if (!string.IsNullOrEmpty(orderBy))
                {
                    query = query.OrderBy(orderBy);
                }
                else
                {
                    query = query.OrderBy(x => x.Id);
                }
            }

            // get top
            if (maxItems.HasValue)
            {
                query = query.Take(maxItems.Value);
            }

            // include
            query = includes.Aggregate(query, (current, include) => current.Include(include));

            // result
            var entities = await query.ToListAsync();
            return entities;
        }

        public async Task<PagedQueryResult<T>> PagedQueryAsync(QueryObject<T> queryObject, List<string> includes, int pageNumber, int pageSize)
        {
            var query = DbContext.Set<T>().AsQueryable();

            // filter
            if (queryObject != null)
            {
                query = query.Where(queryObject.AsExpression());

                // order
                var orderBy = queryObject.GetOrderBy();
                if (!string.IsNullOrEmpty(orderBy))
                {
                    query = query.OrderBy(orderBy);
                }
                else
                {
                    query = query.OrderBy(x => x.Id);
                }
            }

            // paging
            if (pageNumber == 0 || pageSize == 0)
            {
                throw new InvalidOperationException("Page Number and Page Size must be greater than 0");
            }

            var totalCount = await query.CountAsync();
            var pageCount = (totalCount + pageSize - 1) / pageSize;
            query = query.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            // include
            query = includes.Aggregate(query, (current, include) => current.Include(include));

            // result
            var entities = await query.ToListAsync();
            return new PagedQueryResult<T>
            {
                ItemCount = totalCount,
                PageCount = pageCount,
                Items = entities
            };
        }

        public async Task<T> GetByIdAsync(int id)
        {
            var entity = await DbContext.Set<T>().FindAsync(id);
            if (entity != null)
            {
                await LoadChildrenAsync(entity);
            }

            return entity;
        }

        public Task InsertAsync(T entity)
        {
            return DbContext.Set<T>().AddAsync(entity);
        }

        public Task InsertRangeAsync(IEnumerable<T> entities)
        {
            return DbContext.Set<T>().AddRangeAsync(entities);
        }

        public void Update(T entity, string currentRowVersion)
        {
            if (string.IsNullOrWhiteSpace(currentRowVersion))
                throw new ArgumentNullException(nameof(currentRowVersion));

            entity.RowVersion = ByteArrayHelper.FromHex(currentRowVersion);
            DbContext.Set<T>().Update(entity);
        }

        public void Update(T entity, byte[] currentRowVersion)
        {
            entity.RowVersion = currentRowVersion ?? throw new ArgumentNullException(nameof(currentRowVersion));
            DbContext.Set<T>().Update(entity);
        }

        public void Delete(T entity, string currentRowVersion)
        {
            if (string.IsNullOrWhiteSpace(currentRowVersion))
                throw new ArgumentNullException(nameof(currentRowVersion));

            entity.RowVersion = ByteArrayHelper.FromHex(currentRowVersion);
            DbContext.Set<T>().Remove(entity);
        }

        public void Delete(T entity, byte[] currentRowVersion)
        {
            entity.RowVersion = currentRowVersion ?? throw new ArgumentNullException(nameof(currentRowVersion));
            DbContext.Set<T>().Remove(entity);
        }
    }
}
